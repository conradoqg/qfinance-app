<?
Index::start();

Class Index {
			
	public static function start() {
		self::startAutoLoader();
		ConfigurationService::ConfigureServices();        
		LocalizationService::Localize();
						
		// Execute remote constructor or an remote call
		if (isset($_GET['doRemoting']) || isset($_GET['configureRemoting'])) {
			session_start();			
			self::remoting();	
		// Execute normal application
		} else {
			session_start();
			DeveloperService::clearMessages();
			if (self::isMobile()) {
				self::showMobile();		
			} else {
				self::show();
			}
		} 
	} 
	
	private static function startAutoLoader() {
		include 'php/service/AutoLoaderService.php';
		AutoLoaderService::init();
	}
	
	public static function remoting() {
		$remotingService = new RemotingService();
		$remotingService->doRemoting();			
	}
	
	private static function showMobile() {
		$elements = array();
		$elements = array_merge($elements, LibraryInclude::getSenchaTouch());
		$elements = array_merge($elements, LibraryInclude::getQFinanceMobile());		
		
		echo '
			<!DOCTYPE html>
			<html>
			<head>
			 	<meta charset="utf-8">			 	
				<title>QFinance</title>
				<link REL="SHORTCUT ICON" HREF="resources/QFinance.ico">	';
		
		foreach ($elements as $element) {
			echo $element->render();
		}
		
		echo '
			</head>
			<body></body>
			<script type="text/javascript">
				Ext.setup({
				    icon: "resources/QFinance.jpg",
				    tabletStartupScreen: "resources/QFinance_tablet_startup.png",
				    phoneStartupScreen: "resources/QFinance_phone_startup.png",
				    glossOnIcon: true,
				    onReady: function() {
				    	var appMobile = new QFinance.AppMobile();
				    	appMobile.show();        
				    }
				});
			</script>
			</html>';
	}
	
	private static function show() {
		$baseCSS = new CSSElement("lib/Ext/css/Desktop.css"); 			
		$loadingElement = new LoadingElement();
		
		$elementsToAdd = array();
		$elementsToAdd = array_merge($elementsToAdd, LibraryInclude::getExtJs());
		$elementsToAdd = array_merge($elementsToAdd, LibraryInclude::getJQuery());
		$elementsToAdd = array_merge($elementsToAdd, LibraryInclude::getHighChart());
		$elementsToAdd = array_merge($elementsToAdd, LibraryInclude::getExtJsUx());
		$elementsToAdd = array_merge($elementsToAdd, LibraryInclude::getQFinance());		
		
		$loadingElement->mergeElementArray($elementsToAdd);		
		
		$elements = array(
			'<!--{BASE_CSS}-->' => $baseCSS->render(),
			'<!--{COMPONENTS}-->' => $loadingElement->render(),
			'<!--{DESKTOP}-->' => self::getDesktopMarkup()
		);
					
		$template = '
			<html>
				<head>	
					<meta charset="utf-8">	
					<title>QFinance</title>	
					<link REL="SHORTCUT ICON" HREF="resources/images/QFinance.ico">	
				</head>	
				<body scroll="no">
					<!--{BASE_CSS}-->
					<!--{COMPONENTS}-->					
					<!--{DESKTOP}-->	
				</body>
				<script type="text/javascript">
					Ext.onReady( function() {
						var loadingMask = Ext.get("ext-loa15");
						var loading = Ext.get("loading");
						// Hide loading message
						loading.fadeOut({ 
					     	duration: 0.2, 
					     	remove: true,
					     	callback: function() {	
								QFinance.QFinanceApp = new QFinance.App();
								QFinance.QFinanceApp.initApp();
					     	}   	
						});
					});
				</script>
			</html>';	

		echo strtr( $template, $elements );	
	}
	
	private static function getDesktopMarkup() {
		return '<div id="x-desktop">
				    <!--
				    Retirado para n�o aparecer o logo do Ext JS
				    <a href="http://extjs.com" target="_blank" style="margin:5px; float:right;"><img src="resources/images/QFinance.png" /></a> 
				    -->
					<!--
					Retirado pois no momento sem atalhos no Desktop 
				    <dl id="x-shortcuts">
				        <dt id="grid-win-shortcut">
				            <a href="#"><img src="resources/images/s.gif">
				            	<div>Grid Window</div>
				            </a>
				        </dt>
				        <dt id="acc-win-shortcut">
				            <a href="#"><img src="resources/images/s.gif">
				            	<div>Accordion Window</div>
				            </a>
				        </dt>
				    </dl>
				     -->
				</div>
				
				<div id="ux-taskbar">
					<div id="ux-taskbar-start"></div>
					<div id="ux-taskbuttons-panel"></div>
					<div class="x-clear"></div>
				</div>';
	}

	private static function isMobile() {
        $regex_match="/(nokia|iphone|android|motorola|^mot\-|softbank|foma|docomo|kddi|up\.browser|up\.link|";
        $regex_match.="htc|dopod|blazer|netfront|helio|hosin|huawei|novarra|CoolPad|webos|techfaith|palmsource|";
        $regex_match.="blackberry|alcatel|amoi|ktouch|nexian|samsung|^sam\-|s[cg]h|^lge|ericsson|philips|sagem|wellcom|bunjalloo|maui|";
        $regex_match.="symbian|smartphone|midp|wap|phone|windows ce|iemobile|^spice|^bird|^zte\-|longcos|pantech|gionee|^sie\-|portalmmm|";
        $regex_match.="jig\s browser|hiptop|^ucweb|^benq|haier|^lct|opera\s*mobi|opera\*mini|320x320|240x320|176x220";
        $regex_match.=")/i";        
        return isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE']) or (isset($_SERVER['HTTP_USER_AGENT']) and preg_match($regex_match, strtolower($_SERVER['HTTP_USER_AGENT'])));
	}
}
?>