QFinance.AppMobile = Ext.extend(Object, {
	// Localization
	newExpenseAnnotationText: 'New expense annotation',
	requiredText: 'Please enter the information above.',
	accountText: 'Account',
	dateText: 'Date',
	descriptionText: 'Description',
	amountText: 'Amount',
	destinationText: 'Destination',
	categoryText: 'Category',
	resetText: 'Reset',
	addText: 'Add',
	fillRequiredText: 'Please fill required fields.',
	internalServerErrorText: 'Internal server error.',
	
	show: function() {
		Ext.regModel('TransactionAnnotation', {
            fields: [{
				name: 'date',		
			    type: 'date',	
			    dateFormat: "c"
			},{
				name: 'description'
			},,{
				name: 'amount',	
				type: 'float'
			},{
				name: 'sourceAccountID',
				type: 'int',
				useNull: true
			},{
				name: 'destinationAccountID',
				type: 'int',
				useNull: true
			},{
				name: 'categoryID', 
				type: 'int',
				useNull: true
			}]
        });
    	
    	var privateAccountsStore = new Ext.data.JsonStore({
    		fields: [
         		    {
         		    	name: 'id',
         		    	type: 'int'
         		    },
         		    {
         		    	name: 'accountName'	    		    
         		    },
         		    {
         		    	name: 'accountType'
         		    }
         		]
         });
        
    	var accountsStore = new Ext.data.JsonStore({
    		fields: [
         		    {
         		    	name: 'id',
         		    	type: 'int'
         		    },
         		    {
         		    	name: 'accountName'	    		    
         		    },
         		    {
         		    	name: 'accountType'
         		    }
         		]
         });
    	
    	var categoriesStore = new Ext.data.JsonStore({
    		fields: [
         		    {
         		    	name: 'id',
         		    	type: 'int'
         		    },
         		    {
         		    	name: 'categoryName'	    		    
         		    }
         		]
         });
       
        var formBase = {
            scroll: 'vertical',
            items: [
                {
                    xtype: 'fieldset',
                    title: this.newExpenseAnnotationText,
                    instructions: this.requiredText,
                    defaults: {                        
                        labelAlign: 'left',
                        labelWidth: '40%'
                    },
                    items: [ this.sourceAccountCombo = new Ext.form.Select({
                        xtype: 'selectfield',
                        name : 'sourceAccountID',
                        label: this.accountText,
                        valueField : 'id',
                        displayField : 'accountName',
                        store : privateAccountsStore,
                        required: true
                    }),{
                    	xtype: 'datepickerfield',
                    	name: 'date',
                    	label: this.dateText,
                    	value: new Date(),
                    	required: true
                    },{
                        xtype: 'textfield',
                        name : 'description',
                        label: this.descriptionText,
                        useClearIcon: true,
                        autoCapitalize : false,
                        required: true
                    },{
                    	xtype: 'numberfield',
                        name: 'amount',
                        label: this.amountText,
                        required: true,
                        decimalSeparator: QFinance.decimalSeparator
                    }, this.destinationAccountCombo = new Ext.form.Select({
                        xtype: 'selectfield',
                        name : 'destinationAccountID',
                        label: this.destinationText,
                        valueField : 'id',
                        displayField : 'accountName',
                        store : accountsStore,
                        required: false
                    }), this.categoryCombo = new Ext.form.Select({
                        xtype: 'selectfield',
                        name : 'categoryID',
                        label: this.categoryText,
                        valueField : 'id',
                        displayField : 'categoryName',
                        store : categoriesStore,
                        required: false
                    })]
                }],
        
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [                        
                        {
                            text: this.resetText,
                            handler: function() {
                                form.reset();
                            }
                        },
                        {xtype: 'spacer'},
                        {
                            text: this.addText,
                            ui: 'confirm',
                            scope: this,
                            handler: function() {
                            	var newTransaction = Ext.ModelMgr.create({}, 'TransactionAnnotation');
                            	var valid = false;
                            	form.updateRecord(newTransaction, true);
                            	newTransaction.commit();     
                            	
                            	if (newTransaction.data.sourceAccountID == 0)
                            		newTransaction.data.sourceAccountID = null;
                            	
                            	if (newTransaction.data.destinationAccountID == 0)
                            		newTransaction.data.destinationAccountID = null;
                            	
                            	if (newTransaction.data.categoryAccountID == 0)
                            		newTransaction.data.categoryAccountID = null;
                            	
                            	if (newTransaction.data.sourceAccountID != null && newTransaction.data.description != null && newTransaction.data.amount != null)
                            		valid = true;
                            	
                            	if (!valid) {
                            		Ext.Msg.alert(this.newExpenseAnnotationText, this.fillRequiredText, Ext.emptyFn);
                            	} else { 
                            		                       	
	                            	form.setLoading(true);
	                            	QFinance.Remoting.MobileAction.addTransactionAnnotation(newTransaction.data, function(result, event) {	                            		
	                            		var messsage = '';
	                            		var where = '';
	                            		var failure = false;
	                            		
	                            		form.setLoading(false);
	                            		
	                            		if (result) {
	                            			if (result.success) {
	                            				form.reset();		
	                            			} else {
	                            				failure = true;						
	                            				message = this.internalServerErrorText;	                            				
	                            			}            
	                            		} else {
	                            			failure = true;
	                            			message = this.internalServerErrorText;
	                            		}
	                            		
	                            		if (failure) {
	                            			Ext.Msg.alert(this.newExpenseAnnotationText, message, Ext.emptyFn);					
	                            			QFinance.service.DeveloperService.log('Direct call failure: ' + message + ' where\n' + where);	                            			
	                            		} else {
	                            			
	                            		}	                            			                            	
	                            	}, this);
                            	}
                            }
                        }
                    ]
                }
            ]
        };
        
        if (Ext.is.Phone) {
            formBase.fullscreen = true;
        } else {
            Ext.apply(formBase, {
                autoRender: true,
                floating: true,
                modal: true,
                centered: true,
                hideOnMaskTap: false,
                height: 385,
                width: 480
            });
        }
                
        QFinance.Remoting.MobileAction.getAccountsPlusNone(function(result, event) {
        	accountsStore.loadData( result.data );
        	this.sourceAccountCombo.setValue(null);
        	this.destinationAccountCombo.setValue(null);
        	QFinance.Remoting.MobileAction.getAccountsByTypePlusNone(1, function(result, event) {
        		privateAccountsStore.loadData( result.data );
                QFinance.Remoting.MobileAction.getCategoriesPlusNone(function(result, event) {
                	categoriesStore.loadData( result.data );
                	this.categoryCombo.setValue(null);
                	form.setLoading(false);
                }, this);
        	}, this); 
        }, this);
        
        form = new Ext.form.FormPanel(formBase);
        form.setLoading(true);
        form.show();
	}
});

