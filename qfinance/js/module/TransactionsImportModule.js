/*
 * @depends module.js
 */
QFinance.module.TransactionsImportModule = Ext.extend(QFinance.module.Module, {
    // Configuration
    id: 'QFinance.module.TransactionsImportModule',	
	
    // Localization
    closeText: 'Close',
    importText: 'Import',
	
    // Others
    transactionsImportPanel: null,
        
    init: function() {
        this.launcher = {
            text: this.importText,
            iconCls: 'fugue-money--arrow',
            handler: this.createWindow,
            scope: this
        };
    },
    createWindow: function() {		
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('QFinance.module.TransactionsImportModule.createWindow.win');
        if (!win){
            win = desktop.createWindow({
                id: 'QFinance.module.TransactionsImportModule.createWindow.win',
                title: this.importText,
                width: 700,
                height: 500,
                iconCls: 'fugue-money--arrow',
                layout: 'fit',
                items: [ this.transactionsImportPanel = new QFinance.component.TransactionsImportPanel({
                    listeners: {
                        importData: function() {
                            win.close();
                        }
                    }
                })],
                bbar: [new Ext.Button({
                    text: this.closeText,
                    iconCls: 'fugue-door-open-out',	
                    scope: this,
                    handler: function() {
                        this.transactionsImportPanel.checkTemporaryTransactionsGhosts();
                        win.close();					
                    }
                })],
                stateful: true
            }, QFinance.ui.Window);
        }
        win.show();
    }
});