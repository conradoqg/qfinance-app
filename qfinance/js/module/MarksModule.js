/*
 * @depends module.js
 */
QFinance.module.MarksModule = Ext.extend(QFinance.module.Module, {
    // Configuration
    id: 'QFinance.module.MarksModule',
	
    // Localization
    marksText: 'Marks',
    closeText: 'Close',	
    
    init : function(){
        this.launcher = {
            text: this.marksText,
            iconCls: 'fugue-bookmarks',
            handler: this.createWindow,
            scope: this
        };
    },
    
    createWindow : function(){		
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('QFinance.module.MarksModule.createWindow.win');
        var marksEditPanel;
        if(!win){	    	
            win = desktop.createWindow({
                id: 'QFinance.module.MarksModule.createWindow.win',
                title: this.marksText,
                width: 480,
                height: 300,
                iconCls: 'fugue-bookmarks',
                animCollapse: false,
                layout: 'fit',	            
                items: [ marksEditPanel = new QFinance.component.MarksEditPanel() ],
                listeners: {
                    show: function() {
                        marksEditPanel.loadRecords();
                    }
                },
                bbar: [new Ext.Button({
                    text: this.closeText,
                    iconCls: 'fugue-door-open-out',	
                    handler: function() {
                        win.close();				
                    }
                })],
                stateful: true
            }, QFinance.ui.Window);
        }
        win.show();
    }
});