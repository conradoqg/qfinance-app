/*
 * @depends module.js
 */
QFinance.module.OverviewModule = Ext.extend(QFinance.module.Module, {
    // Configuration
    id: 'QFinance.module.OverviewModule',
	
    // Localization
    overviewText: 'Overview',
    closeText: 'Close',

    init : function(){
        this.launcher = {
            text: this.overviewText,
            iconCls: 'globe-green',
            handler: this.createWindow,
            scope: this
        };
    },
    
    createWindow : function(){
        var overviewPanel = null;
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('QFinance.module.OverviewModule.createWindow.win');
        if(!win){	
            win = desktop.createWindow({
                id: 'QFinance.module.OverviewModule.createWindow.win',
                title: this.overviewText,
                width: 400,
                height: 450,
                iconCls: 'globe-green',
                animCollapse: false,
                layout: 'fit',	            
                items: [ overviewPanel = new QFinance.component.OverviewPanel() ],
                listeners: {
                    show: function() {
                        overviewPanel.loadPortlets();
                    }
                },
                bbar: [new Ext.Button({
                    text: this.closeText,
                    iconCls: 'fugue-door-open-out',	
                    handler: function() {
                        win.close();				
                    }
                })],
                stateful: true
            }, QFinance.ui.Window);
        }
        win.show();
    }
});