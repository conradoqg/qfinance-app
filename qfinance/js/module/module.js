Ext.namespace("QFinance.module");

QFinance.module.Module = function(config){
    Ext.apply(this, config);
    QFinance.module.Module.superclass.constructor.call(this);
    this.init();
};

Ext.extend(QFinance.module.Module, Ext.util.Observable, {
    init : Ext.emptyFn,
    setApp : function(app) {
    	this.app = app;
    }
});