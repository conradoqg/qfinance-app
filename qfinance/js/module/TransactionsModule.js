/*
 * @depends module.js, Window.js
 */
QFinance.module.TransactionsModule = Ext.extend(QFinance.module.Module, {
    // Configuration
    id: 'QFinance.module.TransactionsModule',	
	
    // Localization
    transactionsText: 'Transactions',
    selectAccountText: 'Select an account:',
    closeText: 'Close',
    
    // Components
    transactionsEditPanel: null,
    accountStore: null,
    
    init : function(){
        this.launcher = {
            text: this.transactionsText,
            iconCls: 'fugue-moneys',
            handler: this.createWindow,
            scope: this
        };        
    },
    createWindow : function(){		
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('QFinance.module.TransactionsModule.createWindow.win');
        if (!win){
            this.accountStore = new Ext.data.DirectStore({    		
                directFn: QFinance.Remoting.AccountAction.getByAccountType,
                paramsAsHash: false,
                autoLoad: false,
                paramOrder: [ 'type' ],
                baseParams: {
                    type: '1'
                },
                idProperty: 'id',
                root: 'data',
                successProperty: 'success',
                fields: [
                {
                    name: 'id',
                    type: 'int'
                },
                {
                    name: 'accountName'	    		    
                },
                {
                    name: 'accountType'
                }
                ],
                listeners: {
                    scope: this,
                    beforeload: function(store, options) {
                        store.id = win.processStatusManager.start([win.topToolbar]);
                    },				
                    load: function(store, records, options) {
                        win.processStatusManager.success(store.id);
                    },				
                    exception: function(dataProxy, type, action, options, response, arg ) {
                        var message = '';
                        var where = '';
                        if (response.result) {
                            message = response.result.message;
                            if (response.result.type == 'exception')
                                where = response.result.where;
                        } else {
                            message = response.message;
                        }
                        win.processStatusManager.error(this.accountStore.id, message);
                        QFinance.service.DeveloperService.log('Store failure: ' + message + ' where\n' + where);
                    }
                }   		
            });
            win = desktop.createWindow({
                id: 'QFinance.module.TransactionsModule.createWindow.win',
                title: this.transactionsText,
                width: 740,
                height: 480,
                iconCls: 'fugue-moneys',
                animCollapse: false,
                layout: 'fit',
                items: [this.transactionsEditPanel = new QFinance.component.TransactionsEditPanel({
                    showAccountOverview: true,
                    showAccountBalance: true
                })],
                tbar: [
                this.selectAccountText,
                {
                    xtype: 'combo',
                    store: this.accountStore,
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnFocus: true,
                    valueField: 'id',
                    displayField: 'accountName',
                    mode: 'local',
                    listeners: {
                        scope: this,
                        select: function(comboBox, record, index) {	              			
                            this.transactionsEditPanel.contextAccountID = record.get('id');
                            this.transactionsEditPanel.load();
                            this.transactionsEditPanel.enable();
                        }
                    }
                }],
                listeners: {
                    scope: this,
                    show: function() {
                        this.transactionsEditPanel.disable();
                        this.accountStore.load(this);
                    }
                },
                bbar:  [new Ext.Button({
                    text: this.closeText,
                    iconCls: 'fugue-door-open-out',	
                    handler: function() {
                        win.close();				
                    }
                })],
                stateful: true
            },QFinance.ui.Window);
        }
        win.show();
    }
});