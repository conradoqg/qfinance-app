/*
 * @depends module.js
 */
QFinance.module.DeveloperConsoleModule = Ext.extend(QFinance.module.Module, {
    // Configuration
    id: 'QFinance.module.DeveloperConsoleModule',	
	
    // Localization
    developerConsoleText: 'Developer Console',
    startText: 'Start',
    stopText: 'Stop',
    updateText: 'Update',
    clearText: 'Clear',
    closeText: 'Close',
    
    init : function(){
        this.launcher = {
            text: this.developerConsoleText,
            iconCls: 'fugue-terminal',
            handler: this.createWindow,
            scope: this
        };
    },
    
    createWindow : function(){		
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('QFinance.module.DeveloperConsoleModule.createWindow.win');
        var logPanel;
        var running;
        
        if(!win){	    		    	
            var monitorButton = new Ext.Button({
                text: (QFinance.service.DeveloperService.running ? this.stopText : this.startText),
                iconCls: (QFinance.service.DeveloperService.running ? 'fugue-control-pause' : 'fugue-control'),
                scope: this,
                handler: function() {
                    if (QFinance.service.DeveloperService.running) {
                        monitorButton.setText(this.startText);
                        monitorButton.setIconClass('fugue-control');
                        QFinance.service.DeveloperService.stopMonitor();
                        running = false;
                    } else {
                        monitorButton.setText(this.stopText);
                        monitorButton.setIconClass('fugue-control-pause');
                        QFinance.service.DeveloperService.startMonitor();
                        running = true;
                    }
                }
            });
	    	
            win = desktop.createWindow({
                id: 'QFinance.module.DeveloperConsoleModule.createWindow.win',
                title: this.developerConsoleText,
                width: 480,
                height: 200,
                iconCls: 'fugue-terminal',
                animCollapse: false,
                layout: 'fit',	            
                items: [ 
                logPanel = new Ext.Panel({
                    xtype: 'panel',
                    autoScroll: true,
                    region: 'center',
                    border: false,
                    style: 'border-width:0 1px 0 0',
			
                    log : function(){
                        var markup = [  '<div style="padding:5px !important;border-bottom:1px solid #ccc;">',
                        Ext.util.Format.htmlEncode(Array.prototype.join.call(arguments, ', ')).replace(/\n/g, '<br/>').replace(/\s/g, '&#160;'),
                        '</div>'].join(''),
                        bd = this.body.dom;
			
                        this.body.insertHtml('beforeend', markup);
                        bd.scrollTop = bd.scrollHeight;				            
                    },
			
                    clear : function(){
                        this.body.update('');
                        this.body.dom.scrollTop = 0;
                    },
				        
                    bbar: [
                    monitorButton,
                    '->',
                    {
                        text: this.updateText,
                        iconCls: 'fugue-arrow-circle',
                        handler: function() {
                            QFinance.service.DeveloperService.getMessages();
                        }
                    },
                    {
                        text: this.clearText,
                        iconCls: 'fugue-broom',
                        handler: function() {
                            logPanel.clear();
                        }
                    },{
                        text: this.closeText,
                        iconCls: 'fugue-door-open-out',
                        handler: function() {
                            win.close();
                        }
                    }]
                })
                ],
                stateful: true
            });
            QFinance.service.DeveloperService.on('print', logPanel.log, logPanel);
        }
        win.show();	    
    }
});