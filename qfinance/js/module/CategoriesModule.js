/*
 * @depends module.js, Window.js
 */
QFinance.module.CategoriesModule = Ext.extend(QFinance.module.Module, {
    // Configuration
    id: 'QFinance.module.CategoriesModule',
    
    // Localization
    categoriesText: 'Categories',
    closeText: 'Close',
    
    init : function(){
        this.launcher = {
            text: this.categoriesText,
            iconCls: 'fugue-category',
            handler: this.createWindow,
            scope: this
        };
    },
    
    createWindow : function(){		
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('QFinance.module.CategoriesModule.createWindow.win');
        if(!win){	
            win = desktop.createWindow({
                id: 'QFinance.module.CategoriesModule.createWindow.win',
                title: this.categoriesText,
                width: 480,
                height: 300,
                iconCls: 'fugue-category',
                animCollapse: false,
                layout: 'fit',	            
                items: [ 
                new QFinance.component.CategoriesEditPanel({
                    canEditCategory: true
                })
                ],
                bbar: [new Ext.Button({
                    text: this.closeText,
                    iconCls: 'fugue-door-open-out',	    		
                    handler: function() {
                        win.close();
                    }
                })],
                stateful: true
            }, QFinance.ui.Window);
        }
        win.show();
    }
});