/*
 * @depends module.js
 */
QFinance.module.ExpensesAndIncomesByCategoryModule = Ext.extend(QFinance.module.Module, {
    // Configuration
    id: 'QFinance.module.ExpensesAndIncomesByCategoryModule',	
	
    // Localization
    expensesAndIncomesByCategoryText: 'Expenses and incomes by category',
    closeText: 'Close',
	    
    init: function() {
        this.launcher = {
            text: this.expensesAndIncomesByCategoryText,
            iconCls: 'fugue-report',
            handler: this.createWindow,
            scope: this
        };
    },
    
    createWindow: function() {		
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('QFinance.module.ExpensesAndIncomesByCategoryModule.createWindow.win');
        var expensesAndIcomesByCategoryPanel;
        if (!win){
            win = desktop.createWindow({
                id: 'QFinance.module.ExpensesAndIncomesByCategoryModule.createWindow.win',
                title: this.expensesAndIncomesByCategoryText,
                width: 700,
                height: 500,
                iconCls: 'fugue-report',
                layout: 'fit',
                items: [  
                expensesAndIcomesByCategoryPanel = new QFinance.component.ExpensesAndIncomesByCategoryPanel()
                ],
                listeners: {
                    scope: this,
                    show: function() {
                        expensesAndIcomesByCategoryPanel.autoLoadData();
                    }
                },
                bbar: [new Ext.Button({
                    text: this.closeText,
                    iconCls: 'fugue-door-open-out',
                    handler: function() {
                        win.close();
                    }
                })],
                stateful: true
            }, QFinance.ui.Window);
        }
        win.show();
    }
});