/*
 * @depends module.js
 */
QFinance.module.AccountsModule = Ext.extend(QFinance.module.Module, {
    // Configuration
    id: 'QFinance.module.AccountsModule',
	
    // Localization
    accountsText: 'Accounts',
    closeText: 'Close',	
    
    init : function(){
        this.launcher = {
            text: this.accountsText,
            iconCls: 'fugue-wallet',
            handler: this.createWindow,
            scope: this
        };
    },
    
    createWindow : function(){		
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('QFinance.module.AccountsModule.createWindow.win');
        var accountsEditPanel;
        if(!win){	    	
            win = desktop.createWindow({
                id: 'QFinance.module.AccountsModule.createWindow.win',
                title: this.accountsText,
                width: 480,
                height: 300,
                iconCls: 'fugue-wallet',
                animCollapse: false,
                layout: 'fit',	            
                items: [ accountsEditPanel = new QFinance.component.AccountsEditPanel() ],
                listeners: {
                    show: function() {
                        accountsEditPanel.autoLoadData();
                    }
                },
                bbar: [new Ext.Button({
                    text: this.closeText,
                    iconCls: 'fugue-door-open-out',	
                    handler: function() {
                        win.close();				
                    }
                })],
                stateful: true
            }, QFinance.ui.Window);
        }
        win.show();
    }
});