/*
 * @depends module.js
 */
QFinance.module.AnnotatedTransactionsModule = Ext.extend(QFinance.module.Module, {
    // Configuration
    id: 'QFinance.module.AnnotatedTransactionsModule',	
	
    // Localization
    transactionsText: 'Annotated transactions',
    selectAccountText: 'Select an account:',
    closeText: 'Close',
    
    // Components
    transactionsEditPanel: null,
    accountStore: null,
    
    init : function(){
        this.launcher = {
            text: this.transactionsText,
            iconCls: 'fugue-sticky-notes',
            handler: this.createWindow,
            scope: this
        };        
    },
    createWindow : function(){		
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('QFinance.module.AnnotatedTransactionsModule.createWindow.win');
        if (!win){
            this.accountStore = new Ext.data.DirectStore({    		
                directFn: QFinance.Remoting.AccountAction.getByAccountType,
                paramsAsHash: false,
                autoLoad: true,
                paramOrder: [ 'type' ],
                baseParams: {
                    type: '1'
                },
                idProperty: 'id',
                root: 'data',
                successProperty: 'success',
                fields: [
                {
                    name: 'id',
                    type: 'int'
                },
                {
                    name: 'accountName'	    		    
                },
                {
                    name: 'accountType'
                }
                ],
                listeners: {
                    scope: this,
                    beforeload: function(store, options) {
                        store.id = win.getProcessStatusManager().start([[win.topToolbar]]);
                    },				
                    load: function(store, records, options) {
                        win.getProcessStatusManager().success(store.id);
                    },
                    exception: function(dataProxy, type, action, options, response, arg ) {
                        var message = '';
                        var where = '';
                        if (response.result) {
                            message = response.result.message;
                            if (response.result.type == 'exception')
                                where = response.result.where;
                        } else {
                            message = response.message;
                        }
                        win.getProcessStatusManager().error(this.accountStore.id, message);
                        QFinance.service.DeveloperService.log('Store failure: ' + message + ' where\n' + where);
                    }
                }   		
            });
	    	
            win = desktop.createWindow({
                id: 'QFinance.module.AnnotatedTransactionsModule.createWindow.win',
                title: this.transactionsText,
                width: 740,
                height: 480,
                iconCls: 'fugue-sticky-notes',
                animCollapse: false,
                layout: 'fit',
                items: [this.transactionsEditPanel = new QFinance.component.TransactionsEditPanel({                    
                    disabled: true,
                    showAccountOverview: false,
                    showAccountBalance: false,
                    onlyAnnotations: true,
                    canAddTransaction: true,
                    canEditTransaction: true,
                    canRemoveTransaction: true,
                    onlyAnnotation: true,
                    canViewSubtransactions: false,
                    recordDragDrop: true,
                    forceCategorySelection: false
                })],
                tbar: [
                this.selectAccountText,
                {
                    xtype: 'combo',
                    store: this.accountStore,
                    typeAhead: true,
                    triggerAction: 'all',
                    selectOnFocus: true,
                    valueField: 'id',
                    displayField: 'accountName',
                    mode: 'local',
                    listeners: {
                        scope: this,
                        select: function(comboBox, record, index) {	              			
                            this.transactionsEditPanel.contextAccountID = record.get('id');
                            this.transactionsEditPanel.load();
                            this.transactionsEditPanel.enable();
                        }
                    }
                }],
                bbar: [new Ext.Button({
                    text: this.closeText,
                    iconCls: 'fugue-door-open-out',	
                    scope: this,
                    handler: function() {					
                        win.close();					
                    }
                })],
                stateful: true
            }, QFinance.ui.Window);
        }
        win.show();
    }
});