/*
 * @depends module.js
 */
QFinance.module.TestModule = Ext.extend(QFinance.module.Module, {
    id:'test-win',
    init : function(){
        this.launcher = {
            text: 'Test',
            iconCls: 'fugue-burn',
            handler: this.createWindow,
            scope: this
        };
    },
	createWindow : function(){		
	    var desktop = this.app.getDesktop();
	    var win = desktop.getWindow('transaction-win');
	    if(!win){	    	
	    	
	        win = desktop.createWindow({
	            id: 'transaction-win',
	            title: 'Transactions',
	            width: 740,
	            height: 480,
	            iconCls: 'fugue-burn',
	            animCollapse: false,
	            layout: 'fit',
	            items: [new QFinance.component.TransactionsEditPanel({
	            	contextAccountID: 1,
					autoLoadTransactions: true
	            })]
	        });
	    }
	    win.show();
	}
});