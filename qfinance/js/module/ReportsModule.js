/*
 * @depends module.js
 */
QFinance.module.ReportsModule = Ext.extend(QFinance.module.Module, {
    // Configuration
    id: 'QFinance.module.ReportsModule',
	
    // Localization
    reportsText: 'Reports',
	
    // Others
    modules: null,
	
    init : function(){
        this.launcher = {
            text: this.reportsText,
            iconCls: 'fugue-reports',
            handler: function() {
                return false;
            },
            menu: {
                items: []
            }
        };
        this.modules = this.getModules();
        if (this.modules)
            this.initModules(this.modules);  
    },
    
    getModules : function() {
        return [
        new QFinance.module.ExpensesAndIncomesByCategoryModule()
        ];
    },
    
    initModules : function(ms) {
        for(var i = 0, len = ms.length; i < len; i++){
            var m = ms[i];
            this.launcher.menu.items.push(m.launcher);
            m.app = this.app;
        }
    },
    
    setApp : function(app) {   
        var ms = this.modules;
        for(var i = 0, len = ms.length; i < len; i++){
            var m = ms[i];
            m.setApp(app);            
        }    	
        QFinance.module.ReportsModule.superclass.setApp.call(this,app);
    }
});