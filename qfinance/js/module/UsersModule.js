/*
 * @depends module.js
 */
QFinance.module.UsersModule = Ext.extend(QFinance.module.Module, {
    // Configuration
    id: 'QFinance.module.UsersModule',
	
    // Localization
    usersText: 'Users',
    closeText: 'Close',
	    
    init : function(){
        this.launcher = {
            text: this.usersText,
            iconCls: 'fugue-users',
            handler: this.createWindow,
            scope: this
        };
    },
    
    createWindow : function(){		
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('QFinance.module.UsersModule.createWindow.win');
        if(!win){	
            win = desktop.createWindow({
                id: 'QFinance.module.UsersModule.createWindow.win',
                title: this.usersText,
                width: 280,
                height: 300,
                iconCls: 'fugue-users',
                animCollapse: false,
                layout: 'fit',	            
                items: [ new QFinance.component.UsersEditPanel() ],
                bbar: [new Ext.Button({
                    text: this.closeText,
                    iconCls: 'fugue-door-open-out',	
                    handler: function() {
                        win.close();				
                    }
                })],
                stateful: true
            }, QFinance.ui.Window);
        }
        win.show();
    }
});