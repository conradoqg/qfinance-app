/**
 * @depends qfinance.js
 */
QFinance.LoginWindow = Ext.extend( Ext.Window, {
    isLogged : false,
    
    // Localization
    failureText: 'Failure',
    invalidText: 'Invalid username and/or password.',
    usernameText: 'Username',
    passwordText: 'Password',
    loginText: 'Enter',
    waitText: 'Wait',
    waitMsgText: 'Authenticating...',
    title: 'Login',
    
    submitHandler : function(b, e){
        var loginForm = Ext.getCmp('loginForm');
        loginForm.getForm().submit({
            waitTitle: this.waitText,
            waitMsg: this.waitMsgText,
            success: this.validateLogin,
            failure: this.showFailureMessage,
            scope: this
        }, this);
    },
	
    constructor: function(config) {
        Ext.apply(this, config);
        Ext.apply(this,{			
            width: 250,
            layout: 'fit',
            autoHeight: true,
            closable: false,
            closeAction: 'close',
            items: [ {
                xtype: 'form',
                id: 'loginForm',
                autoHeight: true,
                labelWidth: 60,	            
                frame: true,
                defaultType: 'textfield',
                monitorValid: true,
                items: [
                {
                    id: 'QFinance.LoginWindow.LoginForm.Username',
                    fieldLabel: this.usernameText,
                    allowBlank: false,
                    name: 'username'
                },
                {
                    fieldLabel: this.passwordText,
                    inputType: 'password',
                    allowBlank: false,
                    name: 'password'	                    
                }
                ],
                buttons: [{
                    text: this.loginText,
                    formBind: true,
                    scope: this,	            	
                    handler: this.submitHandler  	
                }],
                api: {
                    submit: QFinance.Remoting.LoginAction.login
                },
                keys: [
                {
                    key: [Ext.EventObject.ENTER], 
                    handler: this.submitHandler, 
                    scope: this
                }
                ]
            }
            ]		
        });
		
        this.addEvents({
            'logged' : true
        });	
	    
        QFinance.LoginWindow.superclass.constructor.call(this);
    },
	
    validateLogin: function(form, action) {
        if (action.result.logged) {
            this.isLogged = true;
            this.fireEvent('logged', this);
            this.destroy();
        } else {
            Ext.Msg.alert(this.failureText, this.invalidText);
        }
    },
	
    showFailureMessage: function(form, action) {
        var message = '';
        var where = '';
        switch (action.failureType) {			
            case Ext.form.Action.CLIENT_INVALID:
                message = 'Form fields may not be submitted with invalid values';			    
                break;
            case Ext.form.Action.CONNECT_FAILURE:
                message = 'Ajax communication failed';
                break;
            case Ext.form.Action.SERVER_INVALID:
                message = action.result.message;
                if (action.result.type == 'exception')
                    where = action.result.where;
                break;
        }
        QFinance.service.DeveloperService.log('Submit failure: ' + message + ' where\n' + where);
    },
	
    onLogged : function(fn, scope){
        if (this.isLogged) {
            this.on( 'logged', fn, scope );
        } else {
            fn.call(scope, this);
        }
    },
	
    onShow: function() {
        Ext.Window.superclass.onShow.call(this);
        Ext.getCmp('QFinance.LoginWindow.LoginForm.Username').focus(true,10);
    }
});