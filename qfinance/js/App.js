/*
 * @depends *
 */
QFinance.App = Ext.extend(Ext.util.Observable, {
    isReady: false,
    startMenu: null,
    modules: null,    
    bypassLogin: false,
    
    // Localization
    usersText: 'Users',
    settingsText: 'Settings',
    logoutText: 'Logout',
    menuWidth: 350,
    
    constructor: function(cfg) {
        Ext.apply(this, cfg);
        this.addEvents({
            'ready' : true,
            'beforeunload' : true
        });
        QFinance.App.superclass.constructor.call(this);
    },

    // config for the start menu
    getStartConfig : function(){
        return {
            title: 'QFinance',
            iconCls: 'fugue-user',  
            width: this.menuWidth,
            toolItems: [{
                text: this.usersText,
                iconCls: 'fugue-users',
                scope: this,
                handler: function() {
                    var module = new QFinance.module.UsersModule();
                    module.setApp(this);
                    module.createWindow();
                }
            },{
                text: this.settingsText,
                iconCls: 'fugue-gear',
                scope: this,
                handler: function() {
                    Ext.Msg.alert('Alert', 'Not implemented yet!.');
                }
            },"-",{
                text: this.logoutText,
                iconCls: 'fugue-key',
                scope: this,
                handler: this.logout
            }]
        };
    },

    initApp : function(){
        var init = function() {
            this.startConfig = this.startConfig || this.getStartConfig();			
						
            this.desktop = new Ext.Desktop(this);
	
            this.launcher = this.desktop.taskbar.startMenu;
	
            this.modules = this.getModules();
            if(this.modules)
                this.initModules(this.modules);
	
            this.init();
	        
            this.initPortal();
	
            Ext.EventManager.on(window, 'beforeunload', this.onUnload, this);
            this.fireEvent('ready', this);
            this.isReady = true;
        };
		
        this.configure(function() {
            if (!QFinance.service.DeveloperService.bypassLogin) {
                var loginWindow = new QFinance.LoginWindow( {
                    listeners: {
                        scope: this,
                        logged : init 
                    }
                });
                loginWindow.show();
            } else {
                init.call(this);
            }	
        }, this);
    },

    getModules : function(){
        return [
        new QFinance.module.OverviewModule(),
        new QFinance.module.TransactionsModule(),
        new QFinance.module.AnnotatedTransactionsModule(),
        new QFinance.module.AccountsModule(),
        new QFinance.module.CategoriesModule(),
        new QFinance.module.MarksModule(),
        new QFinance.module.TransactionsImportModule(),
        new QFinance.module.ReportsModule(),
        new QFinance.module.TestModule(),
        new QFinance.module.DeveloperConsoleModule()
        ];
    },
    init : Ext.emptyFn,

    initModules : function(ms){
        for(var i = 0, len = ms.length; i < len; i++){
            var m = ms[i];
            this.launcher.add(m.launcher);
            m.setApp(this);
        }
    },

    getModule : function(name){
        var ms = this.modules;
        for(var i = 0, len = ms.length; i < len; i++){
            if(ms[i].id == name || ms[i].appType == name){
                return ms[i];
            }
        }
        return '';
    },

    onReady : function(fn, scope){
        if(!this.isReady){
            this.on('ready', fn, scope);
        }else{
            fn.call(scope, this);
        }
    },

    getDesktop : function(){
        return this.desktop;
    },

    onUnload : function(e){
        if(this.fireEvent('beforeunload', this) === false){
            e.stopEvent();
        }
    },
    
    configure : function(callback, scope) {
        Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
        Ext.QuickTips.init();		
        Ext.Direct.on('event', function(e, provider) {
            if (e.action != 'DeveloperServiceAction')
                QFinance.service.DeveloperService.log('Called action: \'' + e.action + '\' method: \'' + e.method + '\'');
        });
        QFinance.service.DeveloperService.load(function() {
            if (callback) callback.call(scope||this);
        },this);
    },
    
    logout: function() {
        QFinance.Remoting.LoginAction.logout(function(result, event) {
            var message = '';
            var where = '';
            var failure = false;
    		
            if (result) {
                if (result.success) {
                    window.location.reload();		
                } else {
                    failure = true;						
                    message = result.message;
                    if (result.type == 'exception')
                        where = result.where;
                }            
            } else {
                failure = true;
                message = event.message;
            }
    		
            if (failure) {	
                QFinance.service.DeveloperService.log('Direct call failure: ' + message + ' where\n' + where);
                Ext.Msg.alert('Logout', message);
            }

        }, this);
    },
    
    initPortal: function() {
    	
    	
       
    }
});