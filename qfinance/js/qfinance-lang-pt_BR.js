Ext.apply(QFinance, {
    dateFormat: 'd/m/Y',
    decimalSeparator: ',',
    moneySymbol: 'R$',
    numberFormat: '0,0.0/i',
    loadingText: 'Carregando...'
});

// For mobile app
if (Ext.setup) {
    Ext.LoadMask.prototype.msg = 'Carregando...';
    Ext.util.Format.defaultDateFormat = QFinance.dateFormat;
        
    Ext.apply(QFinance.AppMobile.prototype, {
        newExpenseAnnotationText: 'Nova anotação de despesa',
        requiredText: 'Por favor entre com as informações acima.',
        accountText: 'Conta',
        dateText: 'Data',
        descriptionText: 'Descrição',
        amountText: 'Montante',
        destinationText: 'Destino',
        categoryText: 'Categoria',
        resetText: 'Limpar',
        addText: 'Adicionar',
        fillRequiredText: 'Por favor, preencha os campos obrigatórios',
        internalServerErrorText: 'Erro interno do servidor.'
    });
// For default app
} else {
    Ext.apply( QFinance.component.AccountEditWindow.prototype, {
        defaultTitleText: 'Editando uma nova conta',
        editingTitleText: 'Editando conta: {0}',
        idText: 'ID',
        accountNameText: 'Nome da conta',
        bankNumberText: 'Número do banco',
        bankAgencyText: 'Agência do banco',
        bankAccountText: 'Conta do banco',
        initialAmountText: 'Montante inicial',
        accountTypeText: 'Tipo',
        accountTypeEmptyText: 'Selecione um tipo',
        saveText: 'Salvar',
        closeText: 'Fechar',
        privateText: 'Privado',
        publicText: 'Público',
        labelWidth: 120
    });
        
    Ext.apply(QFinance.component.AccountOverviewPanel.prototype, {
        balanceText: 'Balanço',
        statisticsText: 'Estatísticas'
    });

    Ext.apply(QFinance.component.AccountsEditPanel.prototype, {
        addAccountText: 'Adicionar',
        addAccountTooltipText: 'Adicionar uma nova conta',
        editAccountText: 'Editar',
        editAccountTooltipText: 'Editar conta',
        removeAccountText: 'Remover',
        removeAccountTooltipText: 'Remover conta',
        accountText: 'Conta',
        bankNumberText: 'Número do banco',
        bankAgencyText: 'Agência do banco',
        bankAccoutText: 'Conta do banco',
        initialAmountText: 'Montante inicial',
        accountTypeNameText: 'Tipo',
        removeQuestionTitle: 'Remover conta',
        removeQuestionMsg: 'Você deseja remover as contas selecionadas?',
        editInformationTitle: 'Editar conta',
        editInformationMsg: 'Por favor selecione uma conta para editar.',
        removeInformationTitle: 'Remover conta',
        removeInformationMsg: 'Por favor selecione uma conta para remover.'
    });

    Ext.apply(QFinance.component.CategoriesEditPanel.prototype, {
        addCategoryText: 'Adicionar',
        addCategoryToolTipText: 'Adicionar uma nova categoria',
        editCategoryText: 'Editar',
        editCategoryToolTipText: 'Editar categoria selecionada',
        removeCategoryText: 'Remover',
        removeCategoryToolTipText: 'Remover categoria selecionada',
        categoriesText: 'Categorias',
        removeQuestionTitle: 'Remover categoria',
        removeQuestionMsg: 'Você deseja remover a categoria selecionada?',
        editInformationTitle: 'Editar categoria',
        editInformationMsg: 'Por favor selecione uma categoria para editar.',
        addInformationTitle: 'Adicionar categoria',
        addInformationMsg: 'Por favor selecione uma categoria ou o nó principal.',
        removeInformationTitle: 'Remover categoria',
        removeInformationMsg: 'Por favor selecione uma categoria para remover.'
    });
	
    Ext.apply(QFinance.component.CategoryEditWindow.prototype, {
        defaultTitleText: 'Editando uma nova categoria',
        editingTitleText: 'Editando categoria: {0}',
        idText: 'ID',
        categoyNameText: 'Nome',
        saveText: 'Salvar',
        closeText: 'Fechar'            
    });
	
    Ext.apply(QFinance.component.ExpensesAndIncomesByCategoryPanel.prototype, {
        updateText: 'Atualizar',
        optionsText: 'Opções',
        accountText: 'Conta',
        allTimeText: 'Tudo',
        past30DaysText: 'Últimos 30 dias',
        past60DaysText: 'Últimos 60 dias',
        thisMonthText: 'Esse mês',
        thisYearText: 'Esse ano',
        customText: 'Personalizado',
        rangeText: 'Intervalo',
        bothText: 'Ambos',
        incomeText: 'Renda',
        expenseText: 'Despesa',
        typeText: 'Tipo',
        emptyResultText: 'Não há transações dentro das opções selecionadas'            
    });
    
    Ext.apply(QFinance.component.MarkEditWindow.prototype, {
        defaultTitleText: 'Editando uma nova marca',
        editingTitleText: 'Editando marca: {0}',
        idText: 'ID',
        nameText: 'Nome',
        descriptionText: 'Descrição',
        saveText: 'Salvar',
        closeText: 'Fechar',
        labelWidth: 65
    });
    
    Ext.apply(QFinance.component.MarksEditPanel.prototype, {
        addText: 'Adicionar',
        addTooltipText: 'Adicionamento uma nova marca',
        editText: 'Editar',
        editTooltipText: 'Editar marca',
        removeText: 'Remover',
        removeTooltipText: 'Remover marca',
        markText: 'Marca',
        descriptionText: 'Descrição',    
        removeQuestionTitle: 'Remover marca',
        removeQuestionMsg: 'Você deseja remover a marca selecionada?',
        editInformationTitle: 'Editar marca',
        editInformationMsg: 'Por favor selecione uma marca para editar.',
        removeInformationTitle: 'Remover marca',
        removeInformationMsg: 'Por favor selecione uma marca para remover.'
    });
        
    Ext.apply(QFinance.component.OverviewPanel.prototype, {
        addPortletText: 'Adicionar portlet',
        selectPortletText: 'Selecionar portlet',
        accountsBalanceText: 'Balanço de contas',
        topAmountText: 'Top montante',
        accountsBalanceHistory: 'Histórico do balanço das contas',
        columnText: 'Coluna',
        addColumnText: 'Adicionar',
        removeColumnText: 'Remover',
        saveText: 'Salvar'            
    });
	
    Ext.apply(QFinance.component.TransactionEditWindow.prototype, {
        saveText: 'Salvar',
        closeText: 'Fechar',
        idText: 'ID',
        dateText: 'Data',
        accountEmptyText: 'Selecione uma conta',
        sourceAccountText: 'Conta de origem',
        destinationAccountText: 'Conta de destino',
        descriptionText: 'Descrição',
        bankMemoText: 'Banco memo',
        amountText: 'Montante',
        defaultTitleText: 'Editar transação',
        editingTitleText: 'Editando transação: {0}',
        pleaseSelectAnCategory: 'Por favor, selecione uma categoria.',
        fillRequiredFields: 'Por favor, preencha os campos obrigatórios.',
        removeTransactionTitleText: 'Arrastar e soltar',
        removeTransactionText: 'Remover transação anotada?',
        formValidationTitleText: 'Validação do formulário',
        reloadDataText: 'Recarregar dados',
        categoryText: 'Categoria',
        markText: 'Marca',
        markEmptyText: 'Selecione uma marca'
    });
                
    Ext.apply(QFinance.component.TransactionMultiEditWindow.prototype, {
        saveText: 'Salvar',
        closeText: 'Fechar',
        accountEmptyText: 'Selecione uma conta',
        sourceAccountText: 'Conta de origem',
        destinationAccountText: 'Conta de destino',
        defaultTitleText: 'Editar múltiplas transações',
        pleaseSelectAnCategory: 'Por favor, selecione uma categoria.',
        fillRequiredFields: 'Por favor, preencha os campos obrigatórios.',
        formValidationTitleText: 'Validação do formulário',
        reloadDataText: 'Recarregar dados',
        categoryText: 'Categoria'            
    });
	
    Ext.apply(QFinance.component.TransactionsEditPanel.prototype, {
        subtransactionsText: 'Subtransações',
        addTransactionText: 'Adicionar',
        editTransactionText: 'Editar',
        removeTransactionText: 'Remover',
        recordsTotalText: 'Montante corrente: {0}',
        totalAmountText: 'Montante total: {0}',	
        dateText: 'Data',
        descriptionText: 'Descrição',
        bankMemoText: 'Banco memo',
        accountText: 'Conta',
        amountText: 'Montante',
        categoryText: 'Categoria',
        markText: 'Marca',
        subtransactionsWindowTitleText: 'Subtransações da transação: {0}',
        totalAmountIsntEqualTransactionsAmountText: 'Enquanto o valor total das transações não for igual a transação pai, a transação não será salva.',
        accountOverviewText: 'Resumo da conta',
        accountBalanceText: 'Balanço da conta:',
        removeQuestionTitle: 'Remover transação',
        removeQuestionMsg: 'Você deseja remover as transações selecionadas?',
        editInformationTitle: 'Editar transação',
        editInformationMsg: 'Por favor selecione uma transação para editar.',
        multiEditInformationTitle: 'Editar múltiplas transações',
        multiEditInformationMsg: 'Somente contas com a mesma origem/destino podem ser multi-editadas.',
        viewSubstransactionInformationTitle: 'Visualizar subtransações',
        viewSubtransactionsInformationMsg: 'Por favor selecione uma transação.',
        removeInformationTitle: 'Remove transação',
        removeInformationMsg: 'Por favor selecione uma transação para remover.',	
        saveInformationTitle: 'Salvar informação'
    });
	
    Ext.apply(QFinance.component.TransactionsEditWindow.prototype, {
        closeText: 'Fechar'
    });	
	
    Ext.apply(QFinance.component.TransactionsImportPanel.prototype, {
        welcomeText: 'Bem-vindo, bla bla bla',
        bankText: 'Banco',
        bankEmptyText: 'Selecione um banco...',
        fileText: 'Arquivo',
        fileEmptyText: 'Selecione um arquivo',
        accountText: 'Conta',
        accountEmptyText: 'Selecione uma conta',
        finishButtonText: 'Finalizar', 
        finishText: '{0} transações estão prontas para serem importadas para {1}, clique \'{2}\' para deixá-las permanentes.'            
    });

    Ext.apply(QFinance.component.UserEditWindow.prototype, {
        defaultTitleText: 'Editando novo usuário',
        editingTitleText: 'Editando usuário: {0}',
        idText: 'ID',
        userNameText: 'Nome do usuário',
        passwordText: 'Senha',
        passwordCheckText: 'Redigite a senha',
        saveText: 'Salvar',
        closeText: 'Fechar'            
    });
	
    Ext.apply(QFinance.component.UsersEditPanel.prototype, {
        addUserText: 'Adicionar',
        addUserTooltipText: 'Adicionar novo usuário',
        editUserText: 'Editar',
        editUserTooltipText: 'Editar usuário',
        removeUserText: 'Remover',
        removeUserTooltipText: 'Remover usuário',
        userText: 'Usuário',
        removeQuestionTitle: 'Remover usuário',
        removeQuestionMsg: 'Você deseja remover os usuários selecionados?',
        editInformationTitle: 'Editar usuário',
        editInformationMsg: 'Por favor selecione um usuário para editar.',
        removeInformationTitle: 'Remover usuário',
        removeInformationMsg: 'Por favor selecione um usuário para remover.'            
    });
	
    Ext.apply(QFinance.module.AccountsModule.prototype, {
        accountsText: 'Contas',
        closeText: 'Fechar'    
    });
	
    Ext.apply(QFinance.module.AnnotatedTransactionsModule.prototype, {
        transactionsText: 'Transações anotadas',
        selectAccountText: 'Selecione uma conta:',
        closeText: 'Fechar'
    });
    
    Ext.apply(QFinance.module.CategoriesModule.prototype, {
        categoriesText: 'Categorias',
        closeText: 'Fechar'
    });
	
    Ext.apply(QFinance.module.DeveloperConsoleModule.prototype, {
        developerConsoleText: 'Console do desenvolvedor',
        startText: 'Iniciar',
        stopText: 'Parar',
        updateText: 'Atualizar',
        clearText: 'Limpar',
        closeText: 'Fechar'        
    });
    	
    Ext.apply(QFinance.module.ExpensesAndIncomesByCategoryModule.prototype, {
        expensesAndIncomesByCategoryText: 'Rendas e despesas por categoria',
        closeText: 'Fechar'
    });
    
    Ext.apply(QFinance.module.MarksModule.prototype, {
        marksText: 'Marcas',
        closeText: 'Fechar'
    });
        
    Ext.apply(QFinance.module.OverviewModule.prototype, {
        overviewText: 'Visão geral',
        closeText: 'Fechar'
    });

    Ext.apply(QFinance.module.ReportsModule.prototype, {
        reportsText: 'Relatórios'
    });    
	
    Ext.apply(QFinance.module.TransactionsImportModule.prototype, {
        closeText: 'Fechar',
        importText: 'Importar'
    });
    
    Ext.apply(QFinance.module.TransactionsModule.prototype, {
        transactionsText: 'Transações',
        selectAccountText: 'Selecione uma conta:',
        closeText: 'Fechar'
    });        
	
    Ext.apply(QFinance.module.UsersModule.prototype, {
        usersText: 'Usuários',
        closeText: 'Fechar'
    });
    
    Ext.apply(QFinance.portlet.AccountsBalanceHistoryConfigurationWindow.prototype, {
        title: 'Configuração do portlet histórico do balanço das contas',	
        accountText: 'Conta',
        allTimeText: 'Tudo',
        past30DaysText: 'Últimos 30 dias',
        past60DaysText: 'Últimos 60 dias',
        thisMonthText: 'Esse mês',
        thisYearText: 'Esse ano',
        customText: 'Personalizado',
        rangeText: 'Intervalo',
        bothText: 'Ambos',
        incomeText: 'Renda',
        expenseText: 'Despesa',
        typeText: 'Tipo',	
        saveText: 'Salvar',
        closeText: 'Fechar'
    });
        
    Ext.apply(QFinance.portlet.AccountsBalanceHistoryPortlet.prototype, {
        title: 'Histórico do balanço das contas',
        accountText: 'Conta',
        rangeText: 'Intervalo',
        typeText: 'Tipo',
        amountText: 'Montante'
    });
    
    Ext.apply(QFinance.portlet.AccountsBalancePortlet.prototype, {
        title: 'Balanço de contas',	
        accountText: 'Conta',
        amountText: 'Montante'
    });
        	
    Ext.apply(QFinance.portlet.TopAmountPortletConfigurationWindow.prototype, {
        labelWidth: 120,
        title: 'Configuração do portlet top montante',	
        amountTypeText: 'Tipo de montante',
        amountTypeEmptyText: 'Selecione um tipo',
        categoryText: 'Categoria',
        accountText: 'Conta',
        transactionText: 'Transação',
        markText: 'Marca',
        saveText: 'Salvar',
        closeText: 'Fechar'
    });
        
    Ext.apply(QFinance.portlet.TopAmountPortlet.prototype, {
        titleText: 'Top',
        amountText: 'Montante',
        nameText: 'Nome',
        categoryText: 'categoria',
        accountText: 'conta',
        transactionText: 'transação',
        markText: 'marca'
    });

    Ext.apply(QFinance.ui.MessageBox.prototype, {
        buttonText: {
            ok     : "OK",
            cancel : "Cancelar",
            yes    : "Sim",
            no     : "Não"
        }
    });
    
    Ext.apply(QFinance.ui.SelectWindow.prototype, {
        title: 'Selecione uma opção',
        OKText: 'OK',
        cancelText: 'Cancelar'
    });
        
    Ext.apply(QFinance.ui.Window.prototype, {
        waitingMessageSingular: 'Processando {0} item.',
        waitingMessagePlural: 'Processando {0} itens.',
        errorMessageTitle: 'Erro no processamento',
        infoMessageTitle: 'Informação do processamento'
    });
        
    Ext.apply(QFinance.ui.WizardPanel.prototype, {
        navigatorNextText: 'Próximo',
        navigatorPreviousText: 'Anterior',
        navigatorFinishText: 'Finalizar'
    })
        	
    Ext.ux.TaskBar.prototype.startText = 'Iniciar';
	
    Ext.apply(QFinance.App.prototype, {
        usersText: 'Usuários',
        settingsText: 'Configurações',
        logoutText: 'Desconectar'        
    });

    Ext.ux.StartMenu.prototype.tPanelWidth = 130;    

    Ext.apply(QFinance.LoginWindow.prototype, {
        title: 'Entrar',
        usernameText: 'Usuário',
        passwordText: 'Senha',
        failureText: 'Falha',
        invalidText: 'Usuário e/ou senha inválidos.',
        loginText: 'Entrar',
        waitText: 'Aguarde',
        waitMsgText: 'Autenticando...'
    });
        	    
    Highcharts.setOptions({
        lang: {
            months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            weekdays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            printButtonTitle: 'Imprimir gráfico',
            resetZoom: 'Restaurar zoom',
            resetZoomTitle: 'Restaurar zoom nível 1:1',
            thousandsSep: '.',
            decimalPoint: ','
        }
    });
}