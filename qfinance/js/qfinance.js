Ext.namespace("QFinance");

QFinance.dateFormat = 'Y-m-d';
QFinance.decimalSeparator = '.';
QFinance.moneySymbol = 'U$';
QFinance.numberFormat = '0,000.00';
QFinance.loadingText = 'Loading...';