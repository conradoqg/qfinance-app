/*
 * @depends portlet.js
 */
QFinance.portlet.PortletFactory = Ext.extend( Object, {
	getPortletInstanceByClass: function( className ) {
		var portlet = eval('new QFinance.portlet.' + className + '()');
		
		if (portlet)
			portlet.className = className;
		
		return portlet;		
	}
});

QFinance.portlet.PortletFactory = new QFinance.portlet.PortletFactory();