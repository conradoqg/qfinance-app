/**
 * @depends portlet.js, Window.js
 */
QFinance.portlet.TopAmountPortletConfigurationWindow = Ext.extend( QFinance.ui.Window, {
    formPanel: null,
    comboBoxTopType: null,
    configuration: {
        topType: 1
    },
	
    // Localization
    labelWidth: 90,
    title: 'Top amount portlet configuration',	
    amountTypeText: 'Amount type',
    amountTypeEmptyText: 'Select an type',
    categoryText: 'Category',
    accountText: 'Account',
    transactionText: 'Transaction',
    markText: 'Mark',
    saveText: 'Save',
    closeText: 'Close',
	
    constructor: function(cfg) {
        Ext.apply(this, cfg);
		
        this.addEvents(['save']);
		
        QFinance.portlet.TopAmountPortletConfigurationWindow.superclass.constructor.call(this);
    },
	
    initComponent: function() {	
        Ext.apply(this, {
            width: 300,
            items: [ this.formPanel = new Ext.form.FormPanel({
                frame: true,
                autoHeight: true,
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                labelWidth: this.labelWidth,
                items: [ this.comboBoxTopType = new Ext.form.ComboBox({
                    xtype: 'combo',
                    fieldLabel: this.amountTypeText,
                    name: 'amountType',    		
                    typeAhead: true,
                    triggerAction: 'all',
                    emptyText: this.amountTypeEmptyText,
                    selectOnFocus: true,
                    forceSelection: true,
                    allowBlank: false,
                    value: this.configuration.topType,
                    store: [
                    ['1', this.categoryText],
                    ['2', this.accountText],
                    ['3', this.transactionText],
                    ['4', this.markText]
                    ]    	     
                })]
            })]
        });
        QFinance.portlet.TopAmountPortletConfigurationWindow.superclass.initComponent.call(this);
    },
	
    initStatusBar: function(toolbar) {
        toolbar.add(new Ext.Button({
            text: this.saveText,
            formBind: true,
            scope: this,
            iconCls: 'fugue-disk-black',
            handler: function() {
                if (this.formPanel.getForm().isValid()) {
                    this.doSave();	 
                    this.close();
                }
            }
        }));
				
        toolbar.add(new Ext.Button({
            text: this.closeText,
            iconCls: 'fugue-door-open-out',				
            scope: this,
            handler: function() {
                this.close();
            }
        }));
    },
	
    doSave: function() {
        this.configuration.topType = this.comboBoxTopType.getValue();			
        this.fireEvent('save', this.configuration);
    }
});
Ext.reg('QFinance.portlet.TopAmountPortletConfigurationWindow', QFinance.portlet.TopAmountPortletConfigurationWindow);