/*
 * @depends Portlet.js,portlet.js
 */
QFinance.portlet.TopAmountPortlet = Ext.extend( QFinance.ui.Portlet, {
    topAmountRecord: null,	
    configuration: {
        topType: null
    },
    configurationWindow: 'TopAmountPortletConfigurationWindow',
	
    // Localization
    titleText: 'Top',
    amountText: 'Amount',
    nameText: 'Name',
    categoryText: 'category',
    accountText: 'account',
    transactionText: 'transaction',
    markText: 'mark',
	
    constructor: function(cfg) {
        Ext.apply(this, cfg);		
        QFinance.portlet.TopAmountPortlet.superclass.constructor.call(this);
    },
	
    initComponent: function() {				
        this.createTopAmountRecord();
        this.initStore();
        this.initGrid();
        Ext.apply(this, {
            items: [ this.grid ]
        });
        QFinance.portlet.TopAmountPortlet.superclass.initComponent.call(this);
    },
	
    initStore: function() {
        this.store = new Ext.data.DirectStore({
            autoSave: false,
            autoLoad: false,		    
            idProperty: 'id',
            root: 'data',
            successProperty: 'success',  
            paramsAsHash: false,
            paramOrder: [ 'topType' ],
            baseParams: {
                topType: this.configuration.topType
            },
            api: {
                read: QFinance.Remoting.TopAmountPortletAction.read
            },
            fields: this.topAmountRecord,
            listeners: {
                scope: this,
                beforeload: function(store, options) {
                    this.body.mask(QFinance.loadingText, 'x-mask-loading');		
                },				
                load: function(store, records, options) {
                    this.body.unmask();
                },				
                exception: function(dataProxy, type, action, options, response, arg ) {
                    var message = '';
                    var where = '';
                    if (response.result) {
                        message = response.result.message;
                        if (response.result.type == 'exception')
                            where = response.result.where;
                    } else {
                        message = response.message;
                    }
                    this.body.unmask();
                    this.body.mask(message);					
                    QFinance.service.DeveloperService.log('Store failure: ' + message + ' where\n' + where);					
                }
            }			
        });
    },
	
    initGrid: function() {
        this.grid = new Ext.grid.GridPanel({				
            store: this.store,				
            autoHeight: true,
            columns: [
            {
                id       :'topName',
                header   : this.nameText, 
                sortable : true, 
                dataIndex: 'topName'
            },{
                id       :'amount',
                header   : this.amountText, 
                sortable : true, 
                dataIndex: 'amount',
                scope: this,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    return QFinance.moneySymbol + ' ' + Ext.util.Format.number(value,QFinance.numberFormat);	            		
                }
            }],
            stripeRows: true,
            autoExpandColumn: 'topName',
            sm: new Ext.grid.RowSelectionModel()	        
        });
    },
	
    createTopAmountRecord: function() {
        this.topAmountRecord = Ext.data.Record.create([{
            name: 'topName'
        },{
            name: 'amount',	
            type: 'float'
        }]);
    },
	
    loadData: function() {
        this.setTitle(this.titleText + ' ' + this.getTopName(this.configuration.topType));
        this.store.setBaseParam('topType', this.configuration.topType);
        this.store.load();
    },
	
    onLayout: function() {
        this.grid.getView().refresh();
        QFinance.portlet.TopAmountPortlet.superclass.onLayout.call(this);
    },
	
    onRender: function(ct, position) {
        QFinance.portlet.TopAmountPortlet.superclass.onRender.call(this, ct, position);		
    },
	
    getTopName: function( topType ) {
        var topName = '';
		
        switch (parseInt(topType)) {
            case 1:
                topName = this.categoryText;
                break;
            case 2:
                topName = this.accountText;
                break;
            case 3:
                topName = this.transactionText;
                break;
            case 4:
                topName = this.markText;
                break;
        }
        return topName;
    }
});