/*
 * @depends Portlet.js,portlet.js
 */
QFinance.portlet.AccountsBalancePortlet = Ext.extend( QFinance.ui.Portlet, {
    store: null,
    accountBalanceRecord: null,
	
    // Localization
    title: 'Accounts balance',	
    accountText: 'Account',
    amountText: 'Amount',
	
    constructor: function(cfg) {
        Ext.apply(this, cfg);				
        QFinance.portlet.AccountsBalancePortlet.superclass.constructor.call(this);
    },
	
    initComponent: function() {
        this.createAccountBalanceRecord();
        this.initStore();
        this.initGrid();
        Ext.apply(this, {
            items: [ this.grid ]
        });
        QFinance.portlet.AccountsBalancePortlet.superclass.initComponent.call(this);
    },

    initStore: function() {
        this.store = new Ext.data.DirectStore({
            autoSave: false,
            autoLoad: false,		    
            idProperty: 'id',
            root: 'data',
            successProperty: 'success',  		
            api: {
                read: QFinance.Remoting.AccountsBalancePortletAction.read
            },
            fields: this.accountBalanceRecord,
            listeners: {
                scope: this,
                beforeload: function(store, options) {
                    this.body.mask(QFinance.loadingText, 'x-mask-loading');		
                },				
                load: function(store, records, options) {
                    this.body.unmask();
                },				
                exception: function(dataProxy, type, action, options, response, arg ) {
                    var message = '';
                    var where = '';
                    if (response.result) {
                        message = response.result.message;
                        if (response.result.type == 'exception')
                            where = response.result.where;
                    } else {
                        message = response.message;
                    }
                    this.body.unmask();
                    this.body.mask(message);					
                    QFinance.service.DeveloperService.log('Store failure: ' + message + ' where\n' + where);					
                }
            }			
        });
    },

	
    initGrid: function() {
        this.grid = new Ext.grid.GridPanel({				
            store: this.store,				
            autoHeight: true,
            columns: [
            {
                id       :'accountName',
                header   : this.accountText, 
                sortable : true, 
                dataIndex: 'accountName'
            },{
                id       :'amount',
                header   : this.amountText, 
                sortable : true, 
                dataIndex: 'amount',
                scope: this,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    if (value >= 0) {
                        return '<span style="color:green;">' + QFinance.moneySymbol + ' ' + Ext.util.Format.number(value,QFinance.numberFormat) + '</span>';
                    } else {
                        return '<span style="color:red;">' + QFinance.moneySymbol + ' ' + Ext.util.Format.number(value,QFinance.numberFormat) + '</span>';
                    }	
                },	            
                summaryType: 'sum',
                summaryRenderer: function(value, params, data) {
                    if (value >= 0) {
                        return '<span style="color:green;">' + QFinance.moneySymbol + ' ' + Ext.util.Format.number(value,QFinance.numberFormat) + '</span>';
                    } else {
                        return '<span style="color:red;">' + QFinance.moneySymbol + ' ' + Ext.util.Format.number(value,QFinance.numberFormat) + '</span>';
                    }	            	
                }
            }],
            stripeRows: true,
            autoExpandColumn: 'accountName',
            sm: new Ext.grid.RowSelectionModel(),
            plugins: [new Ext.ux.grid.GridSummary()]
        });
    },
	
    createAccountBalanceRecord: function() {
        this.accountBalanceRecord = Ext.data.Record.create([{
            name: 'accountName'
        },{
            name: 'amount',	
            type: 'float'
        }]);
    },
	
    loadData: function() {		
        this.store.load();
    },	
	
    onLayout: function() {
        this.grid.getView().refresh();
        QFinance.portlet.AccountsBalancePortlet.superclass.onLayout.call(this);
    },
	
    onRender: function(ct, position) {
        QFinance.portlet.AccountsBalancePortlet.superclass.onRender.call(this, ct, position);		
    }
});