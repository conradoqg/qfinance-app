/**
 * @depends Portlet.js,portlet.js
 */
QFinance.portlet.AccountsBalanceHistoryPortlet = Ext.extend( QFinance.ui.Portlet, {
    configurationWindow: 'AccountsBalanceHistoryConfigurationWindow',	
    configuration: {
        accountID: null,
        range: 2,
        startDate: null,
        endDate: null,
        type: 1
    },
	
    // Localization
    title: 'Accounts balance history',
    accountText: 'Account',
    rangeText: 'Range',
    typeText: 'Type',
    amountText: 'Amount',
		
    constructor: function(cfg) {
        Ext.apply(this, cfg);
        QFinance.portlet.AccountsBalanceHistoryPortlet.superclass.constructor.call(this);
    },
	
    initComponent: function() {		
        this.initStore();		
        Ext.apply(this, {
            layout: 'vbox',
            layoutConfig: {
                align : 'stretch',
                pack  : 'start'
            },
            height: 300,
            items: [{
                xtype: 'form',
                autoHeight: true,
                labelWidth: 60,
                style: 'padding: 10px 10px 0px 10px',
                defaults: {
                    anchor: '100%'
                },
                items: [ this.accoutNameField = new Ext.form.DisplayField({
                    name: 'accountName',
                    xtype: 'displayfield',
                    fieldLabel: this.accountText
                }),
                this.rangeNameField = new Ext.form.DisplayField({
                    name: 'rangeName',
                    xtype: 'displayfield',
                    fieldLabel: this.rangeText
                }),
                this.typeNameField = new Ext.form.DisplayField({
                    name: 'typeName',
                    xtype: 'displayfield',
                    fieldLabel: this.typeText
                })]
            },{
                xtype: 'highchart',
                flex: 1,
                store: this.store,
                series: [{
                    type: 'area',
                    dataIndex: 'amount',
                    xField: 'date'
                }],
                chartConfig: {
                    chart: {						
                        zoomType: 'x'						
                    },
                    title: {
                        text: null
                    },
                    xAxis: [{
                        title: {
                            text: this.rangeText
                        },
                        type: 'datetime',
                        maxZoom: 1000 * 3600 * 24 * 7 // 1 semana
                    }],
                    yAxis: [{
                        title: {
                            text: this.amountText
                        },
                        labels: {
                            formatter: function() {
                                return QFinance.moneySymbol + ' ' + Ext.util.Format.number(this.value,QFinance.numberFormat);
                            }
                        }						
                    }],
                    plotOptions: {
                        area: {
                            marker: {
                                enabled: false,
                                symbol: 'circle',
                                radius: 2,
                                states: {
                                    hover: {
                                        enabled: true
                                    }
                                }
                            }
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        formatter: function() {
                            var tooltip = '';
                            tooltip += '<span style="font-size: 10px">' + Highcharts.dateFormat('%A, %b %e, %Y', this.x) + '</span><br/>';
                            if (this.y >= 0 )
                                tooltip += '<span style="color:green;">' + QFinance.moneySymbol + ' ' + Ext.util.Format.number(this.y,QFinance.numberFormat) + '</span>';
                            else
                                tooltip += '<span style="color:red;">' + QFinance.moneySymbol + ' ' + Ext.util.Format.number(this.y,QFinance.numberFormat) + '</span>';
                            return tooltip;
                        }
                    }
                }				
            }]
        });	
		
        QFinance.portlet.AccountsBalanceHistoryPortlet.superclass.initComponent.call(this);
    },
	
    initStore: function() {
        this.store = new Ext.data.DirectStore({
            autoLoad: false,
            idProperty: 'id',
            root: 'data',
            successProperty: 'success',
            paramsAsHash: false,
            paramOrder: [ 'accountID', 'range', 'startDate', 'endDate', 'type' ],
            baseParams: {
                accountID: this.configuration.accountID, 
                range: this.configuration.range, 
                startDate: this.configuration.startDate, 
                endDate: this.configuration.endDate, 
                type: this.configuration.type
            },
            api: {
                read: QFinance.Remoting.AccountsBalanceHistoryPortletAction.read
            },
            fields: [{
                name: 'date',
                type: 'int'				
            },				
            'amount'
            ],
            listeners: {
                scope: this,
                beforeload: function(store, options) {
                    this.body.mask(QFinance.loadingText, 'x-mask-loading');		
                },				
                load: function(store, records, options) {					
                    this.body.unmask();
                },
                exception: function(dataProxy, type, action, options, response, arg ) {
                    var message = '';
                    var where = '';
                    if (response.result) {
                        message = response.result.message;
                        if (response.result.type == 'exception')
                            where = response.result.where;
                    } else {
                        message = response.message;
                    }
                    this.body.mask(message);					
                    QFinance.service.DeveloperService.log('Store failure: ' + message + ' where\n' + where);
                }
            }				
        });
    },
	
    loadData: function() {
        this.accoutNameField.setValue(this.configuration.accountName);
        if (this.configuration.range == 6)
            this.rangeNameField.setValue(this.configuration.rangeName + ' - ' + this.configuration.startDate + ' to ' + this.configuration.endDate);
        else
            this.rangeNameField.setValue(this.configuration.rangeName);
        this.typeNameField.setValue(this.configuration.typeName);
        this.store.setBaseParam('accountID', this.configuration.accountID);
        this.store.setBaseParam('range', this.configuration.range);
        this.store.setBaseParam('startDate', this.configuration.startDate);
        this.store.setBaseParam('endDate', this.configuration.endDate);
        this.store.setBaseParam('type', this.configuration.type);
        this.store.load();
    }
});