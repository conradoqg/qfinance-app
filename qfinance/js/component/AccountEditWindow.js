/*
 * @depends component.js, Window.js
 */
QFinance.component.AccountEditWindow = Ext.extend( QFinance.ui.Window, {	
    // Configuration
    isNew: true,
    record: null,
    autoLoadRecord: false,
    savable: true,
	
    // Localization	
    defaultTitleText: 'Editing a new account',
    editingTitleText: 'Editing account: {0}',
    idText: 'ID',
    accountNameText: 'Account name',
    bankNumberText: 'Bank number',
    bankAgencyText: 'Bank agency',
    bankAccountText: 'Bank account',
    initialAmountText: 'Initial amount',
    accountTypeText: 'Type',
    accountTypeEmptyText: 'Select a type',
    saveText: 'Save',
    closeText: 'Close',
    privateText: 'Private',
    publicText: 'Public',
    labelWidth: 90,
	
    // Components
    formPanel: null,
    accountTypeCombo: null,

    constructor: function(config) {
        Ext.apply(this, config);
		
        this.title = this.defaultTitleText;
		
        Ext.applyIf(this, {
            width: 300,
            autoHeight: true
        });
		
        this.addEvents(['save']);
		
        QFinance.component.AccountEditWindow.superclass.constructor.call(this, config);
    },
	
    initComponent: function() {
        var formPanelConfig = this.getFormPanelConfig();
        this.formPanel = new Ext.form.FormPanel(formPanelConfig);		
				
        Ext.apply(this, {
            layout: 'fit',
            items: [this.formPanel]
        });	
		
        QFinance.component.AccountEditWindow.superclass.initComponent.call(this);		
    },
	
    show: function() {		
        QFinance.component.AccountEditWindow.superclass.show.call(this);

        if (this.autoLoadRecord && this.record)
            this.loadRecord();
    },
	
    initStatusBar: function( toolbar ) {        
        if (this.savable) {
            toolbar.add(new Ext.Button({
                text: this.saveText,
                formBind: true,
                scope: this,
                iconCls: 'fugue-disk-black',
                handler: function() {
                    if (this.formPanel.getForm().isValid()) {
                        this.doSave();	 
                        this.close();
                    }
                }
            }));
        }
				
        toolbar.add(new Ext.Button({
            text: this.closeText,
            iconCls: 'fugue-door-open-out',				
            scope: this,
            handler: function() {
                this.close();
            }
        }));
    },
	
    getFormPanelConfig: function() {
        var formPanelConfig = {
            frame: true,
            autoHeight: true,
            defaultType: 'textfield',
            defaults: {
                anchor: '100%'
            },
            labelWidth: this.labelWidth,
            items: this.getFormPanelItems()
        };
		
        return formPanelConfig;
    },
	
    getFormPanelItems: function() {
        var formPanelItems = [{
            fieldLabel: this.idText,
            name: 'id',
            readOnly: true
        },{
            fieldLabel: this.accountNameText,
            name: 'accountName',    		
            allowBlank: false
        },{
            fieldLabel: this.bankNumberText,
            name: 'bankNumber',
            allowBlank: true
        },{
            fieldLabel: this.bankAgencyText,
            name: 'bankAgency',
            allowBlank: true    		
        },{
            fieldLabel: this.bankAccountText,
            name: 'bankAccount',
            allowBlank: true
        },{
            xtype: 'numberfield',
            fieldLabel: this.initialAmountText,
            name: 'initialAmount',
            allowBlank: false,
            decimalSeparator: QFinance.decimalSeparator,
            value: 0
        },
        this.accountTypeCombo = new Ext.form.ComboBox({
            fieldLabel: this.accountTypeText,
            name: 'accountType',    		
            typeAhead: true,
            triggerAction: 'all',
            emptyText: this.accountTypeEmptyText,
            selectOnFocus: true,
            forceSelection: true,
            allowBlank: false,
            store: [
            ['1', this.privateText],
            ['2', this.publicText]
            ]    	     
        })];
		
        return formPanelItems;
    },
	
    doSave: function() {
        this.record.set('accountTypeName', this.accountTypeCombo.getRawValue());
        this.formPanel.getForm().updateRecord( this.record );		
        this.fireEvent('save', this.record);
    },	
	
    loadRecord: function() {
        if (!this.isNew)
            this.setTitle(String.format(this.editingTitleText, this.record.get('accountName')));
        this.formPanel.getForm().loadRecord( this.record );
    }
});
Ext.reg('QFinance.component.AccountEditWindow', QFinance.component.AccountEditWindow);