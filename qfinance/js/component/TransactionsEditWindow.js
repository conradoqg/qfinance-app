/*
 * @depends component.js, Window.js
 */
QFinance.component.TransactionsEditWindow = Ext.extend( QFinance.ui.Window, {
    // Localization
    closeText: 'Close',
	
    constructor: function(config) {
        Ext.apply(this, config);
		
        QFinance.component.TransactionsEditWindow.superclass.constructor.call(this);
    },
	
    initComponent: function() {
        Ext.apply(this.transactionsEditPanelConfig, {
            listeners: {		
                scope: this,
                close: function() {
                    this.close();
                }
            }
        });
                
        var transactionsEditPanel = new QFinance.component.TransactionsEditPanel(this.transactionsEditPanelConfig);
		
        Ext.applyIf(this,{
            width: 400,
            height: 400
        });
		
        Ext.apply(this, {
            layout: 'fit',
            items: [transactionsEditPanel],
            bbar: [{
                text: this.closeText,
                iconCls: 'fugue-door-open-out',	
                scope: this,
                handler: function() {				
                    this.close();					
                }
            }]
        });
		
        QFinance.component.TransactionsEditWindow.superclass.initComponent.call(this);        
    }
});
Ext.reg('QFinance.component.TransactionsEditWindow', QFinance.component.TransactionsEditWindow);