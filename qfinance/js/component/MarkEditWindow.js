/*
 * @depends component.js, Window.js
 */
QFinance.component.MarkEditWindow = Ext.extend( QFinance.ui.Window, {	
    // Configuration
    isNew: true,
    record: null,
    autoLoadRecord: false,
    savable: true,
	
    // Localization	
    defaultTitleText: 'Editing a new mark',
    editingTitleText: 'Editing mark: {0}',
    idText: 'ID',
    nameText: 'Name',
    descriptionText: 'Description',
    saveText: 'Save',
    closeText: 'Close',
    labelWidth: 65,
	
    // Components
    formPanel: null,
    
    constructor: function(config) {
        Ext.apply(this, config);
		
        this.title = this.defaultTitleText;
		
        Ext.applyIf(this, {
            width: 300,
            autoHeight: true
        });
		
        this.addEvents(['save']);
		
        QFinance.component.MarkEditWindow.superclass.constructor.call(this, config);
    },
	
    initComponent: function() {
        var formPanelConfig = this.getFormPanelConfig();
        this.formPanel = new Ext.form.FormPanel(formPanelConfig);		
				
        Ext.apply(this, {
            layout: 'fit',
            items: [this.formPanel]
        });	
		
        QFinance.component.MarkEditWindow.superclass.initComponent.call(this);		
    },
	
    show: function() {		
        QFinance.component.MarkEditWindow.superclass.show.call(this);

        if (this.autoLoadRecord && this.record)
            this.loadRecord();
    },
	
    initStatusBar: function( toolbar ) {        
        if (this.savable) {
            toolbar.add(new Ext.Button({
                text: this.saveText,
                formBind: true,
                scope: this,
                iconCls: 'fugue-disk-black',
                handler: function() {
                    if (this.formPanel.getForm().isValid()) {
                        this.doSave();	 
                        this.close();
                    }
                }
            }));
        }
				
        toolbar.add(new Ext.Button({
            text: this.closeText,
            iconCls: 'fugue-door-open-out',				
            scope: this,
            handler: function() {
                this.close();
            }
        }));
    },
	
    getFormPanelConfig: function() {
        var formPanelConfig = {
            frame: true,
            autoHeight: true,
            defaultType: 'textfield',
            defaults: {
                anchor: '100%'
            },
            labelWidth: this.labelWidth,
            items: this.getFormPanelItems()
        };
		
        return formPanelConfig;
    },
	
    getFormPanelItems: function() {
        var formPanelItems = [{
            xtype: 'displayfield',
            fieldLabel: this.idText,
            name: 'id'
        },{
            fieldLabel: this.nameText,
            name: 'name',    		
            allowBlank: false
        },{
            fieldLabel: this.descriptionText,
            name: 'description',
            allowBlank: true
        }];
		
        return formPanelItems;
    },
	
    doSave: function() {        
        this.formPanel.getForm().updateRecord( this.record );		
        this.fireEvent('save', this.record);
    },	
	
    loadRecord: function() {
        if (!this.isNew)
            this.setTitle(String.format(this.editingTitleText, this.record.get('name')));
        this.formPanel.getForm().loadRecord( this.record );
    }
});
Ext.reg('QFinance.component.MarkEditWindow', QFinance.component.MarkEditWindow);