/*
 * @depends component.js, Window.js
 */
QFinance.component.TransactionMultiEditWindow = Ext.extend( QFinance.ui.Window, {
    // Configuration    
    canChangeSourceAccount: true,
    canChangeDestinationAccount: true,
    canChangeCategory: true,
    accountEditMode: 'both',
    contextAccountID: null,	
    records: null,
    requireCategory: true,
	
    // Localization
    saveText: 'Save',
    closeText: 'Close',    
    accountEmptyText: 'Select a account',
    sourceAccountText: 'Source account',
    destinationAccountText: 'Destination account',    
    defaultTitleText: 'Edit multiple transactions',
    pleaseSelectAnCategory: 'Please, select an category.',
    fillRequiredFields: 'Please, fill required fields.',
    formValidationTitleText: 'Form validation',  
    reloadDataText: 'Reload data',
    categoryText: 'Category',
		
    // Components
    accountStore: null,
    formPanel: null,
    categoryPanel: null,
    sourceComboBox: null,
    destinationComboBox: null,
	
    constructor: function(config) {
        Ext.apply(this,config);
		
        this.accountStore = new Ext.data.DirectStore({
            storeId: 'account-store',
            directFn: QFinance.Remoting.AccountAction.getAccountsMinus,
            paramsAsHash: false,
            idProperty: 'id',
            root: 'data',
            successProperty: 'success',
            paramOrder: [ 'excludeAccount' ],
            baseParams: {
                excludeAccount: this.contextAccountID
            },
            fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'accountName'	    		    
            },
            {
                name: 'accountType'
            }
            ],
            listeners: {
                scope: this,
                beforeload: function(store, options) {
                    var components = [];
                    if (this.sourceComboBox)
                        components.push(this.sourceComboBox);
                    if (this.destinationComboBox)
                        components.push(this.destinationComboBox);
                    store.id = this.getProcessStatusManager().start(components);
                    delete components;
                },				
                load: function(store, records, options) {
                    this.getProcessStatusManager().success(store.id);
                },			
                exception: function(dataProxy, type, action, options, response, arg ) {
                    var message = '';
                    var where = '';
                    if (response.result) {
                        message = response.result.message;
                        if (response.result.type == 'exception')
                            where = response.result.where;
                    } else {
                        message = response.message;
                    }
                    this.getProcessStatusManager().error(this.accountStore.id, message);                    
                    QFinance.service.DeveloperService.log('Store failure: ' + message + ' where\n' + where);
                }
            }		
        });
		
        this.addEvents(['save']);
		
        Ext.applyIf(this, {
            width: 400,
            height: 400
        });
		
        QFinance.component.TransactionMultiEditWindow.superclass.constructor.call(this);
    },
	
    show: function() {		
        QFinance.component.TransactionMultiEditWindow.superclass.show.call(this);
						        
        this.accountStore.load();	
    },

    initComponent: function() {		
        var formPanelConfig = this.getFormPanelConfig();
        this.formPanel = new Ext.form.FormPanel(formPanelConfig);
						
        Ext.apply(this,{
            title: this.defaultTitleText,
            layout: 'vbox',
            bodyStyle:'padding: 5px',
            layoutConfig: {
                align : 'stretch',
                pack  : 'start'
            },	
            items: [ this.formPanel, {
                xtype: 'fieldset',
                title: this.categoryText,
                layout: 'fit',
                flex: 1,
                items: [ this.categoryPanel = new QFinance.component.CategoriesEditPanel({
                    disabled: !this.canChangeCategory,
                    autoSelectCategoryID: (this.record && this.record.get('categoryID') > 0) ? this.record.get('categoryID') : 'root',
                    ownerWindow: this
                })]
            }],
            tbar: [{
                text: this.reloadDataText,
                iconCls: 'fugue-arrow-circle',
                scope: this,
                handler: this.doRefreshData
            }]
        });		
		
        QFinance.component.TransactionMultiEditWindow.superclass.initComponent.call(this);
    },    
    
    initStatusBar: function(toolbar) {
        toolbar.add(new Ext.Button({
            text: this.saveText,
            iconCls: 'fugue-disk-black',
            scope: this,
            handler: function() {
                if (this.checkValid()) {
                    this.doSave();
                    this.close();
                }
            }
        }));
        
        toolbar.add(new Ext.Button({
            text: this.closeText,
            iconCls: 'fugue-door-open-out',				
            scope: this,
            handler: function() {
                this.close();
            }
        }));
    },
	
    getFormPanelConfig: function() {
        var formPanelConfig = {			
            defaultType: 'textfield',				
            autoHeight: true,
            frame: true,
            labelWidth: 120,
            defaults: {
                anchor: '100%'
            },
            items: this.getFormPanelItems()					
        };
		
        return formPanelConfig;
    },
	
    getFormPanelItems: function() {
        var formPanelItemsConfig = [];
        
        if (this.accountEditMode == QFinance.component.TransactionEditWindow.editModes.BOTH || this.accountEditMode == QFinance.component.TransactionEditWindow.editModes.SOURCE) {
            formPanelItemsConfig.push(this.sourceComboBox = new Ext.form.ComboBox({
                fieldLabel: this.sourceAccountText,
                name: 'sourceAccountID',
                store: this.accountStore,
                typeAhead: true,
                triggerAction: 'all',
                emptyText: this.accountEmptyText,
                selectOnFocus: true,
                valueField: 'id',
                displayField: 'accountName',
                mode: 'local',
                forceSelection: true,
                value: null,
                readOnly: !this.canChangeSourceAccount
            }));
        }
		
        if (this.accountEditMode == QFinance.component.TransactionEditWindow.editModes.BOTH || this.accountEditMode == QFinance.component.TransactionEditWindow.editModes.DESTINATION) {
            formPanelItemsConfig.push(this.destinationComboBox = new Ext.form.ComboBox({
                fieldLabel: this.destinationAccountText,
                name: 'destinationAccountID',
                store: this.accountStore,
                typeAhead: true,
                triggerAction: 'all',
                emptyText: this.accountEmptyText,
                selectOnFocus: true,
                valueField: 'id',
                displayField: 'accountName',
                mode: 'local',
                forceSelection: true,
                value: null,
                readyOnly: !this.canChangeDestinationAccount
            }));
        } 
        
        return formPanelItemsConfig; 
    },
    
    doRefreshData: function() {
        this.accountStore.load();
    },
		
    checkValid: function() {
        var valid = true;
        var alertMessage
		
        if (!this.formPanel.getForm().isValid()) {
            valid = false;
            alertMessage = new QFinance.ui.MessageBox();
            alertMessage.show({
                title: this.formValidationTitleText,
                msg: this.fillRequiredFields,
                icon: alertMessage.WARNING,
                buttons: alertMessage.OK,
                renderTo: this.body
            });
        } else {
            if (this.requireCategory) {
                var selectedCategory = this.categoryPanel.getSelectedCategory();
                if (!selectedCategory) {
                    valid = false;
                    alertMessage = new QFinance.ui.MessageBox();
                    alertMessage.show({
                        title: this.formValidationTitleText,
                        msg: this.pleaseSelectAnCategory,
                        icon: alertMessage.WARNING,
                        buttons: alertMessage.OK,
                        renderTo: this.body
                    });
                }
            }
        }
		
        return valid;
    },
	
    doSave: function() {        
        Ext.each(this.records, function(record) {
            var selectedRecord;
            record.beginEdit();
            if (this.accountEditMode == QFinance.component.TransactionEditWindow.editModes.SOURCE) {
                if (!Ext.isEmpty(this.sourceComboBox.getValue())) { 
                    selectedRecord = this.accountStore.getAt( this.sourceComboBox.selectedIndex );
                    if (selectedRecord) {
                        record.set('sourceAccountID', selectedRecord.id);
                        record.set('sourceAccountName', selectedRecord.get('accountName'));
                    }	
                } else {
                    record.set('sourceAccountID', null);
                    record.set('sourceAccountName', null);
                }
            } else if (this.accountEditMode == QFinance.component.TransactionEditWindow.editModes.DESTINATION) {
                if (!Ext.isEmpty(this.destinationComboBox.getValue())) { 
                    selectedRecord = this.accountStore.getAt( this.destinationComboBox.selectedIndex );
                    if (selectedRecord) {
                        record.set('destinationAccountID', selectedRecord.id);
                        record.set('destinationAccountName', selectedRecord.get('accountName') );	
                    }
                } else {
                    record.set('destinationAccountID', null);
                    record.set('destinationAccountName', null );
                }
            }

            if (this.canChangeCategory) {
                var selectedCategory = this.categoryPanel.getSelectedCategory();
                if (selectedCategory) {
                    record.set('categoryID', selectedCategory.get('id'));
                    record.set('categoryName', selectedCategory.get('categoryName'));
                }
            }

            record.endEdit();		                        
        }, this);
        
        this.fireEvent('save', this.records);
    }	
});

QFinance.component.TransactionMultiEditWindow.editModes = {
    SOURCE: 'source',
    DESTINATION: 'destination',
    BOTH: 'both'
};

Ext.reg('QFinance.component.TransactionMultiEditWindow', QFinance.component.TransactionMultiEditWindow);