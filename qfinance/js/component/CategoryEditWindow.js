/*
 * @depends component.js, Window.js
 */
QFinance.component.CategoryEditWindow = Ext.extend( QFinance.ui.Window, {
    // Configuration
    isNew: true,
    record: null,
    autoLoadRecord: false,
    canChangeID: false,
    canChangeCategoryName: true,
    savable: true,
	
    // Localization
    defaultTitleText: 'Editing a new category',
    editingTitleText: 'Editing category: {0}',
    idText: 'ID',
    categoyNameText: 'Name',
    saveText: 'Save',
    closeText: 'Close',
	
    // Components
    formPanel: null,

    constructor: function(config) {
        Ext.apply(this, config);
		
        this.title = this.defaultTitleText;
		
        Ext.applyIf(this, {
            width: 200,
            autoHeight: true
        });
		
        this.addEvents(['save']);
		
        QFinance.component.CategoryEditWindow.superclass.constructor.call(this);
    },
	
    initComponent: function() {
        var formPanelConfig = this.getFormPanelConfig();
        this.formPanel = new Ext.form.FormPanel(formPanelConfig);
						
        Ext.apply(this, {
            layout: 'fit',
            items: [this.formPanel],
            bbar: this.statusBar
        });	
		
        if (this.autoLoadRecord && this.record)
            this.loadRecord();
		
        QFinance.component.CategoryEditWindow.superclass.initComponent.call(this);
    },
	
    initStatusBar: function(toolbar) {		
        if (this.savable) {
            toolbar.add(new Ext.Button({
                text: this.saveText,
                formBind: true,
                scope: this,
                iconCls: 'fugue-disk-black',
                handler: function() {
                    if (this.formPanel.getForm().isValid()) {
                        this.doSave();	 
                        this.close();
                    }
                }
            }));
        }
				
        toolbar.add(new Ext.Button({
            text: this.closeText,
            iconCls: 'fugue-door-open-out',				
            scope: this,
            handler: function() {
                this.close();
            }
        }));	
    },
	
    getFormPanelConfig: function() {
        var formPanelConfig = {
            frame: true,
            autoHeight: true,
            defaultType: 'textfield',
            defaults: {
                anchor: '100%'
            },
            labelWidth: 50,
            items: this.getFormPanelItems()
        };
		
        return formPanelConfig;
    },
	
    getFormPanelItems: function() {
        var formPanelItems = [{
            fieldLabel: this.idText,
            name: 'id',
            readOnly: !this.canChangeID,
            width: 40
        },
        {
            fieldLabel: this.categoyNameText,
            name: 'categoryName',
            readyOnly: !this.canChangeCategoryName,
            allowBlank: false
        }];
		
        return formPanelItems;
    },
	
    doSave: function() {
        this.formPanel.getForm().updateRecord( this.record );		
        this.fireEvent('save', this.record);
    },	
	
    loadRecord: function() {
        if (!this.isNew)
            this.setTitle(String.format(this.editingTitleText, this.record.get('categoryName')));
        this.formPanel.getForm().loadRecord( this.record );
    }
});
Ext.reg('QFinance.component.CategoryEditWindow', QFinance.component.CategoryEditWindow);