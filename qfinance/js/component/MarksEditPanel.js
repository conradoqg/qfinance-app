/*
 * @depends component.js, Panel.js
 */
QFinance.component.MarksEditPanel = Ext.extend( QFinance.ui.Panel, {
    // Configuration
    canAdd: true,
    canEdit: true,
    canRemove: true,
	
    // Localization	
    addText: 'Add',
    addTooltipText: 'Add a new mark',
    editText: 'Edit',
    editTooltipText: 'Edit mark',
    removeText: 'Remove',
    removeTooltipText: 'Remove mark',
    markText: 'Mark',
    descriptionText: 'Description',    
    removeQuestionTitle: 'Remove mark',
    removeQuestionMsg: 'Would you like to remove selected marks?',
    editInformationTitle: 'Edit mark',
    editInformationMsg: 'Please select an mark to edit.',
    removeInformationTitle: 'Remove mark',
    removeInformationMsg: 'Please select an mark to remove.',	
	
    // Components
    store: null,		
    grid: null,
    record: null,	
	
    constructor: function(config) {
        Ext.apply(this, config);
		
        this.createRecord();
		
        this.store = new Ext.data.DirectStore({
            autoSave: false,
            autoLoad: false,
            idProperty: 'id',
            root: 'data',
            successProperty: 'success',
            paramsAsHash: false,    		
            api: {
                read: QFinance.Remoting.MarksEditPanelAction.read,
                create: QFinance.Remoting.MarksEditPanelAction.create,
                update: QFinance.Remoting.MarksEditPanelAction.update,
                destroy: QFinance.Remoting.MarksEditPanelAction.destroy
            },
            writer: new Ext.data.JsonWriter({
                encode: false,
                writeAllFields: true
            }),			
            fields: this.record,
            listeners: {
                scope: this,
                beforeload: function(store, options) {
                    store.id = this.getProcessStatusManager().start([this]);
                },				
                load: function(store, records, options) {
                    this.getProcessStatusManager().success(store.id);
                },
                beforewrite: function(  store, action, rs, options, arg ) {
                    store.id = this.getProcessStatusManager().start([this]);
                },
                write: function(  store, action, rs, options, arg ) {
                    this.getProcessStatusManager().success(store.id);
                },				
                exception: function(dataProxy, type, action, options, response, arg ) {
                    var message = '';
                    var where = '';
                    if (response.result) {
                        message = response.result.message;
                        if (response.result.type == 'exception')
                            where = response.result.where;
                    } else {
                        message = response.message;
                    }
                    this.getProcessStatusManager().success(this.store.id, message);                    
                    QFinance.service.DeveloperService.log('Store failure: ' + message + ' where\n' + where);					
                }
            }
        });
		
        this.addEvents([ 
            'close'
            ]);		
		
        QFinance.component.MarksEditPanel.superclass.constructor.call(this);
    },
	
    initComponent: function() {		
        var topBarConfig = this.getTopBarConfig();		
        var topBar = new Ext.Toolbar(topBarConfig);
        var gridConfig = this.getGridConfig(topBar);
        this.grid = new Ext.grid.GridPanel(gridConfig);
				
        Ext.apply(this,{
            layout: 'fit',			
            items: [this.grid]
        });

        QFinance.component.MarksEditPanel.superclass.initComponent.call(this);
    },
	
    createRecord: function() {
        this.record = Ext.data.Record.create([{
            name: 'id',
            type: 'int'
        },{
            name: 'name'
        },{
            name: 'description'
        }]);
    },
	
    getTopBarConfig: function() {
        var topBarConfig = [];
				
        if (this.canAdd) {
            topBarConfig.push({
                text: this.addText,
                tooltip: this.addTooltipText,
                iconCls: 'fugue-bookmark--plus',
                scope: this,
                handler: function() {
                    var baseRecordConfig = {
                    }; 
										
                    var newRecord = new QFinance.component.MarkEditWindow({
                        ownerWindow: this.getOwnerWindow(),
                        record: new this.record(baseRecordConfig),
                        listeners: {
                            scope: this,
                            save: function( record ) {
                                this.store.add( record );
                                this.doSave();								
                            }
                        }
                    });
                    newRecord.show();
                }
            });
        }
		
        if (this.canEdit) {
            topBarConfig.push({
                text: this.editText,
                tooltip: this.editTooltipText,
                iconCls: 'fugue-bookmark--pencil',
                scope: this,
                handler: function() {
                    var selectedRecord = this.grid.getSelectionModel().getSelected();
					
                    if (selectedRecord) {
                        var editRecord = new QFinance.component.MarkEditWindow({
                            ownerWindow: this.getOwnerWindow(),
                            record: selectedRecord,
                            isNew: false,
                            autoLoadRecord: true,							
                            listeners: {
                                scope: this,
                                save: function(record) {
                                    this.doSave();
                                }
                            }
                        });
                        editRecord.show();
                    } else {
                        var information = new QFinance.ui.MessageBox();
                        information.show({
                            title: this.editInformationTitle,
                            msg: this.editInformationMsg,
                            buttons: Ext.MessageBox.OK,
                            renderTo: this.body,
                            icon: Ext.MessageBox.INFO
                        });
                    }
                }
            });
        }
		
        if (this.canRemove) {
            topBarConfig.push({
                text: this.removeText,
                tooltip: this.removeTooltipText,
                iconCls: 'fugue-bookmark--minus',
                scope: this,
                handler: function() {
                    var selectedRecords = this.grid.getSelectionModel().getSelections();
					
                    if (selectedRecords.length > 0) {
                        var question = new QFinance.ui.MessageBox();
                        question.show({
                            title: this.removeQuestionTitle,
                            msg: this.removeQuestionMsg,
                            buttons: Ext.MessageBox.YESNO,
                            scope: this,
                            fn: function(btn) {
                                if (btn == 'yes') {
                                    for (var i = 0; i < selectedRecords.length; i++)
                                        this.store.remove( selectedRecords[i] );
                                    this.doSave();
                                }								   
                            },
                            renderTo: this.body,
                            icon: Ext.MessageBox.QUESTION
                        });
                    } else {
                        var information = new QFinance.ui.MessageBox();
                        information.show({
                            title: this.removeInformationTitle,
                            msg: this.removeInformationMsg,
                            buttons: Ext.MessageBox.OK,
                            renderTo: this.body,
                            icon: Ext.MessageBox.INFO
                        });						
                    }
                }
            });
        }
		
        return topBarConfig;
    },
	
    getGridConfig: function(topBar) {
        var gridConfig = {
            store: this.store,			
            columns: [
            {
                id: 'name',
                header: this.markText, 
                sortable: true,
                dataIndex: 'name',
                width: 100
            },{
                id: 'description',
                header: this.descriptionText, 
                sortable: true,
                dataIndex: 'description'
            }],
            stripeRows: true,
            autoExpandColumn: 'description',
            sm: new Ext.grid.RowSelectionModel(),
            tbar: topBar,
            stateful: true,
            stateid: 'QFinance.component.MarksEditPanel.grid'
        };
		
        return gridConfig;
    },
		
    doSave: function() {
        this.store.save();
    },
        
    loadRecords: function() {
        this.store.load();        
    }
});
Ext.reg('QFinance.component.MarksEditPanel', QFinance.component.MarksEditPanel);