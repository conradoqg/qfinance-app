/*
 * @depends component.js
 */
QFinance.component.AccountOverviewPanel = Ext.extend( Ext.Panel, {
    // Configuration
    accountID: null,
	
    // Localization
    balanceText: 'Balance',
    statisticsText: 'Statistics',	
	
    // Subcomponents
    formPanel: null,
	
    initComponent: function() {
        Ext.apply(this,{
            layout: 'fit',				
            items: [ this.formPanel = new Ext.form.FormPanel({					
                xtype: 'form',
                layout: 'hbox',								        
                items: [{
                    xtype: 'fieldset',
                    flex: 1,
                    title: this.balanceText,				        
                    margins: '0 5 0 0',
                    defaults: {
                        anchor: '100%'
                    },
                    items: [{
                        xtype: 'displayfield',
                        fieldLabel: 'Account balance',
                        name: 'accountBalance'							
                    },{
                        xtype: 'displayfield',
                        fieldLabel: 'Info 2',
                        name: 'test'
                    },{
                        xtype: 'displayfield',
                        fieldLabel: 'Info 3',
                        name: 'test'
                    },{
                        xtype: 'displayfield',
                        fieldLabel: 'Info 4',
                        name: 'test'
                    }]						
                },{
                    xtype: 'fieldset',
                    flex: 1,
                    title: this.statisticsText,
                    defaults: {
                        anchor: '100%'
                    },
                    items: [{
                        xtype: 'displayfield',
                        fieldLabel: 'Info 1',
                        name: 'test'
                    },{
                        xtype: 'displayfield',
                        fieldLabel: 'Info 2',
                        name: 'test'
                    },{
                        xtype: 'displayfield',
                        fieldLabel: 'Info 3',
                        name: 'test'
                    },{
                        xtype: 'displayfield',
                        fieldLabel: 'Info 4',
                        name: 'test'
                    }]						
                }],
                api: {
                    load: QFinance.Remoting.AccountOverviewPanelAction.getAccountOverview
                },
                paramOrder: [ 'accountID' ]
            })]
        });
        QFinance.component.AccountOverviewPanel.superclass.initComponent.call(this);
    },
	
    load: function( accountID ) {
        this.getEl().mask('Loading');
        this.accountID = accountID;
        this.formPanel.load({
            scope: this,
            params: {
                accountID: accountID
            },
            
            success: function(form, action) {
                /*
            	this.formPanel.items.get(0).items.get(0).removeClass('qfinance-red');
            	this.formPanel.items.get(0).items.get(0).removeClass('qfinance-blue');
            	
            	if (action.result.data.accountBalance >= 0)
            		this.formPanel.items.get(0).items.get(0).addClass('qfinance-blue');
            	else {
            		this.formPanel.items.get(0).items.get(0).addClass('qfinance-red');
            		this.formPanel.items.get(0).items.get(0).setValue(action.result.data.accountBalance *= -1);
            	}*/
            	
                this.getEl().unmask();
            }
        });
    }
});
Ext.reg('QFinance.component.AccountOverviewPanel', QFinance.component.AccountOverviewPanel);