/*
 * @depends component.js, Window.js
 */
QFinance.component.UserEditWindow = Ext.extend( QFinance.ui.Window, {	
    // Configuration
    isNew: true,
    record: null,
    autoLoadRecord: false,
    savable: true,
	
    // Localization	
    defaultTitleText: 'Editing a new user',
    editingTitleText: 'Editing user: {0}',
    idText: 'ID',
    userNameText: 'User name',
    passwordText: 'Password',
    passwordCheckText: 'Re-type password',
    saveText: 'Save',
    closeText: 'Close',
	
    // Components
    formPanel: null,

    constructor: function(config) {
        Ext.apply(this, config);
		
        this.title = this.defaultTitleText;
		
        Ext.applyIf(this, {
            width: 300,
            autoHeight: true
        });
		
        this.addEvents(['save']);
		
        QFinance.component.UserEditWindow.superclass.constructor.call(this);
    },
	
    initComponent: function() {
        var formPanelConfig = this.getFormPanelConfig();
        this.formPanel = new Ext.form.FormPanel(formPanelConfig);
						
        Ext.apply(this, {
            layout: 'fit',
            items: [this.formPanel]
        });	
		
        if (this.autoLoadRecord && this.record)
            this.loadRecord();
		
        QFinance.component.UserEditWindow.superclass.initComponent.call(this);
    },
	
    initStatusBar: function(toolbar) {	
        if (this.savable) {
            toolbar.add(new Ext.Button({
                text: this.saveText,
                formBind: true,
                scope: this,
                iconCls: 'fugue-disk-black',
                handler: function() {
                    if (this.formPanel.getForm().isValid()) {
                        this.doSave();	 
                        this.close();
                    }
                }
            }));
        }
				
        toolbar.add(new Ext.Button({
            text: this.closeText,
            iconCls: 'fugue-door-open-out',				
            scope: this,
            handler: function() {
                this.close();
            }
        }));
    },
	
    getFormPanelConfig: function() {
        var formPanelConfig = {
            frame: true,
            autoHeight: true,
            defaultType: 'textfield',
            defaults: {
                anchor: '100%'
            },
            labelWidth: 90,
            items: this.getFormPanelItems()
        };
		
        return formPanelConfig;
    },
	
    getFormPanelItems: function() {
        var formPanelItems = [{
            fieldLabel: this.idText,
            name: 'id',
            readOnly: true
        },{
            fieldLabel: this.userNameText,
            name: 'username',    		
            allowBlank: false
        },{
            fieldLabel: this.passwordText,
            id: 'newPassword',
            name: 'newPassword',
            allowBlank: true,
            inputType: 'password'
        },{
            fieldLabel: this.passwordCheckText,
            name: 'newPasswordCheck',
            matches: 'newPassword',
            vtype: 'password',
            allowBlank: false,
            inputType: 'password'
        }];
		
        return formPanelItems;
    },
	
    doSave: function() {
        this.formPanel.getForm().updateRecord( this.record );		
        this.fireEvent('save', this.record);
    },	
	
    loadRecord: function() {
        if (!this.isNew)
            this.setTitle(String.format(this.editingTitleText, this.record.get('username')));
        this.formPanel.getForm().loadRecord( this.record );
    }
});
Ext.reg('QFinance.component.UserEditWindow', QFinance.component.UserEditWindow);