/*
 * @depends component.js, Panel.js
 */
QFinance.component.TransactionsEditPanel = Ext.extend( QFinance.ui.Panel, {
    // Configuration
    checkTotalAmount: false,
    canAddTransaction: true,
    canEditTransaction: true,
    canRemoveTransaction: true,
    isSubtransaction: false,
    canViewSubtransactions: true,
    totalAmount: 0,	
    contextAccountID: null,
    contextDate: null,
    temporaryID: null,
    parentTransactionID: null,
    accountEditMode: 'both',
    autoLoadTransactions: false,
    showAccountOverview: false,
    showAccountBalance: false,
    recordDragDrop: false,
    onlyAnnotation: false,
    forceCategorySelection: true,
	
    // Localization
    subtransactionsText: 'Subtransactions',
    addTransactionText: 'Add',
    editTransactionText: 'Edit',
    removeTransactionText: 'Remove',
    recordsTotalText: 'Current amount: {0}',
    totalAmountText: 'Total amount: {0}',
    dateText: 'Date',
    descriptionText: 'Description',
    bankMemoText: 'Bank memo',
    accountText: 'Account',
    amountText: 'Amount',
    categoryText: 'Category',
    markText: 'Mark',
    subtransactionsWindowTitleText: 'Subtransactions of transaction: {0}',
    totalAmountIsntEqualTransactionsAmountText: 'While the total value of transactions is not equal to the parent transaction, the transaction will not be saved.',
    accountOverviewText: 'Account overview',
    accountBalanceText: 'Account balance:',
    removeQuestionTitle: 'Remove transaction',
    removeQuestionMsg: 'Would you like to remove selected transactions?',
    editInformationTitle: 'Edit transaction',
    editInformationMsg: 'Please select an transaction to edit.',
    multiEditInformationTitle: 'Edit multiple transaction',
    multiEditInformationMsg: 'Only accounts of same source/destination can be multi-edited',
    viewSubtransactionsInformationTitle: 'View subtransactions',
    viewSubtransactionsInformationMsg: 'Please select an transaction.',
    removeInformationTitle: 'Remove transaction',
    removeInformationMsg: 'Please select an transaction to remove.',
    saveInformationTitle: 'Save information',
	
    // Components
    store: null,	
    recordsTotalStatusBarItem: null,
    accountBalanceStatusBarItem: null,
    grid: null,
    transactionRecord: null,
    accountOverviewPanel: null,
    saveInfoShowed: false,
	
    constructor: function(config) {
        Ext.apply(this, config);
		
        this.createTransactionRecord();
		
        this.store = new Ext.data.DirectStore({
            autoSave: false,
            autoLoad: false,
            idProperty: 'id',
            root: 'data',
            successProperty: 'success',
            paramsAsHash: false,
            paramOrder: [ 'accountID', 'temporaryID', 'parentTransactionID', 'annotation' ],
            baseParams: {
                accountID: this.contextAccountID, 
                temporaryID: this.temporaryID, 
                parentTransactionID: this.parentTransactionID, 
                annotation: this.onlyAnnotation
            },
            api: {
                read: QFinance.Remoting.TransactionsEditPanelAction.read,
                create: QFinance.Remoting.TransactionsEditPanelAction.create,
                update: QFinance.Remoting.TransactionsEditPanelAction.update,
                destroy: QFinance.Remoting.TransactionsEditPanelAction.destroy
            },
            writer: new Ext.data.JsonWriter({
                encode: false,
                writeAllFields: true
            }),			
            fields: this.transactionRecord,
            listeners: {
                scope: this,
                beforeload: function(store, options) {
                    store.id = this.getProcessStatusManager().start([this]);		
                },				
                load: function(store, records, options) {
                    this.getProcessStatusManager().success(store.id);				
                    this.updateTransactionsAmount();
                    if (this.showAccountBalance) {
                        this.loadAccountBalance();			
                    }
                },
                beforewrite: function(store, action, rs, options, arg ) {
                    store.id = this.getProcessStatusManager().start([this]);
                },
                write: function(store, action, rs, options, arg ) {
                    this.getProcessStatusManager().success(store.id);
                    this.updateTransactionsAmount();
                    if (this.showAccountBalance) {
                        this.loadAccountBalance();			
                    }
                },				
                exception: function(dataProxy, type, action, options, response, arg ) {
                    var message = '';
                    var where = '';
                    if (response.result) {
                        message = response.result.message;
                        if (response.result.type == 'exception')
                            where = response.result.where;
                    } else {
                        message = response.message;
                    }
                    this.getProcessStatusManager().error(this.store.id, message);
                    QFinance.service.DeveloperService.log('Store failure: ' + message + ' where\n' + where);					
                }
            }			
        });
		
        this.addEvents([ 
            'close'
            ]);		
		
        QFinance.component.TransactionsEditPanel.superclass.constructor.call(this);
    },
	
    initComponent: function() {		
        var topBarConfig = this.getTopBarConfig();		
        var topBar = new Ext.Toolbar(topBarConfig);
        var gridConfig = this.getGridConfig(topBar);
        this.grid = new Ext.grid.GridPanel(gridConfig);
		
        //this.initStatusBar();
		
        var items = [this.grid];
        // TODO: Quando der desenvolver e utilizar o 'Account overview'
        /*
		if (this.showAccountOverview)
			items.push(this.accountOverviewPanel = new QFinance.component.AccountOverviewPanel({								
				title: this.accountOverviewText,
				region: 'south',				
				collapsible: true,
				autoHeight: true,
				frame: true
			}));*/		
		
        Ext.apply(this, {
            layout: 'border',
            items: items
        });

        QFinance.component.TransactionsEditPanel.superclass.initComponent.call(this);
    },
	
    createTransactionRecord: function() {
        this.transactionRecord = Ext.data.Record.create([{
            name: 'id',
            type: 'int'
        },{
            name: 'date',		
            type: 'date',	
            dateFormat: "c"
        },{
            name: 'description'
        },{
            name: 'bankMemo'
        },{
            name: 'amount',	
            type: 'float'
        },{
            name: 'sourceAccountID',
            type: 'int',
            useNull: true
        },{
            name: 'sourceAccountName'
        },{
            name: 'destinationAccountID',
            type: 'int',
            useNull: true
        },{
            name: 'destinationAccountName'
        },{
            name: 'categoryID', 
            type: 'int',
            useNull: true
        },{
            name: 'categoryName'
        },{
            name: 'hasSubtransactions',
            type: 'bolean'
        },{
            name: 'subtransactions',
            type: 'auto'
        },{
            name: 'financialNumber'
        },{
            name: 'temporaryID',
            type: 'int',
            useNull: true
        },{
            name: 'parentTransactionID',
            type: 'int',
            useNull: true
        },{
            name: 'annotation',
            type: 'bolean'
        },{
            name: 'markID',
            type: 'int',
            useNull: true
        },{
            name: 'markName'
        }]);
    },
	
    getTopBarConfig: function() {
        var topBarConfig = [];
		
        if (this.canViewSubtransactions) {
            topBarConfig.push({
                text: this.subtransactionsText,
                iconCls: 'fugue-moneys',
                scope: this,
                handler: function() {	
                    var selectedRecord = this.grid.getSelectionModel().getSelected();
					
                    if (selectedRecord) {
                        var accountEditMode = QFinance.component.TransactionEditWindow.editModes.BOTH;
                        if (selectedRecord.get('sourceAccountID') == this.contextAccountID)
                            accountEditMode = QFinance.component.TransactionEditWindow.editModes.DESTINATION;
                        else if (selectedRecord.get('destinationAccountID') == this.contextAccountID)
                            accountEditMode = QFinance.component.TransactionEditWindow.editModes.SOURCE;							
						
                        var subtransactionsWindow = new QFinance.component.TransactionsEditWindow({
                            width: this.body.dom.offsetWidth - 20,
                            height: this.body.dom.offsetHeight - 20,
                            ownerWindow: this.getOwnerWindow(),
                            title: String.format(this.subtransactionsWindowTitleText, selectedRecord.get('description')),
                            transactionsEditPanelConfig: {
                                checkTotalAmount: true,
                                canAddTransaction: true,
                                canEditTransaction: true,
                                canRemoveTransaction: true,
                                isSubtransaction: true,
                                canViewSubtransactions: true,
                                totalAmount: selectedRecord.get('amount'),									
                                contextAccountID: this.contextAccountID,
                                contextDate: selectedRecord.get('date'),
                                temporaryID: this.temporaryID,							
                                parentTransactionID: selectedRecord.get('id'),
                                accountEditMode: accountEditMode,
                                autoLoadTransactions: true								
                            },
                            listeners: {
                                scope: this,
                                close: function() {
                                    this.updateIfHasSubtransactions( selectedRecord );
                                }
                            }
                        });
                        subtransactionsWindow.show();
                    } else {
                        var information = new QFinance.ui.MessageBox();
                        information.show({
                            title: this.viewSubtransactionsInformationTitle,
                            msg: this.viewSubtransactionsInformationMsg,
                            buttons: Ext.MessageBox.OK,
                            renderTo: this.body,
                            icon: Ext.MessageBox.INFO
                        });
                    }
                }
            });
        }
		
        if (this.canAddTransaction) {
            topBarConfig.push({
                text: this.addTransactionText,
                iconCls: 'fugue-money--plus',
                scope: this,
                handler: function() {
                    var baseRecordConfig = {
                        temporaryID: this.temporaryID,
                        parentTransactionID: this.parentTransactionID,
                        annotation: this.onlyAnnotation
                    }; 
                    var canChangeDate;
                    var canChangeAmount = true;
                    var contextAccountID = this.contextAccountID;
                    var accountEditMode;
					
                    if (this.isSubtransaction) {
                        canChangeDate = false;
                        accountEditMode = this.accountEditMode;						
                        baseRecordConfig.date = this.contextDate;						
                        if (this.accountEditMode == QFinance.component.TransactionEditWindow.editModes.SOURCE)
                            baseRecordConfig.destinationAccountID = this.contextAccountID;
                        else if (this.accountEditMode == QFinance.component.TransactionEditWindow.editModes.DESTINATION)
                            baseRecordConfig.sourceAccountID = this.contextAccountID;

                    } else {
                        canChangeDate = true;
                        contextAccountID = null;
                        accountEditMode = QFinance.component.TransactionEditWindow.editModes.BOTH;						
                    }
					
                    var newTransaction = new QFinance.component.TransactionEditWindow({
                        ownerWindow: this.getOwnerWindow(),
                        record: new this.transactionRecord(baseRecordConfig),
                        accountEditMode: accountEditMode,
                        contextAccountID: contextAccountID,
                        canChangeDate: canChangeDate,
                        canChangeAmount: canChangeAmount,
                        requireCategory: this.forceCategorySelection,
                        listeners: {
                            scope: this,
                            save: function( record ) {
                                this.store.add( record );
                                this.doSave();								
                            }
                        }
                    });
                    newTransaction.show();
                }
            });
        }
		
        if (this.canEditTransaction) {
            topBarConfig.push({
                text: this.editTransactionText,
                iconCls: 'fugue-money--pencil',
                scope: this,
                handler: function() {
                    var selectedRecords = this.grid.getSelectionModel().getSelections();
                    var accountEditMode;
                    var canChangeCategory;
                    var contextAccountID;
                    var editTransactionWindow;
                    var informationMessageBox;
					
                    if (selectedRecords.length == 1) {
                        var selectedRecord = selectedRecords[0];
                        var canChangeAmount;
                        var canChangeDate;
						
                        if (this.isSubtransaction) {
                            canChangeAmount = true;
                            canChangeCategory = true;
                            canChangeDate = false;
                            contextAccountID = this.contextAccountID;
							
                            if (selectedRecord.get('sourceAccountID') == this.contextAccountID)
                                accountEditMode = QFinance.component.TransactionEditWindow.editModes.DESTINATION;
                            else if (selectedRecord.get('destinationAccountID') == this.contextAccountID)
                                accountEditMode = QFinance.component.TransactionEditWindow.editModes.SOURCE;							
                        } else {
                            canChangeAmount = !selectedRecord.get('hasSubtransactions');
                            canChangeCategory = !selectedRecord.get('hasSubtransactions');
                            canChangeDate = !selectedRecord.get('hasSubtransactions');
                            contextAccountID = null;
							
                            if (selectedRecord.get('hasSubtransactions')) {
                                if (selectedRecord.get('sourceAccountID') == this.contextAccountID)
                                    accountEditMode = QFinance.component.TransactionEditWindow.editModes.DESTINATION;
                                else if (selectedRecord.get('destinationAccountID') == this.contextAccountID)
                                    accountEditMode = QFinance.component.TransactionEditWindow.editModes.SOURCE;								
                            } else {
                                accountEditMode = QFinance.component.TransactionEditWindow.editModes.BOTH;	
                            }														
                        }
                        editTransactionWindow = new QFinance.component.TransactionEditWindow({
                            ownerWindow: this.getOwnerWindow(),
                            record: selectedRecord,
                            isNew: false,
                            autoLoadRecord: true,
                            contextAccountID: contextAccountID,
                            accountEditMode: accountEditMode,
                            canChangeAmount: canChangeAmount,
                            canChangeCategory: canChangeCategory,
                            canChangeDate: canChangeDate,														
                            requireCategory: selectedRecord.get('hasSubtransactions') ? false : this.forceCategorySelection,
                            listeners: {
                                scope: this,
                                save: function(record) {
                                    this.doSave();
                                }
                            }
                        });
                        editTransactionWindow.show();
                    } else if (selectedRecords.length > 0) {
                        // Check if selected records is of same direction (income, expense)
                        var sameSourceAccountID = true;
                        var sameDestinationAccountID = true;
                        var sourceAccountID = selectedRecords[0].get('sourceAccountID');
                        var destinationAccountID = selectedRecords[0].get('destinationAccountID');
                        Ext.each(selectedRecords, function(record) {
                            if (record.get('sourceAccountID') != sourceAccountID)
                                sameSourceAccountID = false;
                            if (record.get('destinationAccountID') != destinationAccountID)
                                sameDestinationAccountID = false;
                        });
                    
                        if (!sameSourceAccountID && !sameDestinationAccountID) {
                            informationMessageBox = new QFinance.ui.MessageBox();
                            informationMessageBox.show({
                                title: this.multiEditInformationTitle,
                                msg: this.multiEditInformationMsg,
                                buttons: Ext.MessageBox.OK,
                                renderTo: this.body,
                                icon: Ext.MessageBox.INFO
                            });
                            return
                        }                            
                        
                        // Determine transaction edit mode
                        
                        if (sourceAccountID != null && sameSourceAccountID)
                            accountEditMode = QFinance.component.TransactionEditWindow.editModes.DESTINATION;
                        else if (destinationAccountID != null && sameDestinationAccountID)
                            accountEditMode = QFinance.component.TransactionEditWindow.editModes.SOURCE;
                        
                        // Determine if can change category
                        canChangeCategory = true;
                        Ext.each(selectedRecords, function(record) {
                            if (record.get('hasSubtransactions')) 
                                canChangeCategory = false;                               
                        });
                        
                        contextAccountID = this.contextAccountID;
                        
                        editTransactionWindow = new QFinance.component.TransactionMultiEditWindow({
                            ownerWindow: this.getOwnerWindow(),
                            records: selectedRecords,                            
                            contextAccountID: contextAccountID,
                            accountEditMode: accountEditMode,                            
                            canChangeCategory: canChangeCategory,
                            canChangeDate: canChangeDate,														
                            requireCategory: canChangeCategory,
                            listeners: {
                                scope: this,
                                save: function(record) {
                                    this.doSave();
                                }
                            }
                        });
                        editTransactionWindow.show();
                    } else {
                        informationMessageBox = new QFinance.ui.MessageBox();
                        informationMessageBox.show({
                            title: this.editInformationTitle,
                            msg: this.editInformationMsg,
                            buttons: Ext.MessageBox.OK,
                            renderTo: this.body,
                            icon: Ext.MessageBox.INFO
                        });
                    }
                }
            });
        }
		
        if (this.canRemoveTransaction) {
            topBarConfig.push({
                text: this.removeTransactionText,
                iconCls: 'fugue-money--minus',
                scope: this,
                handler: function() {
                    var selectedRecords = this.grid.getSelectionModel().getSelections();
					
                    if (selectedRecords.length > 0) {
                        var question = new QFinance.ui.MessageBox();
                        question.show({
                            title: this.removeQuestionTitle,
                            msg: this.removeQuestionMsg,
                            buttons: question.YESNO,
                            icon: question.QUESTION,
                            scope: this,
                            fn: function(btn) {
                                if (btn == 'yes') {
                                    Ext.each(selectedRecords, function(record) {
                                        this.store.remove( record );
                                    });
                                    this.doSave();
                                }								   
                            },
                            renderTo: this.body					   
                        });
                    } else {
                        var information = new QFinance.ui.MessageBox();
                        information.show({
                            title: this.removeInformationTitle,
                            msg: this.removeInformationMsg,
                            buttons: Ext.MessageBox.OK,
                            renderTo: this.body,
                            icon: Ext.MessageBox.INFO
                        });						
                    }
                }
            });
        }
		
        return topBarConfig;
    },
	
    initStatusBar: function( toolbar ) {
        if (this.checkTotalAmount) {
            this.recordsTotalStatusBarItem = new Ext.Toolbar.TextItem({
                text: '?'
            });
            toolbar.insert(0, '-');
            toolbar.insert(0, new Ext.Toolbar.TextItem({ 
                text: String.format(this.totalAmountText,QFinance.moneySymbol + ' ' + Ext.util.Format.number(this.totalAmount,QFinance.numberFormat)) 
            }));            
            toolbar.insert(0, this.recordsTotalStatusBarItem);
        }

        if (this.showAccountBalance){
            this.accountBalanceStatusBarItem = new Ext.Toolbar.TextItem({
                text: '?'
            });

            toolbar.insert(0, this.accountBalanceStatusBarItem);
            toolbar.insert(0, this.accountBalanceText);			
        }
    },
	
    getGridConfig: function(topBar) {
        var gridConfig = {
            region: 'center',
            store: this.store,	
            ddGroup: 'gridDDGroup',
            enableDragDrop: this.recordDragDrop,
            columns: [
            {
                id       :'date',
                header   : this.dateText, 
                sortable : true, 
                dataIndex: 'date',
                renderer: Ext.util.Format.dateRenderer(QFinance.dateFormat),
                editor: {
                    xtype: 'textfield',
                    allowBlank: false
                },
                width: 80
            },{
                id       :'description',
                header   : this.descriptionText, 
                sortable : true, 
                dataIndex: 'description'
            },{
                id       :'bankMemo',
                header   : this.bankMemoText, 
                sortable : true, 
                dataIndex: 'bankMemo'
            },{
                id	: 'accountName',
                header	: this.accountText,
                sortable: true,
                scope: this,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    var renderedText = '';
                    if (this.contextAccountID) {
                        if (this.contextAccountID == record.get('sourceAccountID')) {
                            renderedText = record.get('destinationAccountName');
                        } else if (this.contextAccountID == record.get('destinationAccountID')) {
                            renderedText = record.get('sourceAccountName');
                        }
                    }
                    return renderedText;
                }
            },{
                id       :'amount',
                header   : this.amountText, 
                sortable : true, 
                dataIndex: 'amount',
                scope: this,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    var renderedText = '';
                    if (this.contextAccountID) {
                        if (this.contextAccountID == record.get('sourceAccountID')) {
                            renderedText = '<span style="color:red;">' + QFinance.moneySymbol + ' ' + Ext.util.Format.number(value,QFinance.numberFormat) + '</span>';
                        } else if (this.contextAccountID == record.get('destinationAccountID')) {
                            renderedText = '<span style="color:green;">' + QFinance.moneySymbol + ' ' + Ext.util.Format.number(value,QFinance.numberFormat) + '</span>';
                        }
                    } else {
                        renderedText = QFinance.moneySymbol + ' ' + Ext.util.Format.number(value,QFinance.numberFormat);
                    }
                    return renderedText;
                }
            },{
                id		: 'categoryName',
                header		: this.categoryText, 
                sortable	: true, 
                dataIndex	: 'categoryName'
            },{
                id		: 'markName',
                header		: this.markText, 
                sortable	: true, 
                dataIndex	: 'markName'
            },{
                id		: 'hasSubtransactions',
                header		: '',
                sortable	: true,
                width		: 16,
                dataIndex	: 'hasSubtransactions',
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    if (value) {
                        metaData.css = 'fugue-moneys-cell';	        			
                    }
                }	        		
            }],
            stripeRows: true,
            autoExpandColumn: 'description',
            sm: new Ext.grid.RowSelectionModel(),
            tbar: topBar	        
        };
		
        return gridConfig;
    },
	
    updateIfHasSubtransactions: function( selectedRecord ) {
        var updateProcessID = this.getProcessStatusManager().start([this]);
        this.disable();
        QFinance.Remoting.TransactionsEditPanelAction.hasSubtransactions(selectedRecord.get('id'), function(result, event) {
            var message = '';
            var where = '';
            var failure = false;
			
            if (result) {
                if (result.success) {
                    this.getProcessStatusManager().success(updateProcessID);                    
                    selectedRecord.set('hasSubtransactions', result.hasSubtransactions);
                    selectedRecord.set('categoryID', null);
                    selectedRecord.set('categoryName', null);
                    this.doSave();		
                } else {
                    failure = true;						
                    message = result.message;
                    if (result.type == 'exception')
                        where = result.where;
                }            
            } else {
                failure = true;
                message = event.message;
            }
			
            if (failure) {
                this.getProcessStatusManager().error(updateProcessID, message);                
                QFinance.service.DeveloperService.log('Direct call failure: ' + message + ' where\n' + where);
            }
			
            this.enable();			
        }, this);
    },
	
    getTransactionsAmount: function() {
        var recordsSum = 0;
        this.store.each(function(record) {
            recordsSum += record.get('amount');
        });
        return recordsSum.toFixed(2);
    },
	
    updateTransactionsAmount: function() {
        if (this.checkTotalAmount) {			
            var recordsSum = this.getTransactionsAmount();
			
            this.recordsTotalStatusBarItem.setText(String.format(this.recordsTotalText, QFinance.moneySymbol + ' ' + Ext.util.Format.number(recordsSum,QFinance.numberFormat)));
        }
    },
	
    doSave: function() {		
        if (this.checkTotalAmount) {
            var transactionsAmount = this.getTransactionsAmount(); 
            if (transactionsAmount == 0 || transactionsAmount == this.totalAmount) {
                this.store.save();
                this.updateTransactionsAmount();
            } else {
                this.updateTransactionsAmount();
                if (!this.saveInfoShowed) {
                    this.getOwnerWindow().showInfoMessageBox(this.saveInformationTitle, this.totalAmountIsntEqualTransactionsAmountText);
                    this.saveInfoShowed = true;
                }                
            }
        } else {
            this.store.save();
            this.updateTransactionsAmount();
        }
    },
	
    setContextAccountID: function( id ) {
        this.contextAccountID = id;
        this.store.setBaseParam('accountID', id);
    },
	
    setTemporaryTransactionID: function( id ) {
        this.temporaryID = id;
        this.store.setBaseParam('temporaryID', id);
        this.store.reload();
    },
	
    load: function() {
        this.store.setBaseParam('accountID', this.contextAccountID);
        this.store.setBaseParam('temporaryID', this.temporaryID);
        this.store.setBaseParam('parentTransactionID', this.parentTransactionID);		
        this.store.load();		
    // TODO: A mesma coisa do \todo anterior
    /*
        if (this.showAccountOverview)
                this.accountOverviewPanel.load( this.contextAccountID );*/
    },
	
    loadAccountBalance: function() {
        if (this.contextAccountID) {
            var loadAccountBalanceID = this.getProcessStatusManager().start([this]);            
            QFinance.Remoting.TransactionsEditPanelAction.getAccountBalance(this.contextAccountID, function(result, event) {
                var message = '';
                var where = '';
                var failure = false;
				
                if (result) {
                    if (result.success) {
                        this.getProcessStatusManager().success(loadAccountBalanceID);
                        this.accountBalanceStatusBarItem.removeClass('qfinance-red');
                        this.accountBalanceStatusBarItem.removeClass('qfinance-blue');
                        if (result.accountBalance >= 0) {
                            this.accountBalanceStatusBarItem.addClass('qfinance-blue');
                            this.accountBalanceStatusBarItem.setText(QFinance.moneySymbol + ' ' + Ext.util.Format.number(result.accountBalance,QFinance.numberFormat));
                        } else {
                            this.accountBalanceStatusBarItem.addClass('qfinance-red');
                            this.accountBalanceStatusBarItem.setText(QFinance.moneySymbol + ' ' + Ext.util.Format.number(result.accountBalance *= -1,QFinance.numberFormat));		    		
                        } 
                    } else {
                        failure = true;						
                        message = result.message;
                        if (result.type == 'exception')
                            where = result.where;
                    }            
                } else {
                    failure = true;
                    message = event.message;
                }
				
                if (failure) {
                    this.getProcessStatusManager().error(loadAccountBalanceID, message);					
                    QFinance.service.DeveloperService.log('Direct call failure: ' + message + ' where\n' + where);
                }               		
            }, this);	
        }
    },
	
	
    render: function(ct, position) {
        QFinance.component.TransactionsEditPanel.superclass.render.call(this, ct, position);
		
        if (this.autoLoadTransactions)
            this.load();	
    }
});
Ext.reg('transactionseditpanel', QFinance.component.TransactionsEditPanel);