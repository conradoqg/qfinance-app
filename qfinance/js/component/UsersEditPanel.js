/*
 * @depends component.js, Panel.js
 */
QFinance.component.UsersEditPanel = Ext.extend( QFinance.ui.Panel, {
    // Configuration
    autoLoadUsers: true,
    canAddUser: true,
    canEditUser: true,
    canRemoveUser: true,
	
    // Localization	
    addUserText: 'Add',
    addUserTooltipText: 'Add a new user',
    editUserText: 'Edit',
    editUserTooltipText: 'Edit user',
    removeUserText: 'Remove',
    removeUserTooltipText: 'Remove user',
    userText: 'User',    
    removeQuestionTitle: 'Remove user',
    removeQuestionMsg: 'Would you like to remove selected users?',
    editInformationTitle: 'Edit user',
    editInformationMsg: 'Please select a user to edit.',
    removeInformationTitle: 'Remove user',
    removeInformationMsg: 'Please select a user to remove.',	
	
    // Components
    store: null,		
    grid: null,
    userRecord: null,	
	
    constructor: function(config) {
        Ext.apply(this, config);
		
        this.createUserRecord();
		
        this.store = new Ext.data.DirectStore({
            autoSave: false,
            autoLoad: false,
            idProperty: 'id',
            root: 'data',
            successProperty: 'success',
            paramsAsHash: false,    		
            api: {
                read: QFinance.Remoting.UsersEditPanelAction.read,
                create: QFinance.Remoting.UsersEditPanelAction.create,
                update: QFinance.Remoting.UsersEditPanelAction.update,
                destroy: QFinance.Remoting.UsersEditPanelAction.destroy
            },
            writer: new Ext.data.JsonWriter({
                encode: false,
                writeAllFields: true
            }),			
            fields: this.userRecord,
            listeners: {
                scope: this,
                beforeload: function(store, options) {
                    store.id = this.getProcessStatusManager().start([this]);
                },				
                load: function(store, records, options) {
                    this.getProcessStatusManager().success(store.id);
                },
                beforewrite: function(  store, action, rs, options, arg ) {
                    store.id = this.getProcessStatusManager().start([this]);                    
                },
                write: function(  store, action, rs, options, arg ) {
                    this.getProcessStatusManager().success(store.id);
                },				
                exception: function(dataProxy, type, action, options, response, arg ) {
                    var message = '';
                    var where = '';
                    if (response.result) {
                        message = response.result.message;
                        if (response.result.type == 'exception')
                            where = response.result.where;
                    } else {
                        message = response.message;
                    }
                    this.getProcessStatusManager().error(this.store.id, message);
                    QFinance.service.DeveloperService.log('Store failure: ' + message + ' where\n' + where);					
                }
            }
        });
		
        this.addEvents([ 
            'close'
            ]);		
		
        QFinance.component.UsersEditPanel.superclass.constructor.call(this);
    },
	
    initComponent: function() {		
        var topBarConfig = this.getTopBarConfig();		
        var topBar = new Ext.Toolbar(topBarConfig);
        var gridConfig = this.getGridConfig(topBar);
        this.grid = new Ext.grid.GridPanel(gridConfig);
				
        Ext.apply(this,{
            layout: 'fit',			
            items: [this.grid]
        });

        if (this.autoLoadUsers)
            this.load();

        QFinance.component.UsersEditPanel.superclass.initComponent.call(this);
    },
	
    createUserRecord: function() {
        this.userRecord = Ext.data.Record.create([{
            name: 'id',
            type: 'int'
        },{
            name: 'username'
        },{
            name: 'newPassword'
        }]);
    },
	
    getTopBarConfig: function() {
        var topBarConfig = [];
				
        if (this.canAddUser) {
            topBarConfig.push({
                text: this.addUserText,
                tooltip: this.addUserTooltipText,
                iconCls: 'fugue-user--plus',
                scope: this,
                handler: function() {
                    var baseRecordConfig = {
                    }; 
										
                    var newUser = new QFinance.component.UserEditWindow({
                        renderTo: this.getEl(),
                        constrain: true,
                        modal: true,
                        record: new this.userRecord(baseRecordConfig),
                        listeners: {
                            scope: this,
                            save: function( record ) {
                                this.store.add( record );
                                this.doSave();								
                            }
                        }
                    });
                    newUser.show();
                }
            });
        }
		
        if (this.canEditUser) {
            topBarConfig.push({
                text: this.editUserText,
                tooltip: this.editUserTooltipText,
                iconCls: 'fugue-user--pencil',
                scope: this,
                handler: function() {
                    var selectedRecord = this.grid.getSelectionModel().getSelected();
					
                    if (selectedRecord) {
                        var editUser = new QFinance.component.UserEditWindow({
                            renderTo: this.getEl(),
                            constrain: true,
                            modal: true,
                            record: selectedRecord,
                            isNew: false,
                            autoLoadRecord: true,							
                            listeners: {
                                scope: this,
                                save: function(record) {
                                    this.doSave();
                                }
                            }
                        });
                        editUser.show();
                    } else {
                        var information = new QFinance.ui.MessageBox();
                        information.show({
                            title: this.editInformationTitle,
                            msg: this.editInformationMsg,
                            buttons: Ext.MessageBox.OK,
                            renderTo: this.body,
                            icon: Ext.MessageBox.INFO
                        });
                    }
                }
            });
        }
		
        if (this.canRemoveUser) {
            topBarConfig.push({
                text: this.removeUserText,
                tooltip: this.removeUserTooltipText,
                iconCls: 'fugue-user--minus',
                scope: this,
                handler: function() {
                    var selectedRecords = this.grid.getSelectionModel().getSelections();
					
                    if (selectedRecords.length > 0) {
                        var question = new QFinance.ui.MessageBox();
                        question.show({
                            title: this.removeQuestionTitle,
                            msg: this.removeQuestionMsg,
                            buttons: Ext.MessageBox.YESNO,
                            scope: this,
                            fn: function(btn) {
                                if (btn == 'yes') {
                                    for (i = 0; i < selectedRecords.length; i++)
                                        this.store.remove( selectedRecords[i] );
                                    this.doSave();
                                }								   
                            },
                            renderTo: this.body,
                            icon: Ext.MessageBox.QUESTION
                        });
                    } else {
                        var information = new QFinance.ui.MessageBox();
                        information.show({
                            title: this.removeInformationTitle,
                            msg: this.removeInformationMsg,
                            buttons: Ext.MessageBox.OK,
                            renderTo: this.body,
                            icon: Ext.MessageBox.INFO
                        });						
                    }
                }
            });
        }
		
        return topBarConfig;
    },
	
    getGridConfig: function(topBar) {
        var gridConfig = {
            store: this.store,			
            columns: [
            {
                id		: 'username',
                header		: this.userText, 
                sortable	: true,
                dataIndex	: 'username'	            
            }],
            stripeRows: true,
            autoExpandColumn: 'username',
            sm: new Ext.grid.RowSelectionModel(),
            tbar: topBar,
            stateful: true,
            stateid: 'QFinance.component.UsersEditPanel.grid'
        };
		
        return gridConfig;
    },
		
    doSave: function() {
        this.store.save();
    },
	
    load: function() {
        this.store.load();
    }
});
Ext.reg('QFinance.component.UsersEditPanel', QFinance.component.UsersEditPanel);