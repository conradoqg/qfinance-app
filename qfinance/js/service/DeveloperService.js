/*
 * @depends service.js
 */
QFinance.service.DeveloperService = Ext.extend(Ext.util.Observable, {
    task: null,
			
    running: false,
	
    constructor: function() {
        this.task = {
            run: this.getMessages,
            interval: 1000,
            scope: this
        };
		
        this.addEvents(['print']);
    },
	
    log: function( message ) {
        if (this.logMessages)
            this.print( 'Javascript: ' + message);		
    },
	
    print: function( message ) {
        if (window.console)
            window.console.info( message );
        this.fireEvent('print', message);
    },
	
    startMonitor: function() {
        this.running = true;
        Ext.TaskMgr.start(this.task);
    },
	
    stopMonitor: function() {
        this.running = false;
        Ext.TaskMgr.stop(this.task);
    },
	
    getMessages: function() {
        QFinance.Remoting.DeveloperServiceAction.getMessages(function(messages, event) {
            Ext.each(messages, function( message ) {
                this.print( 'PHP: ' + message );
            }, this);                		
        }, this);	
    },
	
    load: function(callback, scope) {
        QFinance.Remoting.DeveloperServiceAction.getConfiguration(function(result, event) {
            var message = '';
            var where = '';
            var failure = false;
			
            if (result) {
                if (result.success) {
                    Ext.apply(this, result.configuration);
                    if (callback) callback.call(scope||this);		
                } else {
                    failure = true;						
                    message = result.message;
                    if (result.type == 'exception')
                        where = result.where;
                }            
            } else {
                failure = true;
                message = event.message;
            }
			
            if (failure) {			
                QFinance.service.DeveloperService.log('Direct call failure: ' + message + ' where\n' + where);
            }                		
        }, this);			
    }
});
QFinance.service.DeveloperService = new QFinance.service.DeveloperService();