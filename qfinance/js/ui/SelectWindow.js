/*
 * @depends ui.js
 */
QFinance.ui.SelectWindow = Ext.extend( Ext.Window, {
    // Localization
    title: 'Select an option',
    OKText: 'OK',
    cancelText: 'Cancel',
	
    constructor: function(cfg) {
        Ext.apply(this, cfg);
		
        QFinance.ui.SelectWindow.superclass.constructor.call(this);
    },
	
    initComponent: function() {
        Ext.apply(this, {			
            //resizable: false,
            constrain: true,
            buttonAlign: 'center',
            autoHeight: true,            
            width: 300,
            closable: true,
            plain: true,
            shim: true,
            border: false,
            items: [{
                layout: 'anchor',
                style: 'padding: 10px',
                defaults: {
                    anchor: '100%'
                },
                items: [this.selectComboBox = new Ext.form.ComboBox({				
                    xtype: 'combo',		    		
                    fieldLabel: '',		    		
                    typeAhead: true,		    		
                    triggerAction: 'all',
                    emptyText: this.title,
                    selectOnFocus: true,
                    forceSelection: true,
                    store: this.options,
                    hiddenName: 'bankID',
                    name: 'bank'
                })]
            }],			
            fbar: {
                autoWidth: true,
                autoHeight: true,
                items: [{
                    text: this.OKText,
                    scope: this,
                    autoWidth: true,
                    handler: function() {
                        if (this.selectComboBox.selectedIndex >= 0) {
                            this.close();
                            Ext.callback(this.handler, this.scope||this, [ this.selectComboBox.getValue() ], 1);
                        }
                    }
                },{
                    text: this.cancelText,
                    scope: this,
                    autoWidth: true,
                    handler: function() {
                        this.close();
                    }
                }]
            }
        });
        QFinance.ui.SelectWindow.superclass.initComponent.call(this);
    }
});