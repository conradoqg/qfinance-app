/**
 * @depends ui.js
 */
QFinance.ui.Window = Ext.extend( Ext.Window, {   
    // Localization
    waitingMessageSingular: 'Processing {0} item.',
    waitingMessagePlural: 'Processing {0} items.',
    errorMessageTitle: 'Processing error',
    infoMessageTitle: 'Processing info',
    
    initComponent: function() {    
        Ext.applyIf(this, {
            processStatusManager: new QFinance.ui.ProcessStatusManager()
        });

        if (this.ownerWindow) {
            Ext.apply(this, {
                renderTo: this.ownerWindow.body,
                modal: true,
                constrain: true                
            });
            this.ownerWindow.on('restore', function(window) {                
                this.doConstrain();
            }, this);
        }
        Ext.apply(this, {
            bbar: new QFinance.ui.StatusBar(this.bbar)
        });
                
        this.processStatusManager.on('statusupdate', this.onStatusUpdate, this);        
        
        QFinance.ui.Window.superclass.initComponent.call(this);
        
        this.doInitStatusBar();
    },
    
    doInitStatusBar: function() {
        if (this.initStatusBar)
            this.initStatusBar( this.bottomToolbar );
        var panels = this.findByType('qpanel');
        Ext.each(panels, function(component) {
            if (component.initStatusBar)
                component.initStatusBar( this.bottomToolbar ); 
        }, this);
    },
    
    getProcessStatusManager: function() {
        return this.processStatusManager;
    },
    
    setProcessStatusManager: function(processStatusManager) {
        this.processStatusManager = processStatusManager;
    },
    
    showErrorMessageBox: function(title, message) {
        var messageBox = new QFinance.ui.MessageBox();
        messageBox.show({					
            title: title,
            buttons: messageBox.OK,
            icon: messageBox.ERROR,
            msg: message,
            renderTo: this.body,                
            modal: true,
            minWidth: 200,
            scope: this
        });   
    },
    
    showInfoMessageBox: function(title, message) {
        var messageBox = new QFinance.ui.MessageBox();
        messageBox.show({					
            title: title,
            buttons: messageBox.OK,
            icon: messageBox.INFO,
            msg: message,
            renderTo: this.body,                
            modal: true,
            minWidth: 200,
            scope: this
        });    
    },
    
    onStatusUpdate: function(processStatusManager, processes) {                
        var runningCount = 0;
        var componentsToDisable = [];
        Ext.each(processes, function(process) {
            if (process.status == QFinance.ui.ProcessStatusManager.STARTED) {                                
                // Status count
                runningCount++;
                // Disabled components
                Ext.each(process.components, function(component) {
                    if (componentsToDisable.indexOf(component) == -1)
                        componentsToDisable.push(component);
                });
            }
        });

        // Update toolbar message e components status
        if (this.rendered) {
            if (runningCount > 0) {
                var busyMessage = '';
                busyMessage = runningCount == 1 ? String.format(this.waitingMessageSingular, runningCount) : String.format(this.waitingMessagePlural, runningCount)
                this.bottomToolbar.showBusy(busyMessage);
            } else {
                this.bottomToolbar.clear();
            }
                                    
            Ext.each(componentsToDisable, function(component) {
                if (component && component.rendered) {
                    if (component.isXType('window')) {                   
                        component.body.mask();
                        if (component.topToolbar && component.topToolbar.getEl()) {
                            component.topToolbar.getEl().mask();
                        }
                    } else if (component.isXType('container')) {
                        component.getEl().mask();
                    } else {
                        component.disable();
                    }
                }
            });
        }
        
        if (runningCount == 0) {
            var endedSuccessfullCount = 0;
            var endedErrorfullCount = 0;
            var errorMessage = '';
            var infoMessage = '';
            var componentsToEnable = []; 
            componentsToDisable = [];
            Ext.each(processes, function(process) {
                if (process.status == QFinance.ui.ProcessStatusManager.ENDED) {                
                    // Status count
                    endedSuccessfullCount++;                
                    // Message
                    if (process.message && !process.showed) {
                        if (infoMessage.length > 0)
                            infoMessage += '<br>';
                        infoMessage += process.message;
                        process.showed = true;
                    }
                    // Enabled components
                    Ext.each(process.components, function(component) {
                        if (componentsToEnable.indexOf(component) == -1)
                            componentsToEnable.push(component);
                    });                
                } else if (process.status == QFinance.ui.ProcessStatusManager.ERROR) {                
                    // Status count
                    endedErrorfullCount++
                    // Message
                    if (process.message && !process.showed) {
                        if (errorMessage.length > 0)
                            errorMessage += '<br>';
                        errorMessage += process.message; 
                        process.showed = true;
                    }
                    // Enabled components
                    Ext.each(process.components, function(component) {
                        if (componentsToDisable.indexOf(component) == -1)
                            componentsToDisable.push(component);
                    });
                }
            });
            
            if (this.rendered) {
                // Update components status
                Ext.each(componentsToEnable, function(component) {
                    if (component && component.rendered) {
                        if (component.isXType('window')) {                   
                            component.body.unmask();
                            if (component.topToolbar && component.topToolbar.getEl()) {
                                component.topToolbar.getEl().unmask();
                            }
                        } else if (component.isXType('container')) {
                            component.getEl().unmask();
                        } else {
                            component.enable();
                        }
                    }
                });   
            }
            
            var showErrorMessage = function() {           
                var messageBox = new QFinance.ui.MessageBox();
                messageBox.show({					
                    title: this.errorMessageTitle,
                    buttons: messageBox.OK,
                    icon: messageBox.ERROR,
                    msg: errorMessage,
                    renderTo: this.body,                
                    modal: true,
                    minWidth: 200,
                    scope: this,
                    fn: function() {
                        Ext.each(componentsToDisable, function(component) {
                            if (component && component.rendered) {
                                if (component.isXType('window')) {                   
                                    component.body.mask();
                                    if (component.topToolbar && component.topToolbar.getEl()) {
                                        component.topToolbar.getEl().mask();
                                    }
                                } else if (component.isXType('container')) {
                                    component.getEl().mask();
                                } else {
                                    component.disable();
                                }
                            }
                        });
                    }
                });   
            };

            var showInfoMessage = function() {            
                var messageBox = new QFinance.ui.MessageBox();
                messageBox.show({					
                    title: this.infoMessageTitle,
                    buttons: messageBox.OK,
                    icon: messageBox.INFO,
                    msg: infoMessage,
                    renderTo: this.body,                
                    modal: true,
                    minWidth: 200,
                    scope: this
                });    
            };

            if (this.rendered) {
                if (errorMessage.length > 0)
                    showErrorMessage.call(this);
                if (infoMessage.length > 0)
                    showInfoMessage.call(this);
            } else {
                if (errorMessage.length > 0)
                    this.on('show', showErrorMessage);
                if (infoMessage.length > 0)
                    this.on('show', showInfoMessage);
            }
        } 
    }
});
Ext.reg('qwindow', QFinance.ui.Window);