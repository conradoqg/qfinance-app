/*
 * @depends ui.js, Panel.js
 */
QFinance.ui.WizardPanel = Ext.extend( QFinance.ui.Panel, {
    // Localization
    navigatorNextText: 'Next',
    navigatorPreviousText: 'Previous',
    navigatorFinishText: 'Finish',
	
    // Navigator bar items
    previousButton: null,
    nextButton: null,
    finishButton: null,
    activeItem: 0,
		
    constructor: function(config) {
        Ext.apply(this, config);
		
        this.addEvents([ 
            'beforeitemchange',
            'itemchange',
            'beforefinish',
            'finish'
            ]);		
        QFinance.ui.WizardPanel.superclass.constructor.call(this);
    },
	
    initComponent: function() {
        Ext.apply(this, {
            layout: 'card',			
            items: []
        });
		
        QFinance.ui.WizardPanel.superclass.initComponent.call(this);
    },

    initStatusBar: function(toolbar) {
        toolbar.insert(0,this.finishButton = new Ext.Button({
            text: this.navigatorFinishText,
            disabled: true,
            scope: this,
            handler: this.finish,
            iconCls: 'fugue-flag-black'
        }));
		
        toolbar.insert(0,this.nextButton =new Ext.Button({
            text: this.navigatorNextText,
            scope: this,
            handler: this.next,
            iconAlign: 'right',
            iconCls: 'fugue-arrow'
        }));
		
        toolbar.insert(0,this.previousButton = new Ext.Button({
            text: this.navigatorPreviousText,
            disabled: true,
            scope: this,
            handler: this.previous,
            iconCls: 'fugue-arrow-180'
        }));
    },
		
    previous: function() {
        if (this.activeItem > 0)
            this.activateItem(this.activeItem - 1);
    },
	
    next: function() {
        if (this.activeItem < this.items.getCount())
            this.activateItem(this.activeItem + 1);
    },
	
    finish: function() {
        if (this.fireEvent('beforefinish', this) !== false)
            this.fireEvent('finish', this);
    },

    activateItem: function( index ) {
        if (this.fireEvent('beforeitemchange', this, this.items.get(this.activeItem), this.items.get(index)) !== false) {
            var fromIndex = this.activeItem;
            this.previousButton.setDisabled(index == 0);
            this.nextButton.setDisabled(index == (this.items.getCount() - 1));
            this.finishButton.setDisabled(index != (this.items.getCount() - 1));
            this.getLayout().setActiveItem(index);
            this.activeItem = index;
            this.fireEvent('itemchange', this, this.items.get(fromIndex), this.items.get(index));
        }
    }
});
Ext.reg('QFinance.ui.WizardPanel', QFinance.ui.WizardPanel);