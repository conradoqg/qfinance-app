/**
 * @depends ui.js
 */
QFinance.ui.Panel = Ext.extend( Ext.Panel, {
    constructor: function(cfg) {                        
        QFinance.ui.Panel.superclass.constructor.call(this, cfg);
    },
    
    initComponent: function() {
        QFinance.ui.Panel.superclass.initComponent.call(this);
    },
    
    getOwnerWindow: function() {
        return this.findParentByType('qwindow');
    },
    
    getProcessStatusManager: function() {
        if (!this.processStatusManager) {
            var ownerWindow = this.getOwnerWindow();
            if (ownerWindow)               
                this.processStatusManager = ownerWindow.getProcessStatusManager();
            else
                this.processStatusManager = new QFinance.ui.ProcessStatusManager();
        }
        return this.processStatusManager;
    },
    
    setProcessStatusManager: function(processStatusManager) {
        this.processStatusManager = processStatusManager;
    }
});
Ext.reg('qpanel', QFinance.ui.Panel);