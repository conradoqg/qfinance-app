/**
 * @depends ui.js
 */
QFinance.ui.ProcessStatusManager = Ext.extend( Ext.util.Observable, {
    constructor: function(cfg) {
        this.processes = [];
        
        Ext.apply(cfg);
        this.addEvents('statusupdate');
    },
    
    start: function(components) {
        var id = Ext.id();
        this.processes.push({
            id: id,
            status: QFinance.ui.ProcessStatusManager.STARTED,
            components: components,
            message: ''
        });        
        this.doStatusUpdate();
        return id;
    },
    
    success: function(id, message) {
        this.end(id, QFinance.ui.ProcessStatusManager.ENDED, message);
    },
    
    error: function(id, message) {
        this.end(id, QFinance.ui.ProcessStatusManager.ERROR, message);
    },
    
    end: function(id, status, message) {
        for (var i = 0; i < this.processes.length; i++) {
            if (this.processes[i].id == id) {                
                this.processes[i].status = status;
                this.processes[i].message = message;              
            }
        }                
        this.doStatusUpdate();
    },
        
    doStatusUpdate: function() {                
        this.fireEvent('statusupdate', this, this.processes);
    }
});

QFinance.ui.ProcessStatusManager.STARTED = 0;
QFinance.ui.ProcessStatusManager.ENDED = 1;
QFinance.ui.ProcessStatusManager.ERROR = 2;