/**
 * @depends ui.js
 */
QFinance.ui.StatusBar = Ext.extend( Ext.ux.StatusBar, {
    showBusy: function(message) {
        this.setStatus({
            text: message,
            iconCls: 'x-status-busy'
        });	
    },

    clear: function() {
        this.clearStatus({
            useDefaults: false
        });
    }
});
Ext.reg('qstatusbar', QFinance.ui.StatusBar);