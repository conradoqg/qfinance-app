/*
 * @depends ui.js
 */
QFinance.ui.Portlet = Ext.extend( Ext.ux.Portlet, {
    configurationWindow: null,
    iconCls: 'puzzle',
    internalID: null,
    refreshText: 'Refresh',
    configureText: 'Configure',
    closeText: 'Close',
	
    initComponent: function() {
        this.initTools();
        QFinance.ui.Portlet.superclass.initComponent.call(this);
    },
	
    initTools: function() {
        this.tools = [{
            id:'refresh',
            qtip: this.refreshText,
            scope: this,
            handler: function(e, target, panel){
                this.loadData(panel.ownerCt.ownerCt);
            }
        },{
            id:'gear',
            qtip: this.configureText,
            scope: this,
            handler: function(e, target, panel){
                this.configure(panel.ownerCt.ownerCt);
            }
        },{
            id:'close',
            qtip: this.closeText,
            scope: this,
            handler: function(e, target, panel){
                if (this.internalID)
                    panel.ownerCt.ownerCt.markToRemove(this);
                panel.ownerCt.remove(panel, true);
            }
        }];
    },
	
    configure: function(portal, callback, scope) {
        if (this.configurationWindow) {
            var cfg = {
                configuration: this.configuration, 
                ownerWindow: portal.ownerCt,
                listeners: {
                    scope: this,
                    save: function( configuration ) {
                        this.configuration = configuration;
                        if (callback)
                            callback.call(scope || this);
                        this.loadData();
                    }
                }
            };
            var configurationWindowInstance = eval('new QFinance.portlet.' + this.configurationWindow + '(cfg)');
            if (configurationWindowInstance) {				
                configurationWindowInstance.show();
            }
        } else {
            if (callback)
                callback.call(scope || this);
            this.loadData();
        }
    }
});