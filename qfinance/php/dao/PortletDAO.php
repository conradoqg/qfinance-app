<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2012-03-05 02:55
 */
interface PortletDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @Return Portlet 
	 */
	public function load($id);

	/**
	 * Get all records from table
	 */
	public function queryAll();
	
	/**
	 * Get all records from table ordered by field
	 * @Param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn);
	
	/**
 	 * Delete record from table
 	 * @param portlet primary key
 	 */
	public function delete($id);
	
	/**
 	 * Insert record to table
 	 *
 	 * @param Portlet portlet
 	 */
	public function insert($portlet);
	
	/**
 	 * Update record in table
 	 *
 	 * @param Portlet portlet
 	 */
	public function update($portlet);	

	/**
	 * Delete all rows
	 */
	public function clean();

		public function queryByClassName($value);

	public function queryByConfiguration($value);

	public function queryByLin($value);

	public function queryByCol($value);


		public function deleteByClassName($value);

	public function deleteByConfiguration($value);

	public function deleteByLin($value);

	public function deleteByCol($value);


}
?>