<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2012-03-05 02:55
 */
interface MarkDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @Return Mark 
	 */
	public function load($id);

	/**
	 * Get all records from table
	 */
	public function queryAll();
	
	/**
	 * Get all records from table ordered by field
	 * @Param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn);
	
	/**
 	 * Delete record from table
 	 * @param mark primary key
 	 */
	public function delete($id);
	
	/**
 	 * Insert record to table
 	 *
 	 * @param Mark mark
 	 */
	public function insert($mark);
	
	/**
 	 * Update record in table
 	 *
 	 * @param Mark mark
 	 */
	public function update($mark);	

	/**
	 * Delete all rows
	 */
	public function clean();

		public function queryByName($value);

	public function queryByDescription($value);


		public function deleteByName($value);

	public function deleteByDescription($value);


}
?>