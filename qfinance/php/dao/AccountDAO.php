<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2012-03-05 02:55
 */
interface AccountDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @Return Account 
	 */
	public function load($id);

	/**
	 * Get all records from table
	 */
	public function queryAll();
	
	/**
	 * Get all records from table ordered by field
	 * @Param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn);
	
	/**
 	 * Delete record from table
 	 * @param account primary key
 	 */
	public function delete($id);
	
	/**
 	 * Insert record to table
 	 *
 	 * @param Account account
 	 */
	public function insert($account);
	
	/**
 	 * Update record in table
 	 *
 	 * @param Account account
 	 */
	public function update($account);	

	/**
	 * Delete all rows
	 */
	public function clean();

		public function queryByAccountName($value);

	public function queryByBankNumber($value);

	public function queryByBankAccount($value);

	public function queryByAccountType($value);

	public function queryByInitialAmount($value);

	public function queryByBankAgency($value);


		public function deleteByAccountName($value);

	public function deleteByBankNumber($value);

	public function deleteByBankAccount($value);

	public function deleteByAccountType($value);

	public function deleteByInitialAmount($value);

	public function deleteByBankAgency($value);


}
?>