<?php
/**
 * DAOFactory
 * @author: http://phpdao.com
 * @date: ${date}
 */
class DAOFactory{
	
	/**
	 * @return AccountExtDAO
	 */
	public static function getAccountDAO(){
		return new AccountMySqlExtDAO();
	}

	/**
	 * @return CategoryExtDAO
	 */
	public static function getCategoryDAO(){
		return new CategoryMySqlExtDAO();
	}

	/**
	 * @return MarkExtDAO
	 */
	public static function getMarkDAO(){
		return new MarkMySqlExtDAO();
	}

	/**
	 * @return PortletExtDAO
	 */
	public static function getPortletDAO(){
		return new PortletMySqlExtDAO();
	}

	/**
	 * @return TransactionExtDAO
	 */
	public static function getTransactionDAO(){
		return new TransactionMySqlExtDAO();
	}

	/**
	 * @return UserExtDAO
	 */
	public static function getUserDAO(){
		return new UserMySqlExtDAO();
	}


}
?>