<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2010-11-29 23:51
 */
interface TransactionExtDAO extends TransactionDAO{
	public function queryTransactionsViewThroughOptions( $options );
	
	public function queryTransactionsViewWhereAccountIDAndTemporaryID( $accountID, $temporaryID );
	
	public function queryTransactionsViewWhereAccountIDAndTemporaryIDAndParentTransactionID( $accountID, $temporaryID, $parentTransactionID );
	
	public function queryTransactionsViewWhereAccountIDAndNotTemporary( $accountID );
	
	public function queryTransactionsViewWhereAccountIDAndNotTemporaryAndParentTransactionID( $accountID, $parentTransactionID );
	
	public function hasSubtransactions( $transactionID );
	
	public function queryAllView();

	public function queryTransactionsOfAccountView( $accountID );
	
	public function updateTransactionsTemporaryID( $fromTemporaryID, $toTemporaryID );
	
	public function queryBetweenDatesAndAccountNotParentAndNotTemporary( $startDate, $endDate, $accountID );
}
?>