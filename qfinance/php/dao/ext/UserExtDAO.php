<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2010-11-27 23:48
 */
interface UserExtDAO extends UserDAO{
	public function queryByUsernameAndPassword( $username, $password );
}
?>