<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2010-11-29 23:51
 */
interface CategoryExtDAO extends CategoryDAO {
	function queryRootCategories();
}
?>