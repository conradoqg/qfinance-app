<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2010-11-29 23:51
 */
interface AccountExtDAO extends AccountDAO {
	function queryAllMinus( $excludeAccount );
	function queryAllView();
}
?>