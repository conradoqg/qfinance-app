<?php
class TransactionExtDAOOptions {
	public $accountID = null;
	public $temporaryID = null;
	public $parentTransactionID = null;
	public $startDate = null;
	public $endDate = null;
	public $parentCategoryID = null;
	public $onlySourceAccountID = null;
	public $onlyDestinationAccountID = null;
	public $annotation = null;
	public $notPrivateToPrivate = null;
	public $categoryID = null;	
        public $markID = null;
}
?>