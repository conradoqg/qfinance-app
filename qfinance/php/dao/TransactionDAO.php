<?php
/**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2012-03-05 02:55
 */
interface TransactionDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @Return Transaction 
	 */
	public function load($id);

	/**
	 * Get all records from table
	 */
	public function queryAll();
	
	/**
	 * Get all records from table ordered by field
	 * @Param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn);
	
	/**
 	 * Delete record from table
 	 * @param transaction primary key
 	 */
	public function delete($id);
	
	/**
 	 * Insert record to table
 	 *
 	 * @param Transaction transaction
 	 */
	public function insert($transaction);
	
	/**
 	 * Update record in table
 	 *
 	 * @param Transaction transaction
 	 */
	public function update($transaction);	

	/**
	 * Delete all rows
	 */
	public function clean();

		public function queryBySourceAccountID($value);

	public function queryByDestinationAccountID($value);

	public function queryByAmount($value);

	public function queryByDescription($value);

	public function queryByBankMemo($value);

	public function queryByDate($value);

	public function queryByFinancialNumber($value);

	public function queryByParentTransactionID($value);

	public function queryByCategoryID($value);

	public function queryByTemporaryID($value);

	public function queryByAnnotation($value);

	public function queryByMarkID($value);


		public function deleteBySourceAccountID($value);

	public function deleteByDestinationAccountID($value);

	public function deleteByAmount($value);

	public function deleteByDescription($value);

	public function deleteByBankMemo($value);

	public function deleteByDate($value);

	public function deleteByFinancialNumber($value);

	public function deleteByParentTransactionID($value);

	public function deleteByCategoryID($value);

	public function deleteByTemporaryID($value);

	public function deleteByAnnotation($value);

	public function deleteByMarkID($value);


}
?>