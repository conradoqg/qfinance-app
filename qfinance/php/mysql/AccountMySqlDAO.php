<?php
/**
 * Class that operate on table 'account'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2012-03-05 02:55
 */
class AccountMySqlDAO implements AccountDAO {

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return AccountMySql 
	 */
	public function load($id){
		$sql = 'SELECT * FROM account WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM account';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM account ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param account primary key
 	 */
	public function delete($id){
		$sql = 'DELETE FROM account WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param AccountMySql account
 	 */
	public function insert($account){
		$sql = 'INSERT INTO account (accountName, bankNumber, bankAccount, accountType, initialAmount, bankAgency) VALUES (?, ?, ?, ?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($account->accountName);
		$sqlQuery->set($account->bankNumber);
		$sqlQuery->set($account->bankAccount);
		$sqlQuery->setNumber($account->accountType);
		$sqlQuery->set($account->initialAmount);
		$sqlQuery->set($account->bankAgency);

		$id = $this->executeInsert($sqlQuery);	
		$account->id = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param AccountMySql account
 	 */
	public function update($account){
		$sql = 'UPDATE account SET accountName = ?, bankNumber = ?, bankAccount = ?, accountType = ?, initialAmount = ?, bankAgency = ? WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($account->accountName);
		$sqlQuery->set($account->bankNumber);
		$sqlQuery->set($account->bankAccount);
		$sqlQuery->setNumber($account->accountType);
		$sqlQuery->set($account->initialAmount);
		$sqlQuery->set($account->bankAgency);

		$sqlQuery->setNumber($account->id);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'DELETE FROM account';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}

		public function queryByAccountName($value){
		$sql = 'SELECT * FROM account WHERE accountName = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByBankNumber($value){
		$sql = 'SELECT * FROM account WHERE bankNumber = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByBankAccount($value){
		$sql = 'SELECT * FROM account WHERE bankAccount = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByAccountType($value){
		$sql = 'SELECT * FROM account WHERE accountType = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByInitialAmount($value){
		$sql = 'SELECT * FROM account WHERE initialAmount = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByBankAgency($value){
		$sql = 'SELECT * FROM account WHERE bankAgency = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}


		public function deleteByAccountName($value){
		$sql = 'DELETE FROM account WHERE accountName = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByBankNumber($value){
		$sql = 'DELETE FROM account WHERE bankNumber = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByBankAccount($value){
		$sql = 'DELETE FROM account WHERE bankAccount = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByAccountType($value){
		$sql = 'DELETE FROM account WHERE accountType = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByInitialAmount($value){
		$sql = 'DELETE FROM account WHERE initialAmount = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByBankAgency($value){
		$sql = 'DELETE FROM account WHERE bankAgency = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}


	
	/**
	 * Read row
	 *
	 * @return AccountMySql 
	 */
	protected function readRow($row){
		$account = new Account();
		
		$account->id = $row['id'];
		$account->accountName = $row['accountName'];
		$account->bankNumber = $row['bankNumber'];
		$account->bankAccount = $row['bankAccount'];
		$account->accountType = $row['accountType'];
		$account->initialAmount = $row['initialAmount'];
		$account->bankAgency = $row['bankAgency'];

		return $account;
	}
	
	protected function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Get row
	 *
	 * @return AccountMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab)==0){
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Query for one row and one column
	 */
	protected function querySingleResult($sqlQuery){
		return QueryExecutor::queryForString($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>