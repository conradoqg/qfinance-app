<?php
/**
 * Class that operate on table 'transaction'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2012-03-05 02:55
 */
class TransactionMySqlDAO implements TransactionDAO {

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return TransactionMySql 
	 */
	public function load($id){
		$sql = 'SELECT * FROM transaction WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM transaction';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM transaction ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param transaction primary key
 	 */
	public function delete($id){
		$sql = 'DELETE FROM transaction WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param TransactionMySql transaction
 	 */
	public function insert($transaction){
		$sql = 'INSERT INTO transaction (sourceAccountID, destinationAccountID, amount, description, bankMemo, date, financialNumber, parentTransactionID, categoryID, temporaryID, annotation, markID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->setNumber($transaction->sourceAccountID);
		$sqlQuery->setNumber($transaction->destinationAccountID);
		$sqlQuery->set($transaction->amount);
		$sqlQuery->set($transaction->description);
		$sqlQuery->set($transaction->bankMemo);
		$sqlQuery->set($transaction->date);
		$sqlQuery->set($transaction->financialNumber);
		$sqlQuery->setNumber($transaction->parentTransactionID);
		$sqlQuery->setNumber($transaction->categoryID);
		$sqlQuery->setNumber($transaction->temporaryID);
		$sqlQuery->setNumber($transaction->annotation);
		$sqlQuery->setNumber($transaction->markID);

		$id = $this->executeInsert($sqlQuery);	
		$transaction->id = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param TransactionMySql transaction
 	 */
	public function update($transaction){
		$sql = 'UPDATE transaction SET sourceAccountID = ?, destinationAccountID = ?, amount = ?, description = ?, bankMemo = ?, date = ?, financialNumber = ?, parentTransactionID = ?, categoryID = ?, temporaryID = ?, annotation = ?, markID = ? WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->setNumber($transaction->sourceAccountID);
		$sqlQuery->setNumber($transaction->destinationAccountID);
		$sqlQuery->set($transaction->amount);
		$sqlQuery->set($transaction->description);
		$sqlQuery->set($transaction->bankMemo);
		$sqlQuery->set($transaction->date);
		$sqlQuery->set($transaction->financialNumber);
		$sqlQuery->setNumber($transaction->parentTransactionID);
		$sqlQuery->setNumber($transaction->categoryID);
		$sqlQuery->setNumber($transaction->temporaryID);
		$sqlQuery->setNumber($transaction->annotation);
		$sqlQuery->setNumber($transaction->markID);

		$sqlQuery->setNumber($transaction->id);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'DELETE FROM transaction';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}

		public function queryBySourceAccountID($value){
		$sql = 'SELECT * FROM transaction WHERE sourceAccountID = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByDestinationAccountID($value){
		$sql = 'SELECT * FROM transaction WHERE destinationAccountID = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByAmount($value){
		$sql = 'SELECT * FROM transaction WHERE amount = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByDescription($value){
		$sql = 'SELECT * FROM transaction WHERE description = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByBankMemo($value){
		$sql = 'SELECT * FROM transaction WHERE bankMemo = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByDate($value){
		$sql = 'SELECT * FROM transaction WHERE date = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByFinancialNumber($value){
		$sql = 'SELECT * FROM transaction WHERE financialNumber = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByParentTransactionID($value){
		$sql = 'SELECT * FROM transaction WHERE parentTransactionID = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByCategoryID($value){
		$sql = 'SELECT * FROM transaction WHERE categoryID = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByTemporaryID($value){
		$sql = 'SELECT * FROM transaction WHERE temporaryID = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByAnnotation($value){
		$sql = 'SELECT * FROM transaction WHERE annotation = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByMarkID($value){
		$sql = 'SELECT * FROM transaction WHERE markID = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}


		public function deleteBySourceAccountID($value){
		$sql = 'DELETE FROM transaction WHERE sourceAccountID = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByDestinationAccountID($value){
		$sql = 'DELETE FROM transaction WHERE destinationAccountID = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByAmount($value){
		$sql = 'DELETE FROM transaction WHERE amount = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByDescription($value){
		$sql = 'DELETE FROM transaction WHERE description = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByBankMemo($value){
		$sql = 'DELETE FROM transaction WHERE bankMemo = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByDate($value){
		$sql = 'DELETE FROM transaction WHERE date = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByFinancialNumber($value){
		$sql = 'DELETE FROM transaction WHERE financialNumber = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByParentTransactionID($value){
		$sql = 'DELETE FROM transaction WHERE parentTransactionID = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByCategoryID($value){
		$sql = 'DELETE FROM transaction WHERE categoryID = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByTemporaryID($value){
		$sql = 'DELETE FROM transaction WHERE temporaryID = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByAnnotation($value){
		$sql = 'DELETE FROM transaction WHERE annotation = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByMarkID($value){
		$sql = 'DELETE FROM transaction WHERE markID = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}


	
	/**
	 * Read row
	 *
	 * @return TransactionMySql 
	 */
	protected function readRow($row){
		$transaction = new Transaction();
		
		$transaction->id = $row['id'];
		$transaction->sourceAccountID = $row['sourceAccountID'];
		$transaction->destinationAccountID = $row['destinationAccountID'];
		$transaction->amount = $row['amount'];
		$transaction->description = $row['description'];
		$transaction->bankMemo = $row['bankMemo'];
		$transaction->date = $row['date'];
		$transaction->financialNumber = $row['financialNumber'];
		$transaction->parentTransactionID = $row['parentTransactionID'];
		$transaction->categoryID = $row['categoryID'];
		$transaction->temporaryID = $row['temporaryID'];
		$transaction->annotation = $row['annotation'];
		$transaction->markID = $row['markID'];

		return $transaction;
	}
	
	protected function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Get row
	 *
	 * @return TransactionMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab)==0){
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Query for one row and one column
	 */
	protected function querySingleResult($sqlQuery){
		return QueryExecutor::queryForString($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>