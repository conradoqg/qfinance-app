<?php
/**
 * Class that operate on table 'account'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2010-11-29 23:51
 */
class AccountMySqlExtDAO extends AccountMySqlDAO implements AccountExtDAO {
	public static $privateText = 'Private';
	public static $publicText = 'Public';

	function queryAllMinus($excludeAccount) {
		$sql = 'SELECT * FROM account WHERE id <> ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($excludeAccount);
		return $this->getList($sqlQuery);
	}
	
	function queryAllView() {
		$sql = 'SELECT * FROM account';
		$sqlQuery = new SqlQuery($sql);			
		return $this->getListView($sqlQuery);
	}
	
	protected function getListView($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRowView($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Read row
	 *
	 * @return AccountMySql 
	 */
	protected function readRowView($row){
		$account = new AccountView();
		
		$account->id = $row['id'];
		$account->accountName = $row['accountName'];
		$account->bankNumber = $row['bankNumber'];
		$account->bankAccount = $row['bankAccount'];
		$account->accountType = $row['accountType'];
		$account->accountTypeName = ($row['accountType'] == 1 ? self::$privateText : self::$publicText);
		$account->initialAmount = $row['initialAmount'];
		$account->bankAgency = $row['bankAgency'];			

		return $account;
	}		
}
?>