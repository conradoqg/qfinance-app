<?php
/**
 * Class that operate on table 'category'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2010-11-29 23:51
 */
class CategoryMySqlExtDAO extends CategoryMySqlDAO implements CategoryExtDAO{
	function queryRootCategories() {
		$sql = 'SELECT * FROM category WHERE parentCategoryID IS NULL';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}		
}
?>