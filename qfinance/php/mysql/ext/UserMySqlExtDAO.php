<?php
/**
 * Class that operate on table 'user'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2010-11-27 23:48
 */
class UserMySqlExtDAO extends UserMySqlDAO implements UserExtDAO {
	public function queryByUsernameAndPassword( $username, $password ) {
		$sql = 'SELECT * FROM user WHERE username = ? AND password = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($username);
		$sqlQuery->set($password);
		return $this->getRow($sqlQuery);
	}	
}
?>