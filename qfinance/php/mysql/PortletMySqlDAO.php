<?php
/**
 * Class that operate on table 'portlet'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2012-03-05 02:55
 */
class PortletMySqlDAO implements PortletDAO {

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return PortletMySql 
	 */
	public function load($id){
		$sql = 'SELECT * FROM portlet WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM portlet';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM portlet ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param portlet primary key
 	 */
	public function delete($id){
		$sql = 'DELETE FROM portlet WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param PortletMySql portlet
 	 */
	public function insert($portlet){
		$sql = 'INSERT INTO portlet (className, configuration, lin, col) VALUES (?, ?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($portlet->className);
		$sqlQuery->set($portlet->configuration);
		$sqlQuery->setNumber($portlet->lin);
		$sqlQuery->setNumber($portlet->col);

		$id = $this->executeInsert($sqlQuery);	
		$portlet->id = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param PortletMySql portlet
 	 */
	public function update($portlet){
		$sql = 'UPDATE portlet SET className = ?, configuration = ?, lin = ?, col = ? WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($portlet->className);
		$sqlQuery->set($portlet->configuration);
		$sqlQuery->setNumber($portlet->lin);
		$sqlQuery->setNumber($portlet->col);

		$sqlQuery->setNumber($portlet->id);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'DELETE FROM portlet';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}

		public function queryByClassName($value){
		$sql = 'SELECT * FROM portlet WHERE className = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByConfiguration($value){
		$sql = 'SELECT * FROM portlet WHERE configuration = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->getList($sqlQuery);
	}

	public function queryByLin($value){
		$sql = 'SELECT * FROM portlet WHERE lin = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}

	public function queryByCol($value){
		$sql = 'SELECT * FROM portlet WHERE col = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}


		public function deleteByClassName($value){
		$sql = 'DELETE FROM portlet WHERE className = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByConfiguration($value){
		$sql = 'DELETE FROM portlet WHERE configuration = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByLin($value){
		$sql = 'DELETE FROM portlet WHERE lin = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}

	public function deleteByCol($value){
		$sql = 'DELETE FROM portlet WHERE col = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->executeUpdate($sqlQuery);
	}


	
	/**
	 * Read row
	 *
	 * @return PortletMySql 
	 */
	protected function readRow($row){
		$portlet = new Portlet();
		
		$portlet->id = $row['id'];
		$portlet->className = $row['className'];
		$portlet->configuration = $row['configuration'];
		$portlet->lin = $row['lin'];
		$portlet->col = $row['col'];

		return $portlet;
	}
	
	protected function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Get row
	 *
	 * @return PortletMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab)==0){
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Query for one row and one column
	 */
	protected function querySingleResult($sqlQuery){
		return QueryExecutor::queryForString($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>