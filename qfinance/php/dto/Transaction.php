<?php
/**
 * Object represents table 'transaction'
 *
 * @author: http://phpdao.com
 * @date: 2012-03-05 02:55	 
 */
class Transaction{
	
		var $id;
		var $sourceAccountID;
		var $destinationAccountID;
		var $amount;
		var $description;
		var $bankMemo;
		var $date;
		var $financialNumber;
		var $parentTransactionID;
		var $categoryID;
		var $temporaryID;
		var $annotation;
		var $markID;
		
}
?>