<?php
/**
 * Object represents table 'account'
 *
 * @author: http://phpdao.com
 * @date: 2012-03-05 02:55	 
 */
class Account{
	
		var $id;
		var $accountName;
		var $bankNumber;
		var $bankAccount;
		var $accountType;
		var $initialAmount;
		var $bankAgency;
		
}
?>