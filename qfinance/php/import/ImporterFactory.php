<?php
class ImporterFactory {
	/**
	 * Factory importer class for bank
	 * @param int $bankID Bank id
	 * @return BankDataImporter Importer
	 */
	public static function createImporterFromID( $bankID ) {
		$importer = null;
		
		if ($bankID == 1) {
			$importer = new RealImporter();
		} else if ($bankID == 2) {
			$importer = new ItauImporter();
		} else if ($bankID == 3) {
			$importer = new CaixaImporter();
		}
		
		return $importer;
	}
}
?>