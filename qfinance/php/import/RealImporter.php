<?php
class RealImporter implements DataBankImporter {
	public function getTransactionsFromFile( $filePath, $accountID ) {
		$ofxParser = new OFXParser();
		$ofxParser->loadFromFile( $filePath );
		
		$transactions = array();
		
		foreach ($ofxParser->moviments as $moviment) {
			$transaction = new Transaction();
			
			$transaction->bankMemo = $moviment['MEMO'];
			if (((float)$moviment['TRNAMT']) > 0)
				$transaction->destinationAccountID = $accountID;
			else
				$transaction->sourceAccountID = $accountID;
			
                        if (strpos($moviment['TRNAMT'],",") > 0)
                            $moviment['TRNAMT'] = str_replace (",", ".", $moviment['TRNAMT']);
			$transaction->amount = abs((float)$moviment['TRNAMT']);
			$transaction->date = substr($moviment['DTPOSTED'], 0, 4) . '-' . substr($moviment['DTPOSTED'], 4, 2) . '-' . substr($moviment['DTPOSTED'], 6, 2) ;
			$transaction->financialNumber = $moviment['FITID'];
			$transactions[] = $transaction;
		}
		return $transactions;
	}	
}
?>