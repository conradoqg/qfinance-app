<?php
interface DataBankImporter {
	public function getTransactionsFromFile( $filePath, $accountID );
}
?>