<?php
class ItauImporter implements DataBankImporter {
	public function getTransactionsFromFile( $filePath, $accountID ) {
		$ofxParser = new OFXParser();
		$ofxParser->loadFromFile( $filePath );
		
		$transactions = array();
		
		foreach ($ofxParser->moviments as $moviment) {
			$transaction = new Transaction();
			
			$transaction->bankMemo = $moviment['MEMO'];
			if ($moviment['TRNTYPE'] == 'DEBIT')
				$transaction->sourceAccountID = $accountID;
			else if ($moviment['TRNTYPE'] == 'CREDIT')
				$transaction->destinationAccountID = $accountID;
			
			$transaction->amount = abs((float)$moviment['TRNAMT']);
			$transaction->date = substr($moviment['DTPOSTED'], 0, 4) . '-' . substr($moviment['DTPOSTED'], 4, 2) . '-' . substr($moviment['DTPOSTED'], 6, 2) ;
			$transaction->financialNumber = $moviment['FITID'];
			$transactions[] = $transaction;
		}
		return $transactions;
	}		
}
?>