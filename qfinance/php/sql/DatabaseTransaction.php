<?php
/**
 * Database DatabaseTransaction
 *
 * @author: http://phpdao.com
 * @date: 27.11.2007
 */
class DatabaseTransaction{
	private static $transactions;

	private $connection;

	public function DatabaseTransaction(){
		$this->connection = new Connection();
		if(!DatabaseTransaction::$transactions){
			DatabaseTransaction::$transactions = new ArrayList();
		}
		DatabaseTransaction::$transactions->add($this);
		$this->connection->executeQuery('BEGIN');
	}

	/**
	 * Zakonczenie transakcji i zapisanie zmian
	 */
	public function commit(){
		$this->connection->executeQuery('COMMIT');
		$this->connection->close();
		DatabaseTransaction::$transactions->removeLast();
	}

	/**
	 * Zakonczenie transakcji i wycofanie zmian
	 */
	public function rollback(){
		$this->connection->executeQuery('ROLLBACK');
		$this->connection->close();
		DatabaseTransaction::$transactions->removeLast();
	}

	/**
	 * Pobranie polaczenia dla obencej transakcji
	 *
	 * @return polazenie do bazy
	 */
	public function getConnection(){
		return $this->connection;
	}

	/**
	 * Zwraca obecna transakcje
	 *
	 * @return transkacja
	 */
	public static function getCurrentTransaction(){
		if(DatabaseTransaction::$transactions){
			$tran = DatabaseTransaction::$transactions->getLast();
			return $tran;
		}
		return;
	}
}
?>