<?php
class LoadingElement implements Element {		
	private $elements = array();

	public function LoadingElement() {}
	
	public function addElement( $element ) {
		array_push($this->elements, $element);
	}
	
	public function mergeElementArray( $array ) {
		$this->elements = array_merge($this->elements, $array);
	}
	
	public function render() {
		$result = '';
		$result .= $this->renderLoading();
		for ($i = 0; $i < sizeof($this->elements); $i++) {
			$result .= $this->elements[$i]->render();
			$result .= $this->renderProgress($this->elements[$i], sizeof($this->elements), $i);
		}
		return $result;
	}
	
	public function renderLoading() {
		return '<div id="loading" >
		 		<div class="x-shadow" id="ext-loa38" style="width: 258px; height: 82px; display: block; z-index: 9002; left: 41.5%; top: 47%;">
		 			<div class="xst">
		 				<div class="xstl"></div>
		 				<div class="xstc" style="width: 246px;"></div>
		 				<div class="xstr"></div>
		 			</div>
		 			<div class="xsc" style="height: 70px;">
		 				<div class="xsml"></div>
		 				<div class="xsmc" style="width: 246px;"></div>
		 				<div class="xsmr"></div>
		 			</div>
		 			<div class="xsb">
		 				<div class="xsbl"></div>
		 				<div class="xsbc" style="width: 246px;"></div>
		 				<div class="xsbr"></div>
		 			</div>
		 		</div>
		 		<div id="ext-comp-1006" class=" x-window x-window-plain x-window-dlg"	style="position: absolute; width: 250px; display: block; left: 42%; top: 46%; visibility: visible; z-index: 9003;">
		 			<div class="x-window-tl">
		 				<div class="x-window-tr">
		 					<div class="x-window-tc">
		 						<div class="x-window-header x-unselectable x-window-draggable"	id="ext-loa4" style="-webkit-user-select: none;">
		 							<div class="x-tool x-tool-close" id="ext-loa10" style="display: none;">&nbsp;</div>
		 							<span class="x-window-header-text" id="ext-loa16">Please wait</span>
		 						</div>
		 					</div>
		 				</div>
		 			</div>
		 			<div class="x-window-bwrap" id="ext-loa5">
		 				<div class="x-window-ml">
		 					<div class="x-window-mr">
		 						<div class="x-window-mc" id="ext-loa9">
		 							<div class="x-window-body" id="ext-loa6"	style="width: 218px; height: auto;">
		 								<div id="ext-loa26">
		 									<div class="ext-mb-icon x-hidden" id="ext-loa27">
		 									</div>
		 									<div class="ext-mb-content">
		 										<span class="ext-mb-text"	id="ext-loa28"></span><br>
		 										<div class="ext-mb-fix-cursor">
		 											<input type="text"	class="ext-mb-input" id="ext-loa29" style="display: none;"><textarea	class="ext-mb-textarea" id="ext-loa30" style="display: none;"></textarea>
		 										</div>
		 									</div>
		 									<div class="x-progress-wrap" id="ext-comp-1007" style="width: 216px;">
		 										<div class="x-progress-inner">
		 											<div class="x-progress-bar" id="ext-loa32" style="height: 16px; width: 150px;">
		 												<div class="x-progress-text" id="ext-loa33"	style="z-index: 99; width: 150px;">
		 													<div id="ext-loa35" style="width: 216px; height: 18px;">&nbsp;</div>
		 												</div>
		 											</div>
		 											<div class="x-progress-text x-progress-text-back" id="ext-loa34">
		 												<div id="ext-loa36" style="width: 216px; height: 18px;">&nbsp;</div>
		 											</div>
		 										</div>
		 									</div>
		 								</div>
		 							</div>
		 						</div>
		 					</div>
		 				</div>
		 				<div class="x-window-bl" id="ext-loa8">
		 					<div class="x-window-br">
		 						<div class="x-window-bc">
		 							<div class="x-window-footer x-panel-btns" id="ext-loa7"	style="width: 228px;"></div>
		 						</div>
		 					</div>
		 				</div>
		 			</div>
		 		</div>
		 	</div>';			
	}

	/**
	 * Render progress updater to page
	 * @param Element $element Element that is loading
	 * @param integer $total Total elements
	 * @param integer $current Current element
	 */
	public function renderProgress($element, $total, $current) {
		$percentage = round((100 * ($current + 1)) / $total,2);
		$width = round((215 * $percentage) / 100,2);
		$result = '<script type="text/javascript">
		     		document.getElementById("ext-loa28").innerHTML = "' . $element->getName() . '";
		     		document.getElementById("ext-loa35").innerHTML = "' . $percentage . '%";
		     		document.getElementById("ext-loa36").innerHTML = "' . $percentage . '%";
		     		document.getElementById("ext-loa32").style["width"] = "' . $width . 'px";
		     		document.getElementById("ext-loa33").style["width"] = "' . ($width -10 > 0 ? ($width - 10 ) : 0) . 'px";
				</script>'; 
		return $result;
	} 
	
	public function getName() {
		return 'Loading element';
	}
}
?>