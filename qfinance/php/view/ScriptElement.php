<?php
class ScriptElement implements Element {
	private $filePath;
	private $name;
	
	public function ScriptElement($filePath, $name) {
		$this->filePath = $filePath;
		$this->name = $name;
	}
	
	public function render() {
		return '<script type="text/javascript" src="' . $this->filePath . '"></script>';			
	}
	
	public function getName() {
		return $this->name;
	} 
}
?>