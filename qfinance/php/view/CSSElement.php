<?php
class CSSElement implements Element {
	private $filePath;
	private $name;
	
	public function CSSElement($filePath, $name = '') {
		$this->filePath = $filePath;
		$this->name = $name;
	}
	
	public function render() {
		return '<link rel="stylesheet" type="text/css" href="' . $this->filePath . '" />';
	}
	
	public function getName() {
		return $this->name;
	}
}
?>