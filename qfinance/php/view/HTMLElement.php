<?php
class HTMLElement implements Element {
	private $innerHTML;
	public function HTMLElement($innerHTML) {
		$this->innerHTML = $innerHTML;
	} 
	
	public function render() {
		return $this->innerHTML;
	}
}
?>