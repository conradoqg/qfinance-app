<?php
interface Element {
	public function render();
	public function getName();
}
?>