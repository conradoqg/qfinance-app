<?php
class LibraryInclude {

	public static function getSenchaTouch() {
				
		if (DeveloperService::$debugJavaScript) {
			$elements = array(
				new CSSElement("external/lib/Sencha-touch-1.0.1a/css/sencha-touch.css"),
				new ScriptElement("external/lib/Sencha-touch-1.0.1a/js/sencha-touch-debug-w-comments.js", "Sencha touch"),
				new ScriptElement("lib/Ext/js/ext-direct.js", "Integration")			
			);
		} else {
			$elements = array(
				new CSSElement("external/lib/Sencha-touch-1.0.1a/css/sencha-touch.css"),
				new ScriptElement("external/lib/Sencha-touch-1.0.1a/js/sencha-touch.js", "Sencha touch"),
				new ScriptElement("lib/Ext/js/ext-direct.js", "Integration")
			);
		}
		return $elements;
	}
	
	public static function getQFinanceMobile() {
		$elements = array(
			new ScriptElement("js/qfinance.js", "QFinance application"),
			new ScriptElement("index.php?configureRemoting", "QFinance application"),
			new ScriptElement("js/AppMobile.js", "QFinance application"),
			new ScriptElement("js/qfinance-lang-pt_BR.js", "QFinance application")			
		);
		
		return $elements;
	}
	
	public static function getExtJs() {
		if (DeveloperService::$debugJavaScript) {
			$elements = array(
				new CSSElement("external/lib/Ext-3.3.1/css/ext-all.css", "ExtJS style"),
				new ScriptElement("external/lib/Ext-3.3.1/js/ext-base-debug.js", "ExtJS base"),
				new ScriptElement("external/lib/Ext-3.3.1/js/ext-all-debug-w-comments.js", "ExtJS interface"),
				new ScriptElement("external/lib/Ext-3.3.1/js/ext-lang-pt_BR.js", "ExtJS interface"),											
			);
		} else {
			$elements = array(
				new CSSElement("external/lib/Ext-3.3.1/css/ext-all.css", "ExtJS style"),
				new ScriptElement("external/lib/Ext-3.3.1/js/ext-base.js", "ExtJS base"),
				new ScriptElement("external/lib/Ext-3.3.1/js/ext-all.js", "ExtJS interface"),
			);				
		}
		
		return $elements;
	}
	
	public static function getJQuery() {
		$elements = array(
			new ScriptElement("http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js", "JQuery")
		);
		
		return $elements;
	}
	
	public static function getHighChart() {
		$elements = array(
			new ScriptElement("external/lib/Highcharts-2.1.4/js/highcharts.js", "High Chart")
		);
		
		return $elements;
	}
	
	public static function getExtJsUx() {
		$elements = array(				
			new ScriptElement("lib/Ext/js/Desktop.js", "ExtJS extensions"),
			new CSSElement("lib/Ext/css/StartMenu.css", "ExtJS extensions"),
			new ScriptElement("lib/Ext/js/StartMenu.js", "ExtJS extensions"),
			new CSSElement("lib/Ext/css/TaskBar.css", "ExtJS extensions"),
			new ScriptElement("lib/Ext/js/TaskBar.js", "ExtJS extensions"),
			new ScriptElement("lib/Ext/js/TaskBarContainer.js", "ExtJS extensions"),
			new CSSElement("lib/Ext/css/TaskButton.css", "ExtJS extensions"),
			new ScriptElement("lib/Ext/js/TaskButton.js", "ExtJS extensions"),
			new ScriptElement("lib/Ext/js/TaskButtonsPanel.js", "ExtJS extensions"),				
			new ScriptElement("lib/Ext/js/WindowSnap.js", "ExtJS extensions"),
			new CSSElement("lib/Ext/css/grid/gridsummary.css", "ExtJS extensions"),
			new ScriptElement("lib/Ext/js/grid/gridsummary.js", "ExtJS extensions"),
			new ScriptElement("lib/Ext/js/adapter-extjs.js", "ExtJS extensions"),			
			new ScriptElement("lib/Ext/js/Ext.ux.HighChart.js", "ExtJS extensions")
		);
				
		if (DeveloperService::$debugJavaScript) {
			array_push($elements, new ScriptElement("external/lib/Ext-3.3.1/js/ux-all-debug.js", "ExtJS extensions"));
			array_push($elements, new CSSElement("external/lib/Ext-3.3.1/css/ux-all.css", "ExtJS extensions"));
		} else {
			array_push($elements, new ScriptElement("external/lib/Ext-3.3.1/js/ux-all.js", "ExtJS extensions"));			
			array_push($elements, new CSSElement("external/lib/Ext-3.3.1/css/ux-all.css", "ExtJS extensions"));
		}
			
		return $elements;		
	}
	
	public static function getQFinance() {
		$elements = array(
			new ScriptElement("index.php?configureRemoting", "QFinance application")				
		);
		
		if (DeveloperService::$debugJavaScript) {
			$fileList = @file_get_contents('cache/qfinancefrontendfilelist.cache');
			if (empty($fileList))
				throw new Exception('Cannot load front-end file list cache.');
			$fileList = json_decode($fileList);
			
			foreach ($fileList as $file) {
				$fileInfo = pathinfo($file);
				
				if ($fileInfo['extension'] == 'css')
					array_push($elements, new CSSElement($file, "QFinance application"));
				else if ($fileInfo['extension'] == 'js')
					array_push($elements, new ScriptElement($file, "QFinance application"));
			}
			array_push($elements, new ScriptElement("js/qfinance-lang-pt_BR.js", "QFinance application"));
		} else {
			array_push($elements, new CSSElement("css/App.css", "QFinance application"));
			array_push($elements, new ScriptElement("js/qfinance-all.js", "QFinance application"));
			array_push($elements, new ScriptElement("js/qfinance-lang-pt_BR.js", "QFinance application"));
		}				

		return $elements;
	}
}
?>