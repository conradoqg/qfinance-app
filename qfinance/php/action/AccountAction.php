<?php

/**
 * @package QFinance\action
 */
class AccountAction {

    public static $allText = 'All';

    /**
     * Get all accounts less the informed one
     * @remotable 
     * @param integer $excludeAccount Account to exclude 
     * @return ActionResult Action result, where return->data as array of Accounts
     */
    public function getAccountsMinus($excludeAccount) {
        $accounts = null;

        $accountDAO = DAOFactory::getAccountDAO();
        if ($excludeAccount == null) {
            $accounts = $accountDAO->queryAll();
        } else {
            $accounts = $accountDAO->queryAllMinus($excludeAccount);
        }

        $result = new ActionResult();
        $result->data = $accounts;

        return $result;
    }

    /**
     * Get all accounts of one type
     * @remotable
     * @param integer $type Account Type
     * @return ActionResult Action result, where return->data as array of Accounts
     */
    public function getByAccountType($type) {
        $accountDAO = DAOFactory::getAccountDAO();

        $result = new ActionResult();
        $result->data = $accountDAO->queryByAccountType($type);

        return $result;
    }

    /**
     * Get all accounts of one type plus an 'All' account
     * @remotable 
     * @param integer $type Account Type
     * @return ActionResult Action result, where return->data as array of Accounts
     */
    public function getByAccountTypePlusAll($type) {
        $allAccount = new Account();
        $allAccount->accountType = 1;
        $allAccount->accountName = self::$allText;
        $accountDAO = DAOFactory::getAccountDAO();
        $accounts = $accountDAO->queryByAccountType($type);
        array_unshift($accounts, $allAccount);

        $result = new ActionResult();
        $result->data = $accounts;

        return $result;
    }

}

?>