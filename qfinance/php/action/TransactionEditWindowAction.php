<?php

class TransactionEditWindowAction {

    /**
     * @remotable
     */
    public function getMarks() {
        $markDAO = DAOFactory::getMarkDAO();

        $marks = $markDAO->queryAll();

        $result = new ActionResult();
        $result->data = $marks;

        return $result;
    }

}

?>