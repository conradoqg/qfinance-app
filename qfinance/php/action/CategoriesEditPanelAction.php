<?php

/**
 * @package QFinance\action
 */
class CategoriesEditPanelAction {

    /**
     * Get categories for tree population
     * @remotable
     * @param string $nodeID Node ID
     * @return array Nodes
     */
    public function getCategoryTree($nodeID) {
        $categoryDAO = DAOFactory::getCategoryDAO();
        $returnNodes = array();

        if ($nodeID == 'root') {
            $selectedCategories = $categoryDAO->queryRootCategories();
        } else {
            $selectedCategories = $categoryDAO->queryByParentCategoryID($nodeID);
        }

        foreach ($selectedCategories as $category) {
            $leafs = $categoryDAO->queryByParentCategoryID($category->id);

            array_push($returnNodes, array(
                'id' => $category->id,
                'text' => $category->categoryName,
                'leaf' => false,
                'expanded' => !$leafs,
                'loaded' => !$leafs
            ));
        }

        return $returnNodes;
    }

    /**
     * Unlink parent category from category
     * @remotable
     * @param int $id Parent category id
     * @return ActionResult Action result
     */
    public function clearParentCategory($id) {
        $categoryDAO = DAOFactory::getCategoryDAO();

        $category = $categoryDAO->load($id);

        if ($category != null) {
            $category->parentCategoryID = null;
            $categoryDAO->update($category);
        }

        return new ActionResult();
    }

    /**
     * Set parent category in informed category
     * @remotable
     * @param int $id Category id
     * @param int $parentID Parent category to link
     * @return ActionResult Action result
     */
    public function setParentCategory($id, $parentID) {
        $categoryDAO = DAOFactory::getCategoryDAO();

        $category = $categoryDAO->load($id);

        if ($category != null) {
            $category->parentCategoryID = $parentID;
            $categoryDAO->update($category);
        }

        return new ActionResult();
    }

    /**
     * Create an category
     * @remotable
     * @param object $createInfo CategoriesEdiPanel structure of category to create
     * @return ActionResult Action result, where result->id as created category id
     */
    public function create($createInfo) {
        $categoryDAO = DAOFactory::getCategoryDAO();

        $category = new Category();
        $category->categoryName = $createInfo->data->categoryName;
        $category->parentCategoryID = $createInfo->data->parentCategoryID;
        $categoryDAO->insert($category);

        $result = new ActionResult();
        $result->id = $category->id;

        return $result;
    }

    /**
     * Update an category
     * @remotable
     * @param object $updateInfo CategoriesEdiPanel structure of category to update
     * @return ActionResult Action result
     */
    public function update($updateInfo) {
        $categoryDAO = DAOFactory::getCategoryDAO();

        $category = new Category();
        $category->id = $updateInfo->data->id;
        $category->categoryName = $updateInfo->data->categoryName;
        $category->parentCategoryID = $updateInfo->data->parentCategoryID;
        $categoryDAO->update($category);

        return new ActionResult();
    }

    /**
     * Destroy an category
     * @remotable
     * @param object $deleteInfo CategoriesEdiPanel structure of category to destroy
     * @return ActionResult Action result
     */
    function destroy($deleteInfo) {
        $categoryDAO = DAOFactory::getCategoryDAO();
        $categoryDAO->delete($deleteInfo->data->id);

        return new ActionResult();
    }

}

?>