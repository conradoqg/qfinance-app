<?php

/**
 * @package QFinance\action
 */

/**
 * Class to perform CRUD operations by TransactionsEditPanel front-end
 */
class TransactionsEditPanelAction {

    /**
     * Read transactions, if first parameter is specified, will filter by account id, if second parameter is specified, will filter by temporary id, if third parameter is specified, will filter by parent transaction id
     * @remotable
     * @param int $accountID Account id
     * @param int $temporaryID Temporary id
     * @param int $parentTransactionID Parent transaction id
     * @return ActionResult Action result, where result->data as array of Transactions
     */
    function read($accountID, $temporaryID, $parentTransactionID, $annotation) {
        $transactions = null;
        $transactionDAO = DAOFactory::getTransactionDAO();

        $options = new TransactionExtDAOOptions();
        $options->accountID = $accountID;
        $options->temporaryID = $temporaryID;
        $options->parentTransactionID = $parentTransactionID;
        $options->annotation = $annotation;
        $transactions = $transactionDAO->queryTransactionsViewThroughOptions($options);

        /*
          if ($temporaryID != null) {
          if ($parentTransactionID != null)
          $transactions = $transactionDAO->queryTransactionsViewWhereAccountIDAndTemporaryIDAndParentTransactionID( $accountID, $temporaryID, $parentTransactionID, $onlyAnnotation );
          else
          $transactions = $transactionDAO->queryTransactionsViewWhereAccountIDAndTemporaryID( $accountID, $temporaryID, $onlyAnnotation );
          } else {
          if ($parentTransactionID != null)
          $transactions = $transactionDAO->queryTransactionsViewWhereAccountIDAndNotTemporaryAndParentTransactionID( $accountID, $parentTransactionID, $onlyAnnotation );
          else
          $transactions = $transactionDAO->queryTransactionsViewWhereAccountIDAndNotTemporary( $accountID, $onlyAnnotation );
          } */

        $result = new ActionResult();
        $result->data = $transactions;

        return $result;
    }

    /**
     * Create an transaction
     * @remotable
     * @param object $createInfo ExtJS structure of transactions to create
     * @return ActionResult Action result, where result->data as array of created transactions
     */
    function create($createInfo) {
        if (!is_array($createInfo->data)) {
            $records = array($createInfo->data);
        } else {
            $records = $createInfo->data;
        }

        $transactionDAO = DAOFactory::getTransactionDAO();
        $addedRecords = array();

        foreach ($records as $record) {
            $transaction = new Transaction();
            foreach (get_object_vars($record) as $prop => $value)
                $transaction->$prop = $value;
            $transactionDAO->insert($transaction);
            array_push($addedRecords, $transaction);
        }

        $result = new ActionResult();

        if (sizeof($addedRecords) == 1)
            $result->data = $addedRecords[0];
        else
            $result->data = $addedRecords;

        return $result;
    }

    /**
     * Update an transaction
     * @remotable
     * @param object $updateInfo ExtJS structure of transactions to update
     * @return ActionResult Action result, where result->data as array of updated transactions 
     */
    function update($updateInfo) {
        if (!is_array($updateInfo->data)) {
            $records = array($updateInfo->data);
        } else {
            $records = $updateInfo->data;
        }

        $transactionDAO = DAOFactory::getTransactionDAO();
        $updatedRecords = array();

        foreach ($records as $record) {
            $transaction = new Transaction();
            foreach (get_object_vars($record) as $prop => $value)
                $transaction->$prop = $value;
            $transactionDAO->update($transaction);
            array_push($updatedRecords, $transaction);
        }

        $result = new ActionResult();

        if (sizeof($updatedRecords) == 1)
            $result->data = $updatedRecords[0];
        else
            $result->data = $updatedRecords;

        return $result;
    }

    /**
     * Destroy an transactions
     * @remotable
     * @param object $destroyInfo ExtJS structure of transactions to destroy
     * @return ActionResult Action result
     */
    function destroy($destroyInfo) {
        if (!is_array($destroyInfo->data))
            $records = array($destroyInfo->data);
        else
            $records = $destroyInfo->data;

        foreach ($records as $record)
            $this->recursiveTransactionDestroy($record);

        $result = new ActionResult();

        if (sizeof($records) == 1)
            $result->data = $records[0];
        else
            $result->data = $records;

        return $result;
    }

    /**
     * Destroy transaction recursively
     * @param int $transactionID Transaction ID
     */
    function recursiveTransactionDestroy($transactionID) {
        $transactionDAO = DAOFactory::getTransactionDAO();
        $transactionDAO->delete($transactionID);

        $childTransactions = $transactionDAO->queryByParentTransactionID($transactionID);

        foreach ($childTransactions as $transaction)
            $this->recursiveTransactionDestroy($transaction->id);
    }

    /**
     * Get account balance
     * @remotable 
     * @param int $accountID
     * @return ActionResult Action result, where result->accountBalance as account balance
     */
    function getAccountBalance($accountID) {
        $transactionDAO = DAOFactory::getTransactionDAO();
        $transactions = $transactionDAO->queryTransactionsViewWhereAccountIDAndNotTemporary($accountID);

        $accountDAO = DAOFactory::getAccountDAO();
        $account = $accountDAO->load($accountID);
        $accountBalance = $account->initialAmount;

        foreach ($transactions as $transaction) {
            if ($accountID == $transaction->sourceAccountID)
                $accountBalance -= $transaction->amount;
            else if ($accountID == $transaction->destinationAccountID)
                $accountBalance += $transaction->amount;
        }

        $result = new ActionResult();
        $result->accountBalance = $accountBalance;

        return $result;
    }

    /**
     * Get if the transaction has subtransactions
     * @param int $transactionID Transaction ID
     */
    function hasSubtransactions($transactionID) {
        $transactionDAO = DAOFactory::getTransactionDAO();


        return array('success' => true,
            'hasSubtransactions' => $transactionDAO->hasSubtransactions($transactionID));
    }

}

?>