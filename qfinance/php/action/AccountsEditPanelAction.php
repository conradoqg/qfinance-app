<?php

/**
 * @package QFinance\action
 */

/**
 * Class to perform CRUD operations by TransactionsEditPanel front-end
 */
class AccountsEditPanelAction {

    /**
     * Read accounts
     * @remotable
     * @return ActionResult Action result, where result->data as array of Accounts
     */
    function read() {
        $accounts = null;
        $accountDAO = DAOFactory::getAccountDAO();

        $accounts = $accountDAO->queryAllView();

        $result = new ActionResult();
        $result->data = $accounts;

        return $result;
    }

    /**
     * Create an account
     * @remotable
     * @param object $createInfo ExtJS structure of accounts to create
     * @return ActionResult Action result, where result->data as array of created accounts
     */
    function create($createInfo) {
        if (!is_array($createInfo->data)) {
            $records = array($createInfo->data);
        } else {
            $records = $createInfo->data;
        }

        $accountDAO = DAOFactory::getAccountDAO();
        $addedRecords = array();

        foreach ($records as $record) {
            $account = new Account();
            foreach (get_object_vars($record) as $prop => $value)
                $account->$prop = $value;
            $accountDAO->insert($account);
            array_push($addedRecords, $account);
        }

        $result = new ActionResult();

        if (sizeof($addedRecords) == 1)
            $result->data = $addedRecords[0];
        else
            $result->data = $addedRecords;

        return $result;
    }

    /**
     * Update an account
     * @remotable
     * @param object $updateInfo ExtJS structure of accounts to update
     * @return ActionResult Action result, where result->data as array of updated accounts 
     */
    function update($updateInfo) {
        if (!is_array($updateInfo->data)) {
            $records = array($updateInfo->data);
        } else {
            $records = $updateInfo->data;
        }

        $accountDAO = DAOFactory::getAccountDAO();
        $updatedRecords = array();

        foreach ($records as $record) {
            $account = new Account();
            foreach (get_object_vars($record) as $prop => $value)
                $account->$prop = $value;
            $accountDAO->update($account);
            array_push($updatedRecords, $account);
        }

        $result = new ActionResult();

        if (sizeof($updatedRecords) == 1)
            $result->data = $updatedRecords[0];
        else
            $result->data = $updatedRecords;

        return $result;
    }

    /**
     * Destroy an account
     * @remotable
     * @param object $destroyInfo ExtJS structure of accounts to destroy
     * @return ActionResult Action result
     */
    function destroy($destroyInfo) {
        if (!is_array($destroyInfo->data)) {
            $records = array($destroyInfo->data);
        } else {
            $records = $destroyInfo->data;
        }

        $accountDAO = DAOFactory::getAccountDAO();

        foreach ($records as $record)
            $accountDAO->delete($record);

        $result = new ActionResult();

        if (sizeof($records) == 1)
            $result->data = $records[0];
        else
            $result->data = $records;

        return $result;
    }

}

?>