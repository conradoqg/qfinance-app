<?php

/**
 * @package QFinance\action
 */
class AccountOverviewPanelAction {

    /**
     * Enter description here ...
     * @param unknown_type $accountID
     * @remotable
     */
    public function getAccountOverview($accountID) {

        $transactionDAO = DAOFactory::getTransactionDAO();
        $transactions = $transactionDAO->queryTransactionsViewWhereAccountIDAndNotTemporary($accountID);

        $accountDAO = DAOFactory::getAccountDAO();
        $account = $accountDAO->load($accountID);
        $accountBalance = $account->initialAmount;

        foreach ($transactions as $transaction) {
            if ($accountID == $transaction->sourceAccountID)
                $accountBalance -= $transaction->amount;
            else if ($accountID == $transaction->destinationAccountID)
                $accountBalance += $transaction->amount;
        }


        return array("success" => true,
            "data" => array("accountBalance" => $accountBalance));
    }

}

?>