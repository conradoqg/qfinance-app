<?php

/**
 * @package QFinance\action
 */
class DeveloperServiceAction {

    /**
     * Get developer service configuration
     * @remotable
     * @return array Array of configurations
     */
    public function getConfiguration() {

        $reflection = new ReflectionClass("DeveloperService");

        $return = new ActionResult();
        $return->configuration = $reflection->getStaticProperties();
        return $return;
    }

    /**
     * Get logged messages
     * @remotable
     * @return array Logged messages
     */
    public function getMessages() {
        $messages = DeveloperService::getMessages();
        return $messages;
    }

}

?>