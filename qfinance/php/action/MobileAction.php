<?php

class MobileAction {

    public static $noneText = 'None';

    /**
     * Enter description here ...
     * @remotable
     */
    public function getAccountsByTypePlusNone($type) {
        $noneAccount = new Account();
        $noneAccount->accountType = 1;
        $noneAccount->accountName = self::$noneText;

        $accountDAO = DAOFactory::getAccountDAO();
        $accounts = $accountDAO->queryByAccountType($type);

        array_unshift($accounts, $noneAccount);

        $result = new ActionResult();
        $result->data = $accounts;

        return $result;
    }

    /**
     * Enter description here ...
     * @remotable
     */
    public function getAccountsPlusNone() {
        $noneAccount = new Account();
        $noneAccount->accountType = 1;
        $noneAccount->accountName = self::$noneText;

        $accountDAO = DAOFactory::getAccountDAO();
        $accounts = $accountDAO->queryAll();

        array_unshift($accounts, $noneAccount);

        $result = new ActionResult();
        $result->data = $accounts;

        return $result;
    }

    /**
     * Enter description here ...
     * @remotable
     */
    public function getCategoriesPlusNone() {
        $noneCategory = new Category();
        $noneCategory->categoryName = self::$noneText;

        $categoryDAO = DAOFactory::getCategoryDAO();
        $categories = $categoryDAO->queryAll();

        array_unshift($categories, $noneCategory);

        $result = new ActionResult();
        $result->data = $categories;

        return $result;
    }

    /**
     * @remotable
     * @param unknown_type $record
     */
    public function addTransactionAnnotation($record) {
        $transactionDAO = DAOFactory::getTransactionDAO();

        $transaction = new Transaction();

        foreach (get_object_vars($record) as $prop => $value)
            $transaction->$prop = $value;

        $transaction->annotation = true;

        $transactionDAO->insert($transaction);

        $result = new ActionResult();
        return $result;
    }

}

?>