<?php

/**
 * @package QFinance\action
 */

/**
 * Login action provides services to manage session authentication and verification.
 */
class LoginAction {

    /**
     * Logs on application
     * @remotable
     * @formHandler
     * @param string $username Username
     * @param string $password Password
     * @return ActionResult Action result, where result->logged as login result.
     */
    public function login($username, $password) {
        $result = new ActionResult();

        $securityService = SecurityService::getInstance();
        $result->logged = $securityService->login($username, $password);

        return $result;
    }

    /**
     * Logs out of application
     * @remotable
     * @return ActionResult Action result
     */
    public function logout() {
        $securityService = SecurityService::getInstance();
        $securityService->logout();
        return new ActionResult();
    }

}

?>