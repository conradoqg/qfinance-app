<?php

class TestAction {

    /**
     * @remotable
     */
    public function echoo($speech) {
        $result = new ActionResult();
        $result->echo = $speech;
        return $result;
    }

}

?>