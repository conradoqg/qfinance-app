<?php

/**
 * @package QFinance\action
 */
class Action {

    public $actionName;
    public $requireAuthorization;

    public function Action($actionName, $requireAuthorization) {
        $this->actionName = $actionName;
        $this->requireAuthorization = $requireAuthorization;
    }

}

?>