<?php

/**
 * @package QFinance\action
 */
class ExpensesAndIncomesByCategoryPanelAction {

    public static $othersText = 'Others';

    /**
     * Get expenses and incomes by category
     * @remotable
     * @param int $options Range options
     * @param int $accountID Contextual account id
     * @param int $parentCategoryID Category to filter
     * @return ActionResult Action result, where result->data as array of hash data, where data contais category name and value
     */
    public function getExpensesAndIncomesByCategory($options, $accountID, $parentCategoryID) {

        $transactionDAO = DAOFactory::getTransactionDAO();
        $accountDAO = DAOFactory::getAccountDAO();

        $queryOptions = new TransactionExtDAOOptions();
        $queryOptions->accountID = $accountID;
        $queryOptions->parentCategoryID = $parentCategoryID;
        $queryOptions->parentTransactionID = 0;
        $queryOptions->notPrivateToPrivate = true;
        if ($options->type == 2)
            $queryOptions->onlyDestinationAccountID = true;
        else if ($options->type == 3)
            $queryOptions->onlySourceAccountID = true;

        switch ($options->range) {
            case 2:
                $now = new DateTime();
                $lastMonth = new DateTime();
                $lastMonth->modify("-30 day");
                $queryOptions->startDate = $lastMonth->format("Y-m-d");
                $queryOptions->endDate = $now->format("Y-m-d");
                break;
            case 3:
                $now = new DateTime();
                $lastMonth = new DateTime();
                $lastMonth->modify("-60 day");
                $queryOptions->startDate = $lastMonth->format("Y-m-d");
                $queryOptions->endDate = $now->format("Y-m-d");
                break;
            case 4:
                $startDayMonth = new DateTime();
                $endDayMonth = new DateTime();
                $startDayMonth->modify("first day");
                $endDayMonth->modify("last day");
                $queryOptions->startDate = $startDayMonth->format("Y-m-d");
                $queryOptions->endDate = $endDayMonth->format("Y-m-d");
                break;
            case 5:
                $startDayYear = new DateTime();
                $endDayYear = new DateTime();
                $startDayYear->modify("first day of this year");
                $endDayYear->modify("first day of next year");
                $endDayYear->modify("-1 day");
                $queryOptions->startDate = $startDayYear->format("Y-m-d");
                $queryOptions->endDate = $endDayYear->format("Y-m-d");
                break;
            case 6:
                $queryOptions->startDate = $options->startDate;
                $queryOptions->endDate = $options->endDate;
                break;
        }

        $transactions = $transactionDAO->queryTransactionsViewThroughOptions($queryOptions);

        $valueByCategory = array();

        foreach ($transactions as $transaction) {
            if (!$transaction->hasSubtransactions) {
                if ($transaction->categoryID == null) {
                    $categoryID = 0;
                    $categoryName = self::$othersText;
                } else {
                    $categoryID = $transaction->categoryID;
                    if ($transaction->categoryName == null)
                        $categoryName = self::$othersText;
                    else
                        $categoryName = $transaction->categoryName;
                }

                if (!isset($valueByCategory[$categoryID]))
                    $valueByCategory[$categoryID] = (object) array('categoryName' => $categoryName, 'value' => 0);

                $valueByCategory[$categoryID]->value += $transaction->amount;
            }
        }

        $data = array();
        foreach ($valueByCategory as $key => $value) {
            $data[] = array("categoryName" => $value->categoryName,
                "value" => $value->value);
        }

        $result = new ActionResult();
        $result->data = $data;

        return $result;
    }

}

?>