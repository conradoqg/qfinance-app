<?php

/**
 * @package QFinance\action
 */

/**
 * Class to perform CRUD operations by TransactionsEditPanel front-end
 */
class UsersEditPanelAction {

    /**
     * Read users
     * @remotable
     * @return ActionResult Action result, where result->data as array of Users
     */
    function read() {
        $users = null;
        $userDAO = DAOFactory::getUserDAO();

        $users = $userDAO->queryAll();

        $result = new ActionResult();
        $result->data = $users;

        return $result;
    }

    /**
     * Create an user
     * @remotable
     * @param object $createInfo ExtJS structure of users to create
     * @return ActionResult Action result, where result->data as array of created users
     */
    function create($createInfo) {
        if (!is_array($createInfo->data)) {
            $records = array($createInfo->data);
        } else {
            $records = $createInfo->data;
        }

        $userDAO = DAOFactory::getUserDAO();
        $addedRecords = array();

        foreach ($records as $record) {
            $user = new User();
            $user = clone $record;
            $user->password = md5($user->newPassword);
            $userDAO->insert($user);
            array_push($addedRecords, $user);
        }

        $result = new ActionResult();

        if (sizeof($addedRecords) == 1)
            $result->data = $addedRecords[0];
        else
            $result->data = $addedRecords;

        return $result;
    }

    /**
     * Update an user
     * @remotable
     * @param object $updateInfo ExtJS structure of users to update
     * @return ActionResult Action result, where result->data as array of updated users 
     */
    function update($updateInfo) {
        if (!is_array($updateInfo->data)) {
            $records = array($updateInfo->data);
        } else {
            $records = $updateInfo->data;
        }

        $userDAO = DAOFactory::getUserDAO();
        $updatedRecords = array();

        foreach ($records as $record) {
            $user = new User();
            $user = clone $record;
            $user->password = md5($user->newPassword);
            $userDAO->update($user);
            array_push($updatedRecords, $user);
        }

        $result = new ActionResult();

        if (sizeof($updatedRecords) == 1)
            $result->data = $updatedRecords[0];
        else
            $result->data = $updatedRecords;

        return $result;
    }

    /**
     * Destroy an user
     * @remotable
     * @param object $destroyInfo ExtJS structure of users to destroy
     * @return ActionResult Action result
     */
    function destroy($destroyInfo) {
        if (!is_array($destroyInfo->data)) {
            $records = array($destroyInfo->data);
        } else {
            $records = $destroyInfo->data;
        }

        $userDAO = DAOFactory::getUserDAO();

        foreach ($records as $record)
            $userDAO->delete($record);

        $result = new ActionResult();

        if (sizeof($records) == 1)
            $result->data = $records[0];
        else
            $result->data = $records;

        return $result;
    }

}

?>