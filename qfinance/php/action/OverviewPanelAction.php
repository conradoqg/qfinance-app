<?php

class OverviewPanelAction {

    /**
     * 
     * Enter description here ...
     * @remotable
     */
    public function getPortlets() {
        $portletDAO = DAOFactory::getPortletDAO();

        $portlets = $portletDAO->queryAll();

        // Transform configuration data
        foreach ($portlets as $portlet) {
            $portlet->configuration = json_decode($portlet->configuration);
        }

        $result = new ActionResult();
        $result->data = $portlets;

        return $result;
    }

    /**
     * 
     * Enter description here ...
     * @remotable
     */
    public function savePortlets($portlets, $portletsToRemove) {
        $portletDAO = DAOFactory::getPortletDAO();

        foreach ($portlets as $portletItem) {
            $portlet = new Portlet();
            $portlet->id = $portletItem->internalID;
            $portlet->className = $portletItem->className;
            if (isset($portletItem->configuration))
                $portlet->configuration = json_encode($portletItem->configuration);
            else
                $portlet->configuration = null;
            $portlet->lin = $portletItem->lin;
            $portlet->col = $portletItem->col;

            if ($portlet->id == null)
                $portletDAO->insert($portlet);
            else
                $portletDAO->update($portlet);
        }

        foreach ($portletsToRemove as $portletItem) {
            $portletDAO->delete($portletItem);
        }

        $result = new ActionResult();
        return $result;
    }

}

?>