<?php

class TopAmountPortletAction {

    static function order($a, $b) {
        if ($a['amount'] == $b['amount']) {
            return 0;
        }
        return ($a['amount'] < $b['amount']) ? 1 : -1;
    }

    /**
     * 
     * @remotable
     * @return ActionResult Action result, where result->data as array of Transactions
     */
    function read($topType) {

        switch ($topType) {
            case 1:
                $categoryDAO = DAOFactory::getCategoryDAO();
                $transactionDAO = DAOFactory::getTransactionDAO();

                $categories = $categoryDAO->queryAll();

                foreach ($categories as $category) {
                    $options = new TransactionExtDAOOptions();
                    $options->categoryID = $category->id;
                    $options->parentTransactionID = 0;
                    $transactions = $transactionDAO->queryTransactionsViewThroughOptions($options);

                    $resultData[$category->id] = array('topName' => $category->categoryName, 'amount' => 0);
                    foreach ($transactions as $transaction) {
                        $resultData[$category->id]['amount'] += $transaction->amount;
                    }
                }
                break;
            case 2:
                $accountDAO = DAOFactory::getAccountDAO();
                $transactionDAO = DAOFactory::getTransactionDAO();

                $accounts = $accountDAO->queryAll();

                foreach ($accounts as $account) {
                    $options = new TransactionExtDAOOptions();
                    $options->accountID = $account->id;
                    $options->parentTransactionID = 0;
                    $transactions = $transactionDAO->queryTransactionsViewThroughOptions($options);

                    $resultData[$account->id] = array('topName' => $account->accountName, 'amount' => 0);
                    foreach ($transactions as $transaction) {
                        $resultData[$account->id]['amount'] += $transaction->amount;
                    }
                }
                break;
            case 3:
                $transactionDAO = DAOFactory::getTransactionDAO();

                $options = new TransactionExtDAOOptions();
                $options->parentTransactionID = 0;
                $transactions = $transactionDAO->queryTransactionsViewThroughOptions($options);

                foreach ($transactions as $transaction) {
                    $resultData[] = array('topName' => (empty($transaction->description) ? $transaction->bankMemo : $transaction->description), 'amount' => $transaction->amount);
                }
                break;
            case 4:
                $markDAO = DAOFactory::getMarkDAO();
                $transactionDAO = DAOFactory::getTransactionDAO();
                
                $marks = $markDAO->queryAll();
                
                foreach ($marks as $mark) {
                    $options = new TransactionExtDAOOptions();
                    $options->markID = $mark->id;
                    $options->parentTransactionID = 0;
                    $transactions = $transactionDAO->queryTransactionsViewThroughOptions($options);
                    
                    $resultData[$mark->id] = array('topName' => $mark->name, 'amount' => 0);
                    foreach ($transactions as $transaction) {
                        $resultData[$mark->id]['amount'] += $transaction->amount;
                    }
                }
                break;
        }

        // Order by amount
        uasort($resultData, array('TopAmountPortletAction', 'order'));

        // Only TOP 10
        array_splice($resultData, 10);

        $result = new ActionResult();
        $result->data = array_values($resultData);

        return $result;
    }

}

?>