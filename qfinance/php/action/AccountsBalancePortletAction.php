<?php

class AccountsBalancePortletAction {

    /**
     * Read transactions, if first parameter is specified, will filter by account id, if second parameter is specified, will filter by temporary id, if third parameter is specified, will filter by parent transaction id
     * @remotable
     * @return ActionResult Action result, where result->data as array of Transactions
     */
    function read() {

        $accountDAO = DAOFactory::getAccountDAO();
        $accounts = $accountDAO->queryByAccountType('1');

        $transactionDAO = DAOFactory::getTransactionDAO();

        $resultData = array();
        foreach ($accounts as $account) {
            $options = new TransactionExtDAOOptions();
            $options->accountID = $account->id;
            $transactions = $transactionDAO->queryTransactionsViewThroughOptions($options);
            $resultHashData[$account->id] = array('accountName' => $account->accountName, 'amount' => $account->initialAmount);
            foreach ($transactions as $transaction) {
                if ($transaction->sourceAccountID == $account->id)
                    $resultHashData[$account->id]['amount'] -= $transaction->amount;
                else if ($transaction->destinationAccountID == $account->id)
                    $resultHashData[$account->id]['amount'] += $transaction->amount;
            }
        }

        // Transform to normal array		
        $resultData = array();
        foreach ($resultHashData as $key => $value)
            $resultData[] = $value;

        $result = new ActionResult();
        $result->data = $resultData;

        return $result;
    }

}

?>