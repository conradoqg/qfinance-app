<?php
/**
 * core_Autoloader provides automatic class load, registering it self in spl_autoload functions
 */
Class AutoLoaderService {
	
	/**
	 * core_AutoLoader instance
	 * @var unknown_type
	 */
	private static $instance;
	
	private $fileList;
	
	public static function init() {
		if (self::$instance == NULL)
			self::$instance = new self();
		
		return self::$instance;
	}
	
	private function __construct() {
		spl_autoload_register(array($this, "actions"));
		
		$fileList = @file_get_contents('cache/qfinancebackendfilelist.cache');
		if (empty($fileList))
			throw new Exception('Cannot load back-end file list cache.');
		$this->fileList = json_decode($fileList);
	}
	
	public function actions( $className ) {
		require_once $this->fileList->$className;
	}
}
?>