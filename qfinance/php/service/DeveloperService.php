<?php
/**
 * @package QFinance.service
 */
class DeveloperService {

    public static $remotingDeny = false;
    public static $remotingRandomDeny = false;
    public static $remotingDelay = true;
    public static $bypassLogin = true;
    public static $debugJavaScript = true;
    public static $logMessages = true;

    public static function log($message) {
        if (self::$logMessages) {
            if (!isset($_SESSION['logQueue']))
                $_SESSION['logQueue'] = array();

            array_push($_SESSION['logQueue'], $message);
        }
    }

    public static function getMessages() {
        $messages = array();
        if (isset($_SESSION['logQueue'])) {
            for ($i = 0; $i < sizeof($_SESSION['logQueue']); $i++) {
                $messages[] = array_shift($_SESSION['logQueue']);
            }
        }

        return $messages;
    }

    public static function clearMessages() {
        if (isset($_SESSION['logQueue']))
            unset($_SESSION['logQueue']);
    }

}
?>