<?php
class LocalizationService {
	public static function Localize() {
		AccountAction::$allText = 'Tudo';
		ExpensesAndIncomesByCategoryPanelAction::$othersText = 'Outros';
		MobileAction::$noneText = 'Nenhum';
		TransactionsImportPanelAction::$uploadNotRecievedText = 'Upload não recebido';
		AccountMySqlExtDAO::$privateText = 'Privado';
		AccountMySqlExtDAO::$publicText = 'Público';
	}
}
?>