<?php
class ConfigurationService {
	public static function ConfigureServices() {
		if (file_exists('production.php')) {
			include 'production.php';
		} else {
			ConnectionProperty::$host = 'localhost';
			ConnectionProperty::$user = 'root';
			ConnectionProperty::$password = '';
			ConnectionProperty::$database = 'qfinance';
			DeveloperService::$bypassLogin = true;
		}
	}
}