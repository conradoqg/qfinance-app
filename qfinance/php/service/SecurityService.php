<?php
/**
 * Security service
 */
class SecurityService {		
	/**
	 * Returns security service instance
	 * @var SecurityService Security service instance
	 */
	private static $securityServiceInstance;
	
	/**
	 * Registered remoting actions
	 * @var array Array of Action
	 */
	private $actions = array();

	/**
	 * Return security service instance
	 * @return SecurityService Security Service 
	 */
	public static function getInstance() {
		if (self::$securityServiceInstance == null)
			self::$securityServiceInstance = new SecurityService();
		
		return self::$securityServiceInstance;
	}
	
	/**
	 * Security service constructor
	 */
	private function SecurityService() {
		$this->registerAction('LoginAction', false);			
		$this->registerAction("CategoriesEditPanelAction", true);
		$this->registerAction("AccountAction", true);			
		$this->registerAction("AccountsEditPanelAction", true);			
		$this->registerAction("TransactionsImportPanelAction", true);
		$this->registerAction("TransactionsEditPanelAction", true);			
		$this->registerAction("DeveloperServiceAction", false);
		$this->registerAction("AccountOverviewPanelAction", true);
		$this->registerAction("ExpensesAndIncomesByCategoryPanelAction", true);
		$this->registerAction("UsersEditPanelAction", true);
		$this->registerAction("TestAction", false);
		$this->registerAction("MobileAction", false);
		$this->registerAction("AccountsBalancePortletAction", true);
		$this->registerAction("TopAmountPortletAction", true);
		$this->registerAction("OverviewPanelAction", true);
		$this->registerAction("AccountsBalanceHistoryPortletAction", true);
                $this->registerAction("MarksEditPanelAction", true);
                $this->registerAction("TransactionEditWindowAction", true);
	}
	
	/**
	 * Login into application
	 * @param string $username Username
	 * @param string $password Password
	 */
	public function login($username, $password) {			
		$userDao = DAOFactory::getUserDAO();
		$user = $userDao->queryByUsernameAndPassword($username, md5($password));
		
		if (isset($user)) {
			if (strtolower($user->username) == strtolower($username) && $user->password == md5($password)) { 			
				$_SESSION["user"] = serialize($user);
				return true;				
			} else {
				return false;
			}
		}
		return false;
	}
	
	/**
	 * Logout of application 
	 */
	public function logout() {
		unset($_SESSION['user']);
	}
	
	/**
	 * Checks if user is logged
	 * @return boolean returns true if user is logged or false if is not
	 */
	public function isLogged() {
		if (isset($_SESSION['user']) && !is_null($_SESSION['user']))
			return true;
		else
			return false;
	}
	
	/**
	 * Register a remote action
	 * @param string $actionName Class path
	 * @param boolean $requireAuthorization If class requires authentication
	 */
	public function registerAction( $actionName, $requireAuthorization ) {
		array_push( $this->actions, new Action($actionName, $requireAuthorization ) );
	}	

	/**
	 * Returns if action requires authentication
	 * @param string $actionName Action name
	 * @return boolean If require authentication return true, else return false.
	 */
	public function actionNeedAuthorization( $actionName ) {
		foreach ($this->actions as $analizedAction) {			
			if ($actionName == $analizedAction->actionName) {
				return $analizedAction->requireAuthorization;
			}
		}
		return true;
	}	

	/**
	 * Get avaliable actions
	 */
	public function getActions() {
		return $this->actions;
	}
}
?>