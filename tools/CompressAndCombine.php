<?php

function compressAndCombine() {
    $path = '../QFinance/';
    $directoriesToExclude = array('../QFinance/js/');

    echo 'Reading cached file list...' . PHP_EOL;

    $fileList = @file_get_contents($path . 'cache/qfinancefrontendfilelist.cache');
    if (empty($fileList))
        throw new Exception('Cannot load front-end file list cache.');
    $fileList = json_decode($fileList);

    /*
      foreach ($fileList as $file) {
      echo $file . PHP_EOL;
      } */

    echo 'Creating joined js file...' . PHP_EOL;

    $fd = fopen($path . 'js/qfinance-all-debug.js', "w");
    if ($fd) {
        foreach ($fileList as $file) {
            $fileInfo = pathinfo($file);
            if ($fileInfo['extension'] == 'js')
                fwrite($fd, file_get_contents($path . $file) . PHP_EOL);
        }
        fclose($fd);
    }

    echo 'Compressing joined js file...';

    exec('java -jar ../external/tools/yuicompressor-2.4.2/build/yuicompressor-2.4.2.jar ' . $path . 'js/qfinance-all-debug.js' . ' -o ' . $path . 'js/qfinance-all.js');
}

?>