<?php
require_once("Util.php");
require_once("PrepareDevelopment.php");
require_once("CompressAndCombine.php");
require_once("QFinanceCacher.php");
require_once("Build.php");
require_once("PHPDao/GenerateDAO.php");

$arguments = getopt("", array("action:"));

if ($arguments["action"]) {
    switch ($arguments["action"]) {
        case "prepareDevelopment":
            prepareDevelopment();
            break;
        case "compressAndCombine":
            compressAndCombine();
            break;
        case "qfinanceCacher":
            qfinanceCacher();
            break;
        case "generateDAO":
            generateDAO();
            break;
        case "buildDevelopment":
            buildDevelopment();
            qfinanceCacher();
            generateDAO();
            break;
        case "clear":
            clearDevelopment();
            break;        
        default:
            echo "Requested task '" . $arguments["action"] . "' does not exist." . PHP_EOL;
            break;
    }
}
?>
