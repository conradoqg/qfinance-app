<?php
function prepareDevelopment() {
    downloadAndUnzip();
    createLinks();
}

function downloadAndUnzip() {
    $temporaryDirectory = "external/";
    $necessaryTools = array(
        array("downloadPath" => "http://qfinance-app.googlecode.com/files/wamp-2.2.zip", "filename" => "wamp-2.2.zip", "destination" => "../external/tools/", "description" => "Wamp Server"),
        array("downloadPath" => "http://qfinance-app.googlecode.com/files/yuicompressor-2.4.2.zip", "filename" => "yuicompressor-2.4.2.zip", "destination" => "../external/tools/", "description" => "YUI Compressor 2.4.2"),
        array("downloadPath" => "http://qfinance-app.googlecode.com/files/Ext-3.3.1.zip", "filename" => "Ext-3.3.1.zip", "destination" => "../external/lib/", "description" => "Ext JS 3.3.1"),
        array("downloadPath" => "http://qfinance-app.googlecode.com/files/Highcharts-2.1.4.zip", "filename" => "Highcharts-2.1.4.zip", "destination" => "../external/lib/", "description" => "Hightcharts 2.1.4"),
        array("downloadPath" => "http://qfinance-app.googlecode.com/files/Sencha-touch-1.0.1a.zip", "filename" => "Sencha-touch-1.0.1a.zip", "destination" => "../external/lib/", "description" => "Sencha Touch 1.0.1a"),
        array("downloadPath" => "http://qfinance-app.googlecode.com/files/Fugue-icons-3.0-src.zip", "filename" => "Fugue-icons-3.0-src.zip", "destination" => "../external/resources/", "description" => "Fugue icons 3.0"),
        array("downloadPath" => "http://qfinance-app.googlecode.com/files/Diagona-icons-1.0.zip", "filename" => "Diagona-icons-1.0.zip", "destination" => "../external/resources/", "description" => "Diagona icons 1.0")
    );

    echo "Cleaning temporary directory." . PHP_EOL;
    if (file_exists($temporaryDirectory)) {
        rrmdir($temporaryDirectory);
    }
    
    @mkdir($temporaryDirectory);
    
    echo "Checking external requirements." . PHP_EOL;

    foreach ($necessaryTools as $tool) {        
        if (file_exists($tool["destination"] . pathinfo($tool["filename"], PATHINFO_FILENAME))) {
            echo "External requirement '" . $tool["description"] . "' already exists." . PHP_EOL;
        } else {
            echo "Downloading external requirement '" . $tool["description"] . "'..." . PHP_EOL;
            downloadFile($tool["downloadPath"], $temporaryDirectory . $tool["filename"]);
            echo "Unzipping external requirement '" . $tool["description"] . "' at '" . $tool["destination"] . "'..." . PHP_EOL;
            unzip($temporaryDirectory . $tool["filename"], $tool["destination"], true, true);
            unlink($temporaryDirectory . $tool["filename"]);
        }
    }

    rmdir($temporaryDirectory);
}

function createLinks() {
    if (createLink("..\\external\\tools\\wamp-2.2\\", "c:/wamp")) {
        createLink("..\\qfinance", "c:/wamp/www/qfinance");
    }
}


?>
