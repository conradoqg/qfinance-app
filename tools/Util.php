<?php

function createLink($source, $destination) {
    $source = realpath(dirname(__FILE__) . "\\" . $source);

    if (file_exists($destination)) {
        if (is_link($destination)) {
            echo "Link '" . $destination . "' already exists." . PHP_EOL;
        } else {
            echo "Directory '" . $destination . "' already exists. Please check." . PHP_EOL;
            return false;
        }
    } else {
        echo "Creating link '" . $source . "' to '" . $destination . "'" . PHP_EOL;
        if (!symlink($source, $destination)) {
            echo "Cannot create link, check admin permission. Please check." . PHP_EOL;
            return false;
        }
    }
    return true;
}

function downloadFile($source, $destination) {
    $fs = fopen($source, 'r');
    $fd = fopen($destination, 'w');
    if ($fs && $fd) {
        while (!feof($fs)) {
            $buffer = fread($fs, 2048);
            fwrite($fd, $buffer);
        }
        fclose($fs);
        fclose($fd);
    }
}

/**
 * Unzip the source_file in the destination dir
 *
 * @param   string      The path to the ZIP-file.
 * @param   string      The path where the zipfile should be unpacked, if false the directory of the zip-file is used
 * @param   boolean     Indicates if the files will be unpacked in a directory with the name of the zip-file (true) or not (false) (only if the destination directory is set to false!)
 * @param   boolean     Overwrite existing files (true) or not (false)
 *  
 * @return  boolean     Succesful or not
 */
function unzip($src_file, $dest_dir = false, $create_zip_name_dir = true, $overwrite = true) {
    if ($zip = zip_open($src_file)) {
        if ($zip) {
            $splitter = ($create_zip_name_dir === true) ? "." : "/";
            if ($dest_dir === false)
                $dest_dir = substr($src_file, 0, strrpos($src_file, $splitter)) . "/";

            // Create the directories to the destination dir if they don't already exist
            createDirs($dest_dir);

            // For every file in the zip-packet
            while ($zip_entry = zip_read($zip)) {
                // Now we're going to create the directories in the destination directories
                // If the file is not in the root dir
                $pos_last_slash = strrpos(zip_entry_name($zip_entry), "/");
                if ($pos_last_slash !== false) {
                    // Create the directory where the zip-entry should be saved (with a "/" at the end)
                    createDirs($dest_dir . substr(zip_entry_name($zip_entry), 0, $pos_last_slash + 1));
                }

                // Open the entry
                if (zip_entry_open($zip, $zip_entry, "r")) {

                    // The name of the file to save on the disk
                    $file_name = $dest_dir . zip_entry_name($zip_entry);

                    // Check if the files should be overwritten or not
                    if ($overwrite === true || $overwrite === false && !is_file($file_name)) {
                        // Get the content of the zip entry
                        $fstream = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                        if ($fstream) {
                            file_put_contents($file_name, $fstream);
                            // Set the rights
                            chmod($file_name, 0777);
                            //echo "save: " . $file_name . PHP_EOL;
                        } else {
                            touch($file_name);
                        }
                    }

                    // Close the entry
                    zip_entry_close($zip_entry);
                }
            }
            // Close the zip-file
            zip_close($zip);
        }
    } else {
        return false;
    }

    return true;
}

/**
 * This function creates recursive directories if it doesn't already exist
 *
 * @param String  The path that should be created
 *  
 * @return  void
 */
function createDirs($path) {
    if (!is_dir($path)) {
        $directory_path = "";
        $directories = explode("/", $path);
        array_pop($directories);

        foreach ($directories as $directory) {
            $directory_path .= $directory . "/";
            if ($directory != "..") {
                if (!is_dir($directory_path)) {
                    mkdir($directory_path);
                    chmod($directory_path, 0777);
                }
            }
        }
    }
}

/*
  // Extract C:/zipfiletest/zip-file.zip to C:/zipfiletest/zip-file/ and overwrites existing files
  unzip("C:/zipfiletest/zip-file.zip", false, true, true);

  // Extract C:/zipfiletest/zip-file.zip to C:/another_map/zipfiletest/ and doesn't overwrite existing files. NOTE: It doesn't create a map with the zip-file-name!
  unzip("C:/zipfiletest/zip-file.zip", "C:/another_map/zipfiletest/", true, false);
 */

function rrmdir($dir) {
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (filetype($dir . "/" . $object) == "dir")
                    rrmdir($dir . "/" . $object); else
                    unlink($dir . "/" . $object);
            }
        }
        reset($objects);
        rmdir($dir);
    }
}

// copies files and non-empty directories
function rcopy($src, $dst) {
    //if (file_exists($dst)) {
    //    rrmdir($dst);
    //}
    if (is_dir($src)) {
        if (!file_exists($dst)) {
            createDirs($dst);
        }
        $files = scandir($src);
        foreach ($files as $file) {
            if ($file != "." && $file != "..") {
                if (is_file($src . $file)) {
                    rcopy($src . $file, $dst . $file);
                } else {
                    rcopy($src . $file . "/", $dst . $file . "/");
                }
            }
        }
    } else if (file_exists($src)) {
        if (!file_exists(pathinfo($dst, PATHINFO_DIRNAME) . "/")) {
            createDirs(pathinfo($dst, PATHINFO_DIRNAME) . "/");
        }
        //echo $src . " - " . $dst . PHP_EOL;
        copy($src, $dst);
    }
}

function array_insert($array, $pos, $val) {
    $array2 = array_splice($array, $pos);
    $array[] = $val;
    $array = array_merge($array, $array2);

    return $array;
}

function array_remove($array, $key) {
    foreach ($array as $j => $i) {
        if ($j == $key) {
            unset($array[$j]);
            $array = array_values($array);
            break;
        }
    }
    return $array;
}

?>
